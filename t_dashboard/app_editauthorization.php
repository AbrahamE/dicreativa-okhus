<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_add_meeting.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

$authorization_name = false;

		if(isset($_POST['registro_autorizacion'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "UPDATE tbl_meeting SET 
					branch_id = '$branch_id',
					id_rent = '$rid',
					event_type = '$_POST[event_type]',
					authorization_name = '$_POST[authorization_name]',
					authorization_category = '$_POST[authorization_category]',
					authorization_note = '$_POST[authorization_note]',
					start_date = '$_POST[fecha_inicio_individual]',	
					end_date= '$_POST[fecha_fin_individual]'
					WHERE meeting_id = '" . $_GET['id_evento'] . "'";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
	
			if($add) {
			
				$id_event = mysqli_affected_rows($link);
			
			}
			
			mysqli_commit($link);
																
			//mysqli_close($link);
			
}

if(isset($_POST['eliminar_autorizacion'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "UPDATE tbl_meeting SET 
					deleted = 1 
					WHERE meeting_id = '" . $_GET['id_evento'] . "'";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
				
			if($add) {
			
				$id_event = mysqli_affected_rows($link);
			
			}
			
			mysqli_commit($link);
																
			//mysqli_close($link);
			
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 12px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 60px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}


.header {
    background-color: #FFF!important;
}

.caption-background, .caption-bg {
    position: relative!important;
}

.content-fixed {
    margin: 0px 20px 30px 20px;
    background-color: #FFF;
}

.content-box {
    margin: 0px 0px 0px 0px;
}

.accordion-style-2 .accordion-icon-right {
    font-size: 15px;
    float: right;
    line-height: 45px;
    width: 20px;
    text-align: center;
}

.divider {
    height: 2px;
    display: block;
    background-color: rgba(0,0,0,0.05);
    margin-bottom: 0px!important;
}

.landing-icons a {
	height: 80px!important;
    width: 50%!important;
    display: block;
    float: left;
    margin-bottom: 5%;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>t_dashboard/app_listauthorization.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_listauthorization.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="far fa-list-alt"></i></a>
		
		<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ $id_event = $_GET['id_evento'];?>		
		
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_viewauthorization.php?id_evento=<?php echo $id_event; ?>mode=edit" class="header-icon header-icon-2 color-white back-button font-30 left-10"><i class="far fa-arrow-alt-circle-left"></i></a>		
	
		<?php } ?>
	
	</div>
	
<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
	<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
	<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
	<div class="clear"></div>
</div>		
	                 
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

				<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0) {
								
					mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
					
					$result = mysqli_query($link,"SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where meeting_id = '" . $_GET['id_evento'] . "' and tbl_meeting.id_rent = " . (int)$_SESSION['objLogin']['rid'] . " and tbl_meeting.branch_id = " . (int)$_SESSION['objLogin']['branch_id'] . " and tbl_meeting.deleted = 0  LIMIT 1");					
					$event_count = mysqli_num_rows($result);
					
					if($event_count != false && $event_count > 0) {
					
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					
					$meeting_id = $row["meeting_id"];
					$event_type = $row["event_type"];
					$visitor_name = $row["visitor_name"];
					$visitor_email = $row["visitor_email"];
					$visitor_phone = $row["visitor_phone"];
					$id_invitacion = $row["meeting_id"];
					$visitor_frequent = $row["visitor_frequent"];
					$visitor_ife = $row["visitor_ife"];
					$visitor_image = $row["visitor_image"];
					$event_name = $row["event_name"];
					$event_visitors = $row["event_visitors"];
					$r_address = $row["r_address"];
					$fecha_inicio_individual = date("Y-m-d\TH:i:s",strtotime($row["start_date"]));
					$fecha_fin_individual = date("Y-m-d\TH:i:s",strtotime($row["end_date"]));
					
					$authorization_name = $row["authorization_name"];
					$authorization_category = $row["authorization_category"];
					$authorization_note = $row["authorization_note"];

					mysqli_commit($link);
					
				
				?>			

        <div id="registro" data-height="cover-header" class="caption">  

            <div class="content left-0 right-0">
                <div class="content content-box content-box-full-top">
                    <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">

					<!--p class="under-heading font-15 color-theme opacity-70 bottom-10">Ingresar detalles para el Registro de la Autorización</p-->			
					
						<div class="tab-controls tab-animated tabs-large tabs-rounded" 
							 data-tab-items="1" 
							 data-tab-active="bg-green1-dark">
							<a href="#" data-tab-active data-tab="tab-1"><i class="far fa-user"></i> Autorizaciones</a>
						</div>
						<div class="clear bottom-15"></div>
						
							<div class="content content-fixed accordion-style-2 accordion-border accordion-round-medium">	
		

								<a href="#" data-accordion="accordion-content-1" class="">
									<!--i class="accordion-icon-left fas fa-utensils color-blue2-dark"></i-->
									Comida
									<i class="accordion-icon-right fa fa-plus"></i>
								</a>
										<div id="accordion-content-1" class="accordion-content content content-box round-medium top-15" style="display:<?= ($row["authorization_category"] == "COMIDA") ? "block" : "none"?>;">

									
										<div class="landing-icons">  
										
										<?php
										
										$count_comida = 0;
										
										$result = mysqli_query($link, "SELECT * FROM tbl_brands WHERE category IN('COMIDA');");					
									
										$event_count = mysqli_num_rows($result);	

										if($event_count != false && $event_count > 0) {
											
											while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { 

											$count_comida++;
											
										?>
													
											<?php if($row["category"] == "COMIDA") { ?>	
											
												<?php if ($count_comida%2!=0){ ?>		

										
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-left: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>
												   
												<?php } else { ?>
												
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-right: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>										

												<?php } ?>	
										

											<?php } ?>	

											
			
										<?php } mysqli_free_result($result);} else { ?>	
										
										<div class="content content-box round-medium shadow-huge bottom-0">
											<h4 class="text-center">Error en el listado de las categoría de Comida</h4>
											<p class="text-center">
												No se han encontrado ninguna información de la categoría buscada.
											</p>
											<center>
												<a href="<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
											</center>
										</div>					
								
										<?php } ?>									
										
									</div> 
								</div>


								<a href="#" data-accordion="accordion-content-2" class="">
									<!--i class="accordion-icon-left fas fa-truck color-blue2-dark"></i-->
									Paquetería
									<i class="accordion-icon-right fa fa-plus"></i>
								</a>
										<div id="accordion-content-2" class="accordion-content content content-box round-medium top-15">
									
										<div class="landing-icons">  
										
										<?php
										
										$count_paqueteria = 0;
										
										$result = mysqli_query($link, "SELECT * FROM tbl_brands WHERE category IN('PAQUETERIA');");					
									
										$event_count = mysqli_num_rows($result);	

										if($event_count != false && $event_count > 0) {
											
											while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {  
											
											$count_paqueteria++;
											
										?>
													
											<?php if($row["category"] == "PAQUETERIA") { ?>
											
											
								
												<?php if ($count_paqueteria%2!=0){ ?>		

										
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-left: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>
												   
												<?php } else { ?>
												
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-right: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>										

												<?php } ?>	
							   
											<?php } ?>	

											
			
										<?php } mysqli_free_result($result);  } else { ?>	
										
										<div class="content content-box round-medium shadow-huge bottom-0">
											<h4 class="text-center">Error en el listado de las categoría de Paquetería</h4>
											<p class="text-center">
												No se han encontrado ninguna información de la categoría buscada.
											</p>
											<center>
												<a href="<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
											</center>
										</div>					
								
										<?php } ?>									
										
									</div> 
								</div>


								<a href="#" data-accordion="accordion-content-3" class="">
									<!--i class="accordion-icon-left fas fa-taxi color-blue2-dark"></i-->
									Taxis
									<i class="accordion-icon-right fa fa-plus"></i>
								</a>
										<div id="accordion-content-3" class="accordion-content content content-box round-medium top-15">
									
											<div class="landing-icons">  
										
										<?php
										
										$count_taxis = 0;
										
										$result = mysqli_query($link, "SELECT * FROM tbl_brands WHERE category IN('TAXIS');");					
									
										$event_count = mysqli_num_rows($result);	

										if($event_count != false && $event_count > 0) {
											
											while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {  
											
											$count_taxis++;
											
										?>
													
											<?php if($row["category"] == "TAXIS") { ?>
											
											
								
												<?php if ($count_taxis%2!=0){ ?>		

										
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-left: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>
												   
												<?php } else { ?>
												
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-right: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>										

												<?php } ?>	
							   
											<?php } ?>	

											
			
										<?php } mysqli_free_result($result); } else { ?>	
										
										<div class="content content-box round-medium shadow-huge bottom-0">
											<h4 class="text-center">Error en el listado de las categoría de Taxis</h4>
											<p class="text-center">
												No se han encontrado ninguna información de la categoría buscada.
											</p>
											<center>
												<a href="<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
											</center>
										</div>						
								
										<?php } ?>									
										
									</div> 
								</div>
								
								
								<a href="#" data-accordion="accordion-content-4" class="">
									<!--i class="accordion-icon-left fas fa-taxi color-blue2-dark"></i-->
									Otros
									<i class="accordion-icon-right fa fa-plus"></i>
								</a>
								<div class="divider bottom-0"></div>
										<div id="accordion-content-4" class="accordion-content content content-box round-medium top-15">
									
										<div class="landing-icons">  
										
										<?php
										
										$count_otros = 0;
										
										$result = mysqli_query($link, "SELECT * FROM tbl_brands WHERE category IN('OTROS');");					
									
										$event_count = mysqli_num_rows($result);	

										if($event_count != false && $event_count > 0) {
											
											while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {  
											
											$count_otros++;
											
										?>
													
											<?php if($row["category"] == "OTROS") { ?>
											
											
								
												<?php if ($count_otros%2!=0){ ?>		

										
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-left: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>
												   
												<?php } else { ?>
												
												   <a href="#" data-brand="<?php echo $row["id"];?>" data-name="<?php echo $row["name"];?>" data-category="<?php echo $row["category"];?>"data-menu="action-autorizacion">
													   <img style="margin-right: 50%!important;" src="../app/images/iconos/empresas/<?php echo $row["image"];?>">
													   <!--em><?php echo $row["name"];?></em-->
												   </a>										

												<?php } ?>	
							   
											<?php } ?>	

											
			
										<?php } mysqli_free_result($result); } else { ?>	
										
										<div class="content content-box round-medium shadow-huge bottom-0">
											<h4 class="text-center">Error en el listado de las categoría de Taxis</h4>
											<p class="text-center">
												No se han encontrado ninguna información de la categoría buscada.
											</p>
											<center>
												<a href="<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
											</center>
										</div>					
								
										<?php } ?>									
										
									</div> 
								</div>																
                </div>						
						
						
										
                </div>
            </div>
			
				<?php } else { ?>
				
				<img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">
						
				<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en Detalle de la Autorización</h4>
					<p class="text-center">
						No se han encontrado ninguna información a la autorización buscada.
					</p>
					<center>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_listauthorization.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					</center>
				</div>	
				<?php } ?>				
	
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="" style=""></div>
        </div> 
		<?php } ?>	
		
    </div>  

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-green1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_viewauthorization.php?id_evento=<?php echo $id_event; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-green1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php?id_evento=<?php echo $id_event; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>
	
    <div id="action-autorizacion" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="420" data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Autorizaciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Registro de Autorización</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>        
        
        <div class="content">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
						
							<input type="hidden" name="event_type" value="AUTORIZACION"/>
							<input type="hidden" id="authorization_name" name="authorization_name" value="<?=$authorization_name?>"/>
							<input type="hidden" id="authorization_category" name="authorization_category" value="<?=$authorization_category?>"/>
							<input type="hidden" id="authorization_brand" name="authorization_brand" value="<?=$authorization_brand?>"/>
													
								<span class="input-style-1-active input-style-1-inactive">Fecha Inicio</span>

							<div class="input-style input-style-1 input-required">
								
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_inicio_evento" name="fecha_inicio_evento" value="<?=$fecha_inicio_individual?>" required>
							</div>
								<span class="input-style-1-active input-style-1-inactive">Fecha Fin</span>
							
							<div class="input-style input-style-1 input-required">
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_fin_evento" name="fecha_fin_evento" value="<?=$fecha_fin_individual?>" required>
							</div>	
							<span class="input-style-1-active input-style-1-inactive">Notas</span>	
							<div class="input-style input-style-2 input-required">
								<em>(requiredo)</em>								
								<textarea type="text" id="authorization_note" name="authorization_note" placeholder="Ingresa alguna nota..." value="<?php echo preg_replace( "/\r|\n/", "", $authorization_note ); ?>" required><?php echo preg_replace( "/\r|\n/", "", $authorization_note ); ?></textarea>
							</div>								
							<div class="clear"></div>
							<center>									
								<div class="one-half">
									<button type="submit" id="registro_autorizacion" name="registro_autorizacion" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Guardar</button>
								</div>								
								<div class="one-half last-column">	
									<button type="submit" id="eliminar_autorizacion" name="eliminar_autorizacion" class="button button-m round-huge shadow-huge button-full bg-gradient-red2 shadow-huge top-30 bottom-25">Eliminar</button>
								</div>	
							</center>
						</form>		
        </div>
    </div>  	

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript" src="../app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	<?php if((isset($_POST['registro_autorizacion']) || isset($_POST['eliminar_autorizacion'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_autorizacion']) || isset($_POST['eliminar_autorizacion'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } else { ?>

		activateMenu("action-autorizacion");	
	
	<?php } ?>	

			
	
        $('[data-menu]').on('click', function() {
		
			$("#authorization_name").val($(this).data('name'));
			$("#authorization_category").val($(this).data('category'));
			$("#authorization_brand").val($(this).data('brand'));
			
            return false;
        });		
	

	});

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
    $("#qr-auth").addClass("shadow-huge");
    //$("#qr-auth").css("width", "50%");
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>