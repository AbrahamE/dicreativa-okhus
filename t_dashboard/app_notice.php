<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_notice.php');


if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

$total_rent = '0.00';
$monthly_bill_data = '0,0,0,0,0,0,0,0,0,0,0,0';
$result_amount = mysqli_query($link,"SELECT sum(rent) as total FROM tbl_add_fair where bill_status = 0 and rid =".(int)$_SESSION['objLogin']['rid']);
if($row_amount_total = mysqli_fetch_array($result_amount)){
	if((float)$row_amount_total['total'] > 0){
		$total_rent = $row_amount_total['total'];
	}
}


if(isset($_POST['registro_notice'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];
			
			$notice_image = NULL;
			
			$notice_image = ($_POST['notice_image'] != "") ? mysqli_real_escape_string($link, $_POST['notice_image']): NULL;
			
			$created_date = $ams_helper->inputDateToMySqlDate($_POST['created_date']);
				
			$sql = "INSERT INTO tbl_notice_board(
					notice_title,
					notice_description,
					branch_id,
					rid_notice,
					notice_image,
					notice_type,
					created_date) 
					values(
					'$_POST[notice_title]',
					'$_POST[notice_description]',
					'$branch_id',
					'$rid',
					'$notice_image',
					'$_POST[notice_type]',
					'$created_date');";
					
			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
			
			mysqli_commit($link);
																
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.color-white1 {
    color: #FFFFFF!important;
}

.header {
    background-color: #FFF!important;
}

.link-list-3 em {
    position: absolute;
    font-size: 12px;
    right: 0px;
    margin-top: -21px;
    width: 100px;
    text-transform: uppercase;
    font-weight: 500;
    text-align: center;
    font-style: normal;
    border-radius: 14px;
}

.color-white-balance {
    color: #FFFFFF!important;
}

.detail td {
    text-align: left!important;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}

.header {
    background-color: #FFF!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-arrow-left"></i></a>
		
	</div>
	
	<div class="footer-menu footer-4-icons shadow-huge shadow-huge">
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general" class="active-nav"><i class="fa fa-users" style="background: linear-gradient(0deg,#3D3949,#6772A4);"></i><span>Aviso General</span></a>
		<?php } else { ?>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general"><i class="fa fa-users"></i><span>Aviso General</span></a>
		<?php } ?>
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>		
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal" class="active-nav"><i class="fa fa-user" style="background: linear-gradient(0deg,#3D3949,#6772A4);"></i><span>Aviso Personal</span></a>		
		<?php } else { ?>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal"><i class="fa fa-user"></i><span>Aviso Personal</span></a>		
		<?php } ?>
		<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
		<div class="clear"></div>
	</div>		
         
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
		
		<div class="content">
				<img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">						
				<h4 class="text-center bottom-20">Aviso General</h4>
				<a href="#" data-menu="action-notice-add" class="button shadow-huge button-xxs button-round-large button-center-large bg-green2-dark">Agregar Aviso General</a>					
				<input type="hidden" name="notices_type" value="GENERAL"/>
		</div>		

			<div class="content">	
				<?php
					$result = mysqli_query($link,"SELECT * FROM tbl_notice_board where notice_type = 'GENERAL' and branch_id=".(int)$_SESSION['objLogin']['branch_id']);
					$event_count = mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result)){
				?>	

				<div class="link-list link-list-3">
			<a href="#" data-menu="action-notice-<?= $row["notice_id"] ?>" class="round-small shadow-tiny">
				<img src="<?= $row["notice_image"] ?>" class="preload-image responsive-image" style="float:left; border: 1px solid #ccc!important; padding: 5px!important;margin: 4px!important; border-radius: 100px; height: 80px; width: 80px;">	
				<span class="color-black1 top-25"><?= utf8_encode(substr($row["notice_title"], 0, 17)."...");?> <?= date("F j, Y",strtotime($row["created_date"])); ?></span>
				<span style="font-size: 14px;"><?= utf8_encode(substr($row["notice_description"], 0, 17)."...");?></span>
			</a>
				</div>	
				<?php } ?>

				<?php  if($event_count == 0) { ?>
					
						<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el listado de los Avisos</h4>
					<p class="text-center">
						No se han encontrado ninguna información de el Aviso buscado.
					</p>
					<center>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>					
					</center>
				</div>	
				
				<?php } ?>

			</div>
		<?php } ?>	

		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>		
		
		<div class="content">
				<img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">						
				<h4 class="text-center bottom-20">Aviso General</h4>
				<a href="#" data-menu="action-notice-add" class="button shadow-huge button-xxs button-round-large button-center-large bg-green2-dark">Agregar Aviso Personal</a>					
				<input type="hidden" name="notices_type" value="PERSONAL"/>
		</div>		

			<div class="content">	
				<?php
					$result = mysqli_query($link,"SELECT * FROM tbl_notice_board where notice_type = 'PERSONAL' and branch_id=".(int)$_SESSION['objLogin']['branch_id']);
					$event_count = mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result)){
				?>	

				<div class="link-list link-list-3">
			<a href="#" data-menu="action-notice-<?= $row["notice_id"] ?>" class="round-small shadow-tiny">
				<img src="<?= $row["notice_image"] ?>" class="preload-image responsive-image" style="float:left; border: 1px solid #ccc!important; padding: 5px!important;margin: 4px!important; border-radius: 100px; height: 80px; width: 80px;">	
				<span class="color-black1 top-25"><?= utf8_encode(substr($row["notice_title"], 0, 17)."...");?> <?= date("F j, Y",strtotime($row["created_date"])); ?></span>
				<span style="font-size: 14px;"><?= utf8_encode(substr($row["notice_description"], 0, 17)."...");?></span>
			</a>
				</div>	
			<?php } ?>	
			

				<?php  if($event_count == 0) { ?>
					
						<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el listado de los Avisos</h4>
					<p class="text-center">
						No se han encontrado ninguna información de el Aviso buscado.
					</p>
					<center>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>					
					</center>
				</div>	
				
				<?php } ?>			

			</div>
		<?php } ?>
	
		<?php if(!isset($_GET['aviso'])){ ?>	
		
		<div class="content">
				<img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">						
				<h4 class="text-center bottom-20">Aviso General</h4>
		</div>		
						<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el listado de los Avisos</h4>
					<p class="text-center">
						No se han encontrado ninguna información de el Aviso buscado.
					</p>
					<center>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>
					<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					<?php } ?>					
					</center>
				</div>		
		<?php } ?>   
	</div>
	
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>

		<?php
			$result = mysqli_query($link,"SELECT * FROM tbl_notice_board where notice_type = 'GENERAL' and branch_id=".(int)$_SESSION['objLogin']['branch_id']);
			while($row = mysqli_fetch_array($result)){
		?>		
	
    <div id="action-notice-<?= $row["notice_id"] ?>"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-modal"
         data-menu-height="450"
         data-menu-width="320"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">			
            <h3 class="font-900 top-20 bottom-0"><?= utf8_encode($row["notice_title"]);?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?= date("F j, Y",strtotime($row["created_date"])); ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
		<img style="border: 1px solid #ccc!important; padding: 5px!important;" src="<?php echo $row["notice_image"];?>" class="center-text responsive-image bottom-20" />		 							
            <?= utf8_encode($row["notice_description"]);?>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar Aviso</a>        
    </div> 

	<?php } ?>
<?php } ?>
	
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>

		<?php
			$result = mysqli_query($link,"SELECT * FROM tbl_notice_board where notice_type = 'PERSONAL' and branch_id=".(int)$_SESSION['objLogin']['branch_id']);
			while($row = mysqli_fetch_array($result)){
		?>		
	
    <div id="action-notice-<?= $row["notice_id"] ?>"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-modal"
         data-menu-height="450"
         data-menu-width="320"
         data-menu-effect="menu-over">
		         
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?= utf8_encode($row["notice_title"]);?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?= date("F j, Y",strtotime($row["created_date"])); ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
		<img style="border: 1px solid #ccc!important; padding: 5px!important;" src="<?php echo $row["notice_image"];?>" class="center-text responsive-image bottom-20" />		 							
            <?= utf8_encode($row["notice_description"]);?>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar Aviso</a>        
    </div> 

	<?php } ?>   	
<?php } ?>

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-blue1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
		<?php } ?>  		
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
		<?php } ?>   	
	</div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'general'){ ?>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
		<?php } ?>  		
		<?php if(isset($_GET['aviso']) && $_GET['aviso'] == 'personal'){ ?>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=personal'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
		<?php } ?>   
    </div>
	

    <div id="action-notice-add" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="500" data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Anuncios</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Registro de Anuncio</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>        
        
        <div class="content">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
						<input type="hidden" name="notice_type" value=""/>
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Título del Aviso</span>
								<em>(requerido)</em>
								<input type="text" name="notice_title" id="notice_title" class="form-control" required placeholder="Título del Aviso" />
							</div>									

							<div class="input-style input-style-2 input-required">
								<span>Ingresa una descripción</span>
								<em>(requerido)</em>
								<textarea id="notice_description" name="notice_description" placeholder="Ingresa una descripción..." required></textarea>
							</div>
							
							<span class="input-style-1-active input-style-1-inactive">Fecha Aviso<em style="text-align: right;float: right;top: 14px;font-size: 10px;font-style: normal;right: 0px;color: rgba(0,0,0,0.3);">(requerido)</em></span>
							
							<div class="input-style input-style-1 input-required">								
								<em><i class="fas fa-calendar-alt"></i></em>
								<input type="date" id="created_date" name="created_date" value="" required>
							</div>							

							<div class="file-data top-20">
								<input type="file" id="set_image_notice" class="upload-file button bg-green1-dark button-full shadow-huge round-small button-xs " accept="image/png, image/jpeg" required>
								<p class="upload-file-text">Subir una Imagen</p>
								<img src="../app/images/empty.png" style="margin: auto;">
								<input type="hidden" id="notice_image" name="notice_image" value=""/>
							</div>								
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_notice"  name="registro_notice" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Registrar Aviso</button>
							</center>	
						</form>		
        </div>
    </div>  	

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	$('input[name="notice_type"]').val($('input[name="notices_type"]').val());
		
	<?php if((isset($_POST['registro_notice']) || isset($_POST['registro_notice'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_notice']) || isset($_POST['registro_notice'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	

	});

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>