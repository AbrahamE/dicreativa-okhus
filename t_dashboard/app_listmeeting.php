<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_add_meeting.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

//var_dump($ams_helper->dump($_SESSION));die;

//var_dump($ams_helper->dump($_POST));die;

if(isset($_POST['registro_individual'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					visitor_name,
					visitor_email,
					visitor_phone,
					visitor_frequent,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[nombres]',
					'$_POST[correo]',
					'$_POST[telefono]',
					'$_POST[frecuente]',
					'$_POST[fecha_inicio_individual]',					
					'$_POST[fecha_fin_individual]')";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
			
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

if(isset($_POST['registro_evento'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					event_name,
					event_visitors,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[event_name]',
					'$_POST[numero_invitados]',
					'$_POST[fecha_inicio_evento]',					
					'$_POST[fecha_fin_evento]')";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
						
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.color-white1 {
    color: #FFFFFF!important;
}

.header {
    background-color: #FFF!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-arrow-left"></i></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="header-icon header-icon-2 color-white back-button font-30 left-10"><i class="fa fa-plus-circle"></i></a>		
	</div>
	
<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
	<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
	<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
	<div class="clear"></div>
</div>		
         
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

        <div id="registro" data-height="cover-header" class="caption" style="display:block;">  

            <div class="caption left-0 right-0">
                <div class="content content-box content-box-full-top">
                    <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">
						
					<h4 class="text-center bottom-20">Mis Invitaciones y Eventos</h4>
					
				<?php 
				
					$result = mysqli_query($link, "SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where id_rent IS NOT NULL and event_type != 'INVITACION' and tbl_meeting.id_rent = " . (int)$_SESSION['objLogin']['rid'] . " and tbl_meeting.branch_id = " . (int)$_SESSION['objLogin']['branch_id'] . " and tbl_meeting.deleted = 0 order by meeting_id desc;");					
					
					$event_count = mysqli_num_rows($result);	

					if($event_count != false && $event_count > 0) {
						
						while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { 
						
							$start_date = date("d-m-Y h:m:s a",strtotime($row["start_date"]));
							$end_date = date("d-m-Y h:m:s a",strtotime($row["end_date"]));
						
						?>
						
							<?php if($row["event_type"] == "EVENTO") { ?>
						
				
							<div class="link-list link-list-3">
								<a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_viewmeeting.php?id_evento=<?php echo $row["meeting_id"]; ?>'" class="round-small shadow-tiny bg-green2-dark no-border">
									<i class="fas fa-users color-white1"></i>
									<span class="color-white1 text-right right-10"># <?= $row["meeting_id"]?></span>
									<span class="color-white1">Evento: <?= $row["event_name"]?></span>
									<span class="color-white1">Inicio: <?= $start_date?></span>
									<span class="color-white1">Fin: <?= $end_date?></span>
								</a>
								
							</div>
							
							<?php } ?>
							
							<?php if($row["event_type"] == "INDIVIDUAL") { ?>
														
							<div class="link-list link-list-3">
								<a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_viewmeeting.php?id_evento=<?php echo $row["meeting_id"]; ?>'" class="round-small shadow-tiny bg-blue2-dark no-border">
									<i class="fas fa-user color-white1"></i>
									<span class="color-white1 text-right right-10"># <?= $row["meeting_id"]?></span>									
									<span class="color-white1">Invitación para: <?= $row["visitor_name"]?></span>									
									<span class="color-white1">Inicio: <?= $start_date?></span>
									<span class="color-white1">Fin: <?= $end_date?></span>
								</a>
							</div>							
							
							<?php } ?>
				<?php } mysqli_free_result($result); mysqli_close($link); } else { ?>	
				
				<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el listado de las Invitaciones</h4>
					<p class="text-center">
						No se han encontrado ninguna información de la Invitación buscada.
					</p>
					<center>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					</center>
				</div>					
				
				<?php } ?>
					
                </div>
            </div>
	
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="" style=""></div>
        </div> 
		
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div> 

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-blue1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php?id_evento=<?php echo $id_event; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>

	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>

    <div id="action-maps"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="374"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Ubicación del Evento</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Mapa detallado del Evento</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class='responsive-iframe max-iframe bottom-0'>
		
			<?php $address = "https://maps.google.com/maps?q=".$r_address."&t=&z=13&ie=UTF8&iwloc=&output=embed"; ?>
		
			<iframe src='<?= $address ?>' frameborder='0' allowfullscreen>
			
			</iframe>
		
		</div>
    </div> 

	<?php } ?>


<div id="event_share" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="390" data-menu-effect="menu-over">
	<div class="content bottom-0">
        <img src="../app/images/logo-dark.png" class="center-text responsive-image-full top-20 bottom-20" />
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>

        <div class="landing-icons">
            <a href="#">
                <img src="../app/images/iconos/compartir/whatsapp.png" />
                <em>Whatsapp</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/facebook.png" />
                <em>Facebook</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/messenger.png" />
                <em>Messenger</em>
            </a>

            <div class="clear"></div>
        </div>

        <div class="landing-icons">
            <a href="#">
                <img src="../app/images/iconos/compartir/instagram.png" />
                <em>Instagram</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/whatsappbusiness.png" />
                <em>Whatsapp Business</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/gmail.png" />
                <em>Gmail</em>
            </a>

            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript" src="../app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	<?php if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	
	
	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>
		
		generateQR(<?= $meeting_i ?>);

	<?php } ?>

	});

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
    $("#qr-auth").addClass("shadow-huge");
    //$("#qr-auth").css("width", "50%");
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>