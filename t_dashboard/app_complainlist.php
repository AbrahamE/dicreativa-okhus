<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_complain_list.php');


if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

if(isset($_POST['registro_complain'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];
			
			$notice_image = NULL;
			
			$notice_image = ($_POST['notice_image'] != "") ? mysqli_real_escape_string($link, $_POST['notice_image']): NULL;
			
			$created_date = $ams_helper->inputDateNormalToMySqlDate($_POST['c_date']);
				
			$xmonth = date('m');
			$xyear = date('Y');
						
			$sql = "INSERT INTO tbl_add_complain(c_title,c_description, c_date, c_month,c_year,c_userid,branch_id,complain_by,person_name,person_email,person_contact) values('$_POST[c_title]','$_POST[c_description]','$created_date',$xmonth,$xyear,'".(int)$_SESSION['objLogin']['rid']."','" . $_SESSION['objLogin']['branch_id'] . "','tenant','" . $_SESSION['objLogin']['r_name'] . "','" . $_SESSION['objLogin']['r_email'] . "','" . $_SESSION['objLogin']['r_contact'] . "')";
					
			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
			
			mysqli_commit($link);
																
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.color-white1 {
    color: #FFFFFF!important;
}

.header {
    background-color: #FFF!important;
}

.link-list-3 em {
    position: absolute;
    font-size: 12px;
    right: 0px;
    margin-top: -21px;
    width: 100px;
    text-transform: uppercase;
    font-weight: 500;
    text-align: center;
    font-style: normal;
    border-radius: 14px;
}

.color-white-balance {
    color: #FFFFFF!important;
}

.detail td {
    text-align: left!important;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}

.header {
    background-color: #FFF!important;
}

.detail td {
    text-align: left!important;
}

.title_detail {
	
	font-weight: 900;
}


</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-arrow-left"></i></a>
		
	</div>
	
	<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
		<div class="clear"></div>
	</div>		
         
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

		<div class="content">
				<img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">						
				<h4 class="text-center bottom-20">Incidentes</h4>
				<a href="#" data-menu="action-complain-add" class="button shadow-huge button-xxs button-round-large button-center-large bg-green2-dark">Agrega Incidencia</a>					
		</div>		

			<div class="content">	
				<?php
					$result = mysqli_query($link,"Select *,m.month_name from tbl_add_complain c inner join tbl_add_month_setup m on m.m_id = c.c_month where c.branch_id = '" . (int)$_SESSION['objLogin']['branch_id'] . "' and c.c_userid = '" . (int)$_SESSION['objLogin']['rid'] . "' order by complain_id DESC");
					$event_count = mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result)){
				?>	

				<div class="link-list link-list-3">
			<a href="#" data-menu="action-notice-<?= $row["complain_id"] ?>" class="round-small shadow-tiny">
				<img src="../app/images/iconos/incidencias/alerta.png" class="preload-image responsive-image" style="float:left; padding: 5px!important;margin: 4px!important; height: 80px; width: 80px;">	
				<span class="color-black1 top-25"><?= utf8_encode(substr($row["c_title"], 0, 17)."...");?></span>
				<span class="color-black1"><?= $row["c_date"]; ?></span>
								
				<?php if($row['job_status']=='0'){echo '<em class="bg-blue2-dark right-20">'.$_data['t_status_pending'].'</em>';} else if($row['job_status']=='1'){echo '<em class="bg-yellow2-dark right-20">'.$_data['t_status_in_progress'].'</em>';} else if($row['job_status']=='2'){echo '<em class="bg-red2-dark right-20">'.$_data['t_status_on_hold'].'</em>';} else if($row['job_status']=='3'){echo '<em class="bg-green2-dark right-20">'.$_data['t_status_completed'].'</em>';} ?>
				
				
			</a>
				</div>	
				<?php } ?>

				<?php  if($event_count == 0) { ?>
					
						<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en en listado de los Avisos</h4>
					<p class="text-center">
						No se han encontrado ninguna información de el Aviso buscado.
					</p>
					<center>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_complainlist.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            			
					</center>
				</div>	
				
				<?php } ?>

			</div>

	</div>

		<?php
			$result = mysqli_query($link,"Select *,m.month_name from tbl_add_complain c inner join tbl_add_month_setup m on m.m_id = c.c_month where c.branch_id = '" . (int)$_SESSION['objLogin']['branch_id'] . "' and c.c_userid = '" . (int)$_SESSION['objLogin']['rid'] . "' order by complain_id DESC");
			while($row = mysqli_fetch_array($result)){
		?>		
	
    <div id="action-notice-<?= $row["complain_id"] ?>"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-modal"
         data-menu-height="450"
         data-menu-width="320"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">			
            <h3 class="font-900 top-20 bottom-0"><?= utf8_encode($row["c_title"]);?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?= $row["c_date"]; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
		<img style="border: 0px solid #ccc!important; padding: 5px!important;" src="../app/images/iconos/incidencias/alerta.png" class="center-text responsive-image bottom-20" />		 							
			<h4 class="text-center">Detalle de la Incidencia</h4>
			<table class="detail table-borders no-border">
				<tbody>
							<tr>
								<td class="title_detail"><?php echo $_data['text_5'];?></td>
								<td><?php echo $row['c_title']; ?></td>
							</tr>					
							<tr>
								<td class="title_detail"><?php echo $_data['text_11'];?></td>
								<td><?php echo $row['month_name']; ?></td>
							</tr>
							
							<tr>
								<td class="title_detail"><?php echo $_data['text_12'];?></td>
								<td><?php echo $row['c_year']; ?></td>
							</tr>
							
							<tr>
								<td class="title_detail"><?php echo $_data['t_text_2'];?></td>
								<td><?php if($row['job_status']=='0'){echo '<label style="margin-bottom: 0px!important;" class="bg-blue2-dark  button button-xxs button-round-small">'.$_data['t_status_pending'].'</label>';} else if($row['job_status']=='1'){echo '<label style="margin-bottom: 0px!important;" class="bg-yellow2-dark  button button-xxs button-round-small">'.$_data['t_status_in_progress'].'</label>';} else if($row['job_status']=='2'){echo '<label style="margin-bottom: 0px!important;" class="bg-red2-dark  button button-xxs button-round-small">'.$_data['t_status_on_hold'].'</label>';} else if($row['job_status']=='3'){echo '<label style="margin-bottom: 0px!important;" class="bg-green2-dark  button button-xxs button-round-small">'.$_data['t_status_completed'].'</label>';} ?></td>
							</tr>

							<tr>
								<td class="title_detail"><?php echo $_data['text_6'];?></td>
								<td><?php echo $row['c_description']; ?></td>
							</tr>							
				</tbody>
			</table>
			
			<h4 class="text-center">Solución</h4>
			<table class="detail table-borders no-border">
				<tbody>
							<tr>
								<td class="title_detail"><?php echo $_data['solution_label'];?></td>
								<td><?php echo $row['solution']; ?></td>
							</tr>											
				</tbody>
			</table>			
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar Incidencia</a>        
    </div> 

	<?php } ?>

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-blue1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_complainlist.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
  	
	</div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_complainlist.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
  
    </div>
	

    <div id="action-complain-add" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="370" data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Incidencias</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Registro de Incidencia</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>        
        
        <div class="content">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Título de la Incidencia</span>
								<em>(requerido)</em>
								<input type="text" name="c_title" id="c_title" class="form-control" required placeholder="Título de la Incidencia" />
							</div>							

							<div class="input-style input-style-2 input-required">
								<span>Ingresa una descripción</span>
								<em>(requerido)</em>
								<textarea id="c_description" name="c_description" placeholder="Ingresa una descripción..." required></textarea>
							</div>
							
							<span class="input-style-1-active input-style-1-inactive">Fecha Incidencia<em style="text-align: right;float: right;top: 14px;font-size: 10px;font-style: normal;right: 0px;color: rgba(0,0,0,0.3);">(requerido)</em></span>
							
							<div class="input-style input-style-1 input-required">								
								<em><i class="fas fa-calendar-alt"></i></em>
								<input type="date" id="c_date" name="c_date" value="" required>
							</div>														
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_complain"  name="registro_complain" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Registrar Incidencia</button>
							</center>	
						</form>		
        </div>
    </div>  	

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	$('input[name="notice_type"]').val($('input[name="notices_type"]').val());
		
	<?php if((isset($_POST['registro_complain']) || isset($_POST['registro_complain'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_complain']) || isset($_POST['registro_complain'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	

	});

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>