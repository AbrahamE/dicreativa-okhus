<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_add_meeting.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

//var_dump($ams_helper->dump($_SESSION));die;

//var_dump($ams_helper->dump($_POST));die;

if(isset($_POST['registro_individual'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					visitor_name,
					visitor_email,
					visitor_phone,
					visitor_frequent,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[nombres]',
					'$_POST[correo]',
					'$_POST[telefono]',
					'$_POST[frecuente]',
					'$_POST[fecha_inicio_individual]',					
					'$_POST[fecha_fin_individual]')";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
			
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

if(isset($_POST['registro_evento'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					event_name,
					event_visitors,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[nombre_evento]',
					'$_POST[numero_invitados]',
					'$_POST[fecha_inicio_evento]',					
					'$_POST[fecha_fin_evento]')";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
						
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}


.header {
    background-color: #FFF!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="far fa-list-alt"></i></a>
		
		<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ $id_event = $_GET['id_evento'];?>		
		
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_editmeeting.php?id_evento=<?php echo $id_event; ?>&mode=edit" class="header-icon header-icon-2 color-white back-button font-30 left-10"><i class="far fa-edit"></i></a>		
	
		<?php } ?>
	
	</div>
	
<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
	<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
	<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
	<div class="clear"></div>
</div>		
	                 
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

				<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){
			
					$result = mysqli_query($link,"SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where meeting_id = '" . $_GET['id_evento'] . "' and tbl_meeting.id_rent = " . (int)$_SESSION['objLogin']['rid'] . " and tbl_meeting.branch_id = " . (int)$_SESSION['objLogin']['branch_id'] . " and tbl_meeting.deleted = 0 LIMIT 1");					
					$event_count = mysqli_num_rows($result);	

					if($event_count != false && $event_count > 0) {
					
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					
					$meeting_id = $row["meeting_id"];
					$event_type = $row["event_type"];
					$visitor_name = $row["visitor_name"];
					$visitor_email = $row["visitor_email"];
					$visitor_phone = $row["visitor_phone"];
					$id_invitacion = $row["meeting_id"];
					$visitor_frequent = $row["visitor_frequent"];
					$event_name = $row["event_name"];
					$event_visitors = $row["event_visitors"];
					$r_address = $row["r_address"];
					$fecha_inicio_individual = date("d-m-Y h:m:s a",strtotime($row["start_date"]));
					$fecha_fin_individual = date("d-m-Y h:m:s a",strtotime($row["end_date"]));
					
					mysqli_close($link);
					
				?>		

		<div id="qr" data-height="cover-header" class="caption" style="display: block;">
			
			<div class="content content-box">

				<img src="../app/images/logo-dark.png" class="center-text responsive-image-full bottom-20" />
			
				<h4 class="text-center">Detalle de la Invitación #<?= $meeting_id ?></h4>
				<p class="text-center">
					En esta sección se muestra el Código QR de la Invitación y la Ubicación.
				</p>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa fa-user color-blue1-dark"></i>
							
							<?php if($event_type == "INDIVIDUAL") { ?>
			
							<strong>Nombre del Visitante</strong>
							<span><?= $visitor_name ?></span>
							
							<?php } else if($event_type == "EVENTO") {?>
							
							<strong>Nombre del Evento</strong>
							<span><?= $event_name ?></span>							
							
							<?php } ?>
							
						</a>
					</div>
				</div>
				
				<?php if($event_type == "EVENTO") { ?>
				
				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="far fa fa-users color-blue1-dark"></i>
							
							<strong>Número de Invitados</strong>
							<span><?= $event_visitors ?></span>	
							
						</a>
					</div>
				</div>				
				
				<?php } ?>				

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa-calendar-alt color-blue1-dark"></i>
							<strong>Hora de Inicio / Fin</strong>
							<span><?= $fecha_inicio_individual ?></span>
							<span><?= $fecha_fin_individual ?></span>
						</a>
					</div>
				</div>

				<?php if($event_type == "INDIVIDUAL") { ?>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa-mobile color-blue1-dark"></i>
							<strong>Telefonos</strong>
							<span><?= $visitor_phone ?></span>
						</a>
					</div>
				</div>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fas fa-id-badge color-blue1-dark"></i>
							<strong>Identificación</strong>
							<span><?= $id_invitacion ?></span>
						</a>
					</div>
				</div>

				<div class="link-list link-list-3">
					<a href="#" class="round-small shadow-tiny">
						<i class="fas fa-at color-blue1-dark"></i>
						<strong>Email</strong>
							<span><?= $visitor_email ?></span>
					</a>
				</div>
				
				<?php } ?>
								
				<h4 class="text-center">QR para invitaciones</h4>
				<p class="text-center">
					Código QR valido para invitaciones de OKHUS.
				</p>
				
				<img id="qr-auth" src="../app/images/page-loader.gif" class="center-text responsive-image-full bottom-20" />
				<input id="qr-auth-data" type="hidden" name="qr-auth-data" value=""/>
				  <div id="error" style="display:none;"></div>
				  <section id="contentqr">
					<div><input type="hidden" id="invitacionqr" value="Prueba"></div>
				  </section>				
				
				<div class="landing-icons">
					<a href="#" data-menu="action-share">
						<img src="../app/images/iconos/invitacion/compartir.png" />
						<em>Compartir</em>
					</a>
					<a href="#" data-menu="action-maps" style="float: right;">
						<img src="../app/images/iconos/invitacion/ubicacion.png" style="" />
						<em>Ubicación</em>
					</a>
				</div>				
			</div>
			
				<?php } else { ?>
				
				<img src="../app/images/logo-dark.png" class="center-text responsive-image-full bottom-20" />
						
				<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el Detalle de la Invitación</h4>
					<p class="text-center">
						No se han encontrado ninguna información de la invitación buscada.
					</p>
					<center>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					</center>
				</div>	
				<?php } ?>				

		</div>
				
					
		<?php }  else { ?>

        <div id="registro" data-height="cover-header" class="caption" style="display:block;">  

            <div class="caption-center left-20 right-20">
                <div class="content content-box content-box-full-top">
                    <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">

					<p class="under-heading font-15 color-theme opacity-70 bottom-10">Ingresar detalles para el Registro de Residente</p>				
					
						<div class="tab-controls tab-animated tabs-large tabs-rounded" 
							 data-tab-items="2" 
							 data-tab-active="bg-green1-dark">
							<a href="#" data-tab-active data-tab="tab-1"><i class="far fa-user"></i> Individual</a>
							<a href="#" data-tab="tab-2"><i class="fas fa-users"></i> Evento</a>
						</div>
						<div class="clear bottom-15"></div>
						<div class="tab-content" id="tab-1">
						
						<form method="post" enctype="multipart/form-data" id="invitacion_individual">
						
							<input type="hidden" name="event_type" value="INDIVIDUAL"/>
						
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Nombre del Visitante</span>
								<em>(requerido)</em>
								<input type="text" name="nombres" id="nombres" class="form-control" required placeholder="Nombres" />
							</div>	

							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-envelope"></i>
								<span>Correo Electrónico</span>
								<em>(requerido)</em>
								<input type="email" name="correo" id="correo" class="form-control" required placeholder="Correo Electrónico" />
							</div> 						
							
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-phone"></i>
								<span>Teléfonos</span>
								<em>(requerido)</em>
								<input type="text" name="telefono" id="telefono" class="form-control" required placeholder="Teléfonos" />
							</div>
							
							<div class="input-style bottom-10">						
								<div class="checkboxes-demo">
									<div class="">
										<center>
											<span>¿Visitante frecuente?</span>	
											<input id="frecuente" name="frecuente" type="checkbox" value="1">
										</center>
									</div>
								</div>
							</div>

							<div class="input-style input-style-1 input-required">
								
								<span class="input-style-1-active input-style-1-inactive">Fecha Inicio</span>
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_inicio_individual" name="fecha_inicio_individual" value="" required>
							</div>
							
							<div class="input-style input-style-1 input-required">
								<span class="input-style-1-active input-style-1-inactive">Fecha Fin</span>
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_fin_individual" name="fecha_fin_individual" value="" required>
							</div>			
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_individual" name="registro_individual" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Guardar y Generar QR</button>
							</center>	
						</form>
					
						</div>
						<div class="tab-content" id="tab-2">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
						
							<input type="hidden" name="event_type" value="EVENTO"/>
						
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Nombre del Evento</span>
								<em>(requerido)</em>
								<input type="text" name="nombre_evento" id="nombre_evento" class="form-control" required placeholder="Nombre del Evento" />
							</div>	
							
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Número de Invitados</span>
								<em>(requerido)</em>
								<input type="number" name="numero_invitados" id="numero_invitados" class="form-control" required placeholder="Número de Invitados" />
							</div>								

							<div class="input-style input-style-1 input-required">
								
								<span class="input-style-1-active input-style-1-inactive">Fecha Inicio</span>
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_inicio_evento" name="fecha_inicio_evento" value="" required>
							</div>
							
							<div class="input-style input-style-1 input-required">
								<span class="input-style-1-active input-style-1-inactive">Fecha Fin</span>
								<em><i class="fa fa-angle-down"></i></em>
								<input type="datetime-local" id="fecha_fin_evento" name="fecha_fin_evento" value="" required>
							</div>			
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_evento"  name="registro_evento" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Guardar y Generar QR</button>
							</center>	
						</form>					
							
						</div>										
                </div>
            </div>
	
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="" style=""></div>
        </div> 


		<?php } ?>
		
		
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div> 

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-green1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php?id_evento=<?php echo $id_event; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-green1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>

	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>

    <div id="action-maps"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="374"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Ubicación del Evento</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Mapa detallado del Evento</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class='responsive-iframe max-iframe bottom-0'>
		
			<?php $address = "https://maps.google.com/maps?q=".$r_address."&t=&z=13&ie=UTF8&iwloc=&output=embed"; ?>
		
			<iframe src='<?= $address ?>' frameborder='0' allowfullscreen>
			
			</iframe>
		
		</div>
    </div> 

	<?php } ?>


<div id="action-share" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="390" data-menu-effect="menu-over">
	
	<div class="content bottom-0">
        <img src="../app/images/logo-dark.png" class="center-text responsive-image-full top-20 bottom-20" />
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>

        <div class="landing-icons">
            <a href="#" class="shareToWhatsAppQR">
                <img src="../app/images/iconos/compartir/whatsapp.png" />
                <em>Whatsapp</em>
            </a>
            <a href="#" class="shareToFacebookQR">
                <img src="../app/images/iconos/compartir/facebook.png" />
                <em>Facebook</em>
            </a>
            <a href="#" class="shareToMessengerQR">
                <img src="../app/images/iconos/compartir/messenger.png" />
                <em>Messenger</em>
            </a>

            <div class="clear"></div>
        </div>

        <div class="landing-icons">
            <a href="#" class="shareToInstagramQR">
                <img src="../app/images/iconos/compartir/instagram.png" />
                <em>Instagram</em>
            </a>
            <a href="#" class="shareToWhatsAppQR">
                <img src="../app/images/iconos/compartir/whatsappbusiness.png" />
                <em>Whatsapp Business</em>
            </a>
            <a href="#" class="shareToMailQR">
                <img src="../app/images/iconos/compartir/gmail.png" />
                <em>Gmail</em>
            </a>

            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript" src="../app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	<?php if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	
	
	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>
			
		generateQR(<?= $meeting_id ?>);
		
		var meeting_id = <?= $meeting_id ?>;
		
		var documento = "https://okhus.com/mobile/app_qr.php?id_evento="+meeting_id;
		
		var imagen = $("#qr-auth-data").val();

		console.log(imagen);
		
		$(".shareToFacebookQR").prop("href", "https://www.facebook.com/sharer/sharer.php?u=" + documento);
		$(".shareToFacebookQR").attr("target", "_blank");			
		
		$(".shareToMailQR").prop("href", "mailto:?body=" + documento);
		$(".shareToMailQR").attr("target", "_blank");
		
		$(".shareToWhatsAppQR").prop("href", "whatsapp://send?text=" + documento);
		$(".shareToWhatsAppQR").attr("target", "_blank");
		
		$(".shareToMessengerQR").prop("href", "fb-messenger://share?link=" + documento);
		$(".shareToMessengerQR").attr("target", "_blank");
		
		/*document.querySelector(".shareToWhatsAppQR").onclick = () => {
			fetch(imagen)
				.then(function (response) {
					return response.blob();
				})
				.then(function (blob) {
					var file = new File([blob], "invitacion.png", { type: "image/png" });
					var filesArray = [file];

					if (navigator.canShare && navigator.canShare({ files: filesArray })) {
						navigator
							.share({
								text: "Código QR para invitación",
								files: filesArray,
								title: "Invitación QR",
							})
							.then(() => {
								
							})
							.catch((err) => {
								console.log("Unsuccessful share");
								console.log("Share failed: " + err.message);
							});
					} else {
						console.log("File sharing is not supported");
					}
				});
		};*/

	<?php } ?>

	});
	
	function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
	  
	  console.log(blob); 
	  
      return blob;
}

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
	$("#qr-auth-data").val(document.getElementById("qr-auth").src)
    $("#qr-auth").addClass("shadow-huge");
    //$("#qr-auth").css("width", "50%");
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>