<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_rented_tenant_details.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_common.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

$total_rent = '0.00';
$monthly_bill_data = '0,0,0,0,0,0,0,0,0,0,0,0';
var_dump(rent);
$result_amount = mysqli_query($link,"SELECT sum(rent) as total FROM tbl_add_fair where bill_status = 0 and rid =".(int)$_SESSION['objLogin']['rid']);
if($row_amount_total = mysqli_fetch_array($result_amount)){
	if((float)$row_amount_total['total'] > 0){
		$total_rent = $row_amount_total['total'];
	}
}

if(isset($_POST['registro_payment'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];

			$payment_image = ($_POST['payment_image'] != "") ? mysqli_real_escape_string($link, $_POST['payment_image']): NULL;

			$sql = "UPDATE tbl_add_fair SET 
					payment_description = '$_POST[payment_description]',
					payment_amount = '$_POST[payment_amount]',
					payment_reference = '$_POST[payment_reference]',
					payment_image = '$_POST[payment_image]'
					WHERE f_id = $_POST[f_id]";
										
			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
	
			if($add) {
			
				$id_event = mysqli_affected_rows($link);
			
			}

			mysqli_commit($link);
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.color-white1 {
    color: #FFFFFF!important;
}

.header {
    background-color: #FFF!important;
}

.link-list-3 em {
    position: absolute;
    font-size: 12px;
    right: 0px;
    margin-top: -21px;
    width: 100px;
    text-transform: uppercase;
    font-weight: 500;
    text-align: center;
    font-style: normal;
    border-radius: 14px;
}

.color-white-balance {
    color: #FFFFFF!important;
}

.detail td {
    text-align: left!important;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}

.header {
    background-color: #FFF!important;
}


</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-arrow-left"></i></a>
	</div>
	
	<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
		<div class="clear"></div>
	</div>		
         
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

<div class="content">
        <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">						
        <h4 class="text-center bottom-20">Estados de Cuenta</h4>	
</div>

<div class="content content-box bg-gradient-blue2 round-medium shadow-huge">
    <p class="center-text color-white-balance font-20 bottom-20">Balance por Pagar</p>
    <h1 class="center-text color-white-balance bolder top-20 bottom-20"><?php echo $ams_helper->currency($localization, $total_rent); ?></h1>
    <a href="#" data-menu="action-payment-add" class="bottom-20 button button-center-small button-xs button-circle bg-white color-black">Reporte de Pago</a>
</div>

<div class="content content-box shadow-huge round-medium accordion-style-1 accordion-round-medium">	
        	<?php
				$result = mysqli_query($link,"Select *,fl.floor_no as fl_floor,u.unit_no as u_unit,r.r_name,m.month_name from tbl_add_fair f inner join tbl_add_floor fl on fl.fid = f.floor_no inner join tbl_add_unit u on u.uid = f.unit_no inner join tbl_add_rent r on r.r_unit_no = f.unit_no inner join tbl_add_month_setup m on m.m_id = f.month_id where f.unit_no = ".(int)$_SESSION['objLogin']['r_unit_no']." and f.branch_id = ".(int)$_SESSION['objLogin']['branch_id']." order by f.month_id ASC");
				while($row = mysqli_fetch_assoc($result)){
					$image = WEB_URL . 'img/no_image.jpg';	
			if(file_exists(ROOT_PATH . '/img/upload/' . $_SESSION['objLogin']['image']) && $_SESSION['objLogin']['image'] != ''){
				$image = WEB_URL . 'img/upload/' . $_SESSION['objLogin']['image'];
			}
			?>	

    <a href="#" data-accordion="accordion-content-<?php echo $row['f_id']; ?>" class="bg-blue2-dark round-medium">
        <i class="accordion-icon-left fas fa-file-alt"></i>
        <?php echo $row['month_name']; ?>, <?php echo $row['xyear']; ?>
        <i class="accordion-icon-right fa fa-plus"></i>
    </a>
		<div id="accordion-content-<?php echo $row['f_id']; ?>" class="accordion-content content content-box">
			<table class="table-borders no-border">
				<tbody>
					<tr>
								<td>Mes <?php echo $row['month_name']; ?></td>
								<td><?php echo $ams_helper->currency($localization, $row['total_rent']); ?></td>
							</tr>
							<tr>
								<td><?php echo date("F j, Y",strtotime($row["issue_date"])); ?></td>
								<td><?php echo ($row['bill_status']=='1') ? '<label style="margin-bottom: 0px!important;" class="bg-green2-dark button button-xxs button-round-small">'.$_data['text_21'].'</label>' : '<label style="margin-bottom: 0px!important;" class="bg-red2-dark button button-xxs button-round-small">'.$_data['text_22'].'</label>'; ?></td>
					</tr>		
				</tbody>
			</table>
			<h4 class="text-center">Detalles del Residente</h4>
			<table class="detail table-borders no-border">
				<tbody>
				
					<tr>				
						<td><b><?php echo $_data['text_5'];?></b></td>
						<td><?php echo $_SESSION['objLogin']['r_name']; ?></td>
					</tr>					
					<tr>				
						<td><b><?php echo $_data['text_6'];?></b></td>
						<td><?php echo $_SESSION['objLogin']['r_email']; ?></td>
					</tr>	
					<tr>				
						<td><b><?php echo $_data['text_7'];?></b></td>
						<td><?php echo $_SESSION['objLogin']['r_contact']; ?></td>
					</tr>	
					<tr>				
						<td><b><?php echo $_data['text_8'];?></b></td>
						<td><?php echo $_SESSION['objLogin']['r_address']; ?></td>
					</tr>	
					<tr>				
						<td><b><?php echo $_data['text_9'];?></b></td>
						<td><?php echo $row['fl_floor']; ?></td>
					</tr>	
					<tr>				
						<td><b><?php echo $_data['text_10'];?></b></td>
						<td><?php echo $row['u_unit']; ?></td>
					</tr>	
					<tr>				
						<td><b><?php echo $_data['text_2'];?></b></td>
						<td><?php echo $row['month_name']; ?></td>
					</tr>
					<tr>				
						<td><b><?php echo $_data['text_11'];?></b></td>
						<td><?php echo $ams_helper->currency($localization, $row['rent']); ?></td>
					</tr>							
						
				</tbody>
			</table>

			<!--h4 class="text-center">Detalles de la Cuenta</h4>
			<table class="detail table-borders no-border">
				<tbody>
				
					<tr>				
						<td><b><?php //echo $_data['text_12'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['water_bill']); ?></td>
					</tr>					
					<tr>				
						<td><b><?php //echo $_data['text_13'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['electric_bill']); ?></td>
					</tr>	
					<tr>				
						<td><b><?php //echo $_data['text_14'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['gas_bill']); ?></td>
					</tr>	
					<tr>				
						<td><b><?php //echo $_data['text_15'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['security_bill']); ?></td>
					</tr>	
					<tr>				
						<td><b><?php //echo $_data['text_16'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['utility_bill']); ?></td>
					</tr>	
					<tr>				
						<td><b><?php //echo $_data['text_17'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['other_bill']); ?></td>
					</tr>	
					<tr>				
						<td><b><?php //echo $_data['text_18'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['total_rent']); ?></td>
					</tr>
					<tr>				
						<td><b><?php //echo $_data['text_4'];?></b></td>
						<td><?php //echo $ams_helper->currency($localization, $row['issue_date']); ?></td>
					</tr>							
						
				</tbody>
			</table-->
			
			<?php if($row['payment_description'] !== NULL) { ?>
			
			<h4 class="text-center">Detalles del Pago</h4>
			<table class="detail table-borders no-border">
				<tbody>
				
					<tr>				
						<td><b>Descripción</b></td>
						<td><?php echo $row['payment_description']; ?></td>
					</tr>					
					<tr>				
						<td><b>Monto Pagado</b></td>
						<td><?php echo $row['payment_amount']; ?></td>
					</tr>	
					<tr>				
						<td><b>Referencia</b></td>
						<td><?php echo $row['payment_reference']; ?></td>
					</tr>							
				</tbody>
			</table>
			<h4 class="text-center">Comprobante de Pago</h4>
			<img style="border: 1px solid #ccc!important; padding: 5px!important;" src="<?php echo $row["payment_image"];?>" class="center-text responsive-image bottom-20" />			
			<?php } ?>		
        </div>
	</br>
<?php } ?>	

</div>		


	</div>

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div> 

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-blue1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_tenant_details.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue1-dark">Continuar</a>
    </div>
	
    <div id="action-info"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-blue2-dark"><i class="fas fa-info-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Información</h1>        
            <p class="font-14 boxed-text-large">
                Ya el comprobante a esta factura fue enviado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_tenant_details.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-blue2-dark">Continuar</a>
    </div>   	

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_tenant_details.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>
	
    <div id="action-payment-add" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="500" data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Registro de Pago</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Estado de Cuenta</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>        
        
        <div class="content">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
						
						<div class="input-style input-style-2 input-required">
							<span>Seleccione un Pago</span>
							<em><i class="fa fa-angle-down"></i></em>
							<select name="f_id" id="f_id" required>
								<option value="" disabled selected>Seleccione un Pago</option>
						  <?php 

								$result = mysqli_query($link,"Select *,fl.floor_no as fl_floor,u.unit_no as u_unit,r.r_name,m.month_name from tbl_add_fair f inner join tbl_add_floor fl on fl.fid = f.floor_no inner join tbl_add_unit u on u.uid = f.unit_no inner join tbl_add_rent r on r.r_unit_no = f.unit_no inner join tbl_add_month_setup m on m.m_id = f.month_id where f.unit_no = ".(int)$_SESSION['objLogin']['r_unit_no']." and f.branch_id = ".(int)$_SESSION['objLogin']['branch_id']." and f.bill_status = 0 order by f.month_id ASC");
								while($row = mysqli_fetch_assoc($result)){
													
							?>
							<option value="<?php echo $row['f_id'];?>"><?php echo $row['month_name']; ?>, <?php echo $row['xyear']; ?></option>
						  <?php } ?>
							</select>
						</div>							
						
						<input type="hidden" name="notice_type" value=""/>
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Descripción</span>
								<em>(requerido)</em>
								<input type="text" name="payment_description" id="payment_description" class="form-control" required placeholder="Descripción" />
							</div>									

							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fas fa-money-bill-wave"></i>
								<span>Monto Pagado</span>
								<em>(requerido)</em>
								<input type="text" name="payment_amount" id="payment_amount" class="form-control" onkeypress="return OnlyNumericXDecimal(event,this,2,'.')" required placeholder="0.00" />
							</div>
							
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fas fa-money-check"></i>
								<span>Referencia</span>
								<em>(requerido)</em>
								<input type="text" name="payment_reference" id="payment_reference" class="form-control" required placeholder="Referencia" />
							</div>														

							<div class="file-data top-20">
								<input type="file" id="set_payment_image" class="upload-file button bg-green1-dark button-full shadow-huge round-small button-xs " accept="image/png, image/jpeg" required>
								<p class="upload-file-text">Subir un Comprobante</p>
								<img src="../app/images/empty.png" style="margin: auto;">
								<input type="hidden" id="payment_image" name="payment_image" value=""/>
							</div>								
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_payment"  name="registro_payment" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Registrar Aviso</button>
							</center>	
						</form>		
        </div>
    </div>  		

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript" src="../app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	$('input[name="notice_type"]').val($('input[name="notices_type"]').val());
		
	<?php if((isset($_POST['registro_payment']) || isset($_POST['registro_payment'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_payment']) || isset($_POST['registro_payment'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } else if((isset($_POST['registro_payment']) || isset($_POST['registro_payment'])) && $add == true && $id_event == 0) { ?>
	
		activateMenu('action-info');
	
	<?php } ?>

	});
	
function OnlyNumericXDecimal(e, txt, decimales, caracter) {
        var valor = txt.value;
        var charCode;
        if (navigator.appName == 'Netscape')
            charCode = e.which;
        else
            charCode = e.keyCode;
        if (charCode == 13)
            return true;
        if (charCode == 8)
            return true;

        if (charCode == 0)
            return true;
        var str = valor;

        var chars = "." + caracter + "0123456789";
        if (chars.indexOf(String.fromCharCode(charCode)) == -1)
            return false;

        if (String.fromCharCode(charCode) == caracter || String.fromCharCode(charCode) == ".")
        {
            if (str == "")
                return false;
            if (str.indexOf(".") > 0)
                return false;
            if (str.indexOf(caracter) > 0)
                return false;
        }

        pos = str.indexOf('.');

        if (pos == -1)
            pos = str.indexOf(caracter);

        if (pos > 0) //parte decimal
        {
            if (valor.substr(pos).length > decimales)
                return false;
        } else //parte entera
        {
            if (valor.length > 14)
                txt.value = valor + ",";
        }
        return true;
    }
	
function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
    $("#qr-auth").addClass("shadow-huge");
    //$("#qr-auth").css("width", "50%");
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>