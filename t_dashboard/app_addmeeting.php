<?php 
include('../app_header_ten.php');
include('../utility/common.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_add_meeting.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}

if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

$add = false;
$id_event = false;
$event_count = false;

//var_dump($ams_helper->dump($_SESSION));die;

//var_dump($ams_helper->dump($_POST));die;

if(isset($_POST['registro_individual'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];
			
			$visitor_image = NULL;
			$visitor_ife =  NULL;
			
			if($_POST['frecuente']) {
			
				$visitor_image = ($_POST['image_visitor'] != "") ? mysqli_real_escape_string($link, $_POST['image_visitor']): NULL;
				$visitor_ife = ($_POST['ife'] != "") ? mysqli_real_escape_string($link, $_POST['ife']): NULL;

			}
				
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					visitor_name,
					visitor_email,
					visitor_phone,
					visitor_frequent,
					visitor_ife,
					visitor_image,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[nombres]',
					'$_POST[correo]',
					'$_POST[telefono]',
					'$_POST[frecuente]',
					'$visitor_ife',
					'$visitor_image',
					'$_POST[fecha_inicio_individual]',		
					'$_POST[fecha_fin_individual]');";
					
			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
			
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

if(isset($_POST['registro_evento'])){
	
			$branch_id = $_SESSION['objLogin']['branch_id'];
			$rid = $_SESSION['objLogin']['rid'];	
	
			$sql = "INSERT INTO tbl_meeting(branch_id,
					id_rent,
					event_type,
					event_name,
					event_visitors,
					start_date,
					end_date) 
					values(
					'$branch_id',
					'$rid',
					'$_POST[event_type]',
					'$_POST[nombre_evento]',
					'$_POST[numero_invitados]',
					'$_POST[fecha_inicio_evento]',					
					'$_POST[fecha_fin_evento]')";

			mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
			
			$add = mysqli_query($link, $sql);
			
			if($add) {
			
				$id_event = mysqli_insert_id($link);		
			
			}
						
			mysqli_commit($link);
																
			mysqli_close($link);
			
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/datepicker.min.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(50%);
    overflow: hidden;
}

.responsive-image {
	margin: auto;
}


.header {
    background-color: #FFF!important;
}

.caption-background, .caption-bg {
    position: relative!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0) { ?>

	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="back-button header-icon header-icon-1 color-white back-button font-18 left-10"><i class="fas fa-arrow-left"></i></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php" class="header-icon header-icon-2 color-white back-button font-18 left-10"><i class="fas fa-bars font-13"></i></a>		
	</div>

<?php } else { ?>	
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_t_dashboard.php" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-arrow-left"></i></a>
		<a href="<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php" class="header-icon header-icon-2 color-white back-button font-30 left-10"><i class="far fa-list-alt"></i></a>
	
	</div>

<?php } ?>	

	
<div class="footer-menu footer-2-icons shadow-huge shadow-huge">
	<a href="<?php echo WEB_URL; ?>app_t_dashboard.php"><i class="fa fa-home"></i><span>Inicio</span></a>
	<a href="<?php echo WEB_URL; ?>app_logout.php"><i class="fas fa-sign-out-alt"></i><span>Cerrar Sesión</span></a>
	<div class="clear"></div>
</div>		
	                 
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">  

				<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){
			
					$result = mysqli_query($link,"SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where meeting_id = '" . $_GET['id_evento'] . "' LIMIT 1");					
					$event_count = mysqli_num_rows($result);	

					if($event_count != false && $event_count > 0) {
					
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					
					$meeting_id = $row["meeting_id"];
					$visitor_name = $row["visitor_name"];
					$visitor_email = $row["visitor_email"];
					$visitor_phone = $row["visitor_phone"];
					$id_invitacion = $row["meeting_id"];
					$r_address = $row["r_address"];
					$fecha_inicio_individual = date("d-m-Y h:m:s a",strtotime($row["start_date"]));
					$fecha_fin_individual = date("d-m-Y h:m:s a",strtotime($row["end_date"]));
					
					mysqli_close($link);
					
				?>		

		<div id="qr" data-height="cover-header" class="caption" style="display: block;">
			
			<div class="content content-box">

				<img src="../app/images/logo-dark.png" class="center-text responsive-image-full bottom-20" />
			
				<h4 class="text-center">Detalle de la Invitación #<?= $meeting_id ?></h4>
				<p class="text-center">
					En esta sección se muestra el Código QR de la Invitación y la Ubicación.
				</p>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa fa-user color-blue1-dark"></i>
							<strong>Nombre del Visitante</strong>
							<span><?= $visitor_name ?></span>
						</a>
					</div>
				</div>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa-calendar-alt color-blue1-dark"></i>
							<strong>Hora de Inicio / Fin</strong>
							<span><?= $fecha_inicio_individual ?></span>
							<span><?= $fecha_fin_individual ?></span>
						</a>
					</div>
				</div>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fa fa-mobile color-blue1-dark"></i>
							<strong>Telefonos</strong>
							<span><?= $visitor_phone ?></span>
						</a>
					</div>
				</div>

				<div class="">
					<div class="link-list link-list-3">
						<a href="#" class="round-small shadow-tiny">
							<i class="fas fa-id-badge color-blue1-dark"></i>
							<strong>Identificación</strong>
							<span><?= $id_invitacion ?></span>
						</a>
					</div>
				</div>

				<div class="link-list link-list-3">
					<a href="#" class="round-small shadow-tiny">
						<i class="fas fa-at color-blue1-dark"></i>
						<strong>Email</strong>
							<span><?= $visitor_email ?></span>
					</a>
				</div>
				
				<h4 class="text-center">QR para invitaciones</h4>
				<p class="text-center">
					Código QR valido para invitaciones de OKHUS.
				</p>
				
				<img id="qr-auth" src="../app/images/page-loader.gif" class="center-text responsive-image-full bottom-20" />

				<div class="landing-icons">
					<a href="#" data-menu="event_share">
						<img src="../app/images/iconos/invitacion/compartir.png" />
						<em>Compartir</em>
					</a>
					<a href="#" data-menu="action-maps" style="float: right;">
						<img src="../app/images/iconos/invitacion/ubicacion.png" style="" />
						<em>Ubicación</em>
					</a>
				</div>				
			</div>
			
				<?php } else { ?>
						
				<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en el Detalle de la Invitación</h4>
					<p class="text-center">
						No se han encontrado ninguna información de la Invitación buscada.
					</p>
					<center>
						<a href="<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					</center>
				</div>				
			<?php } ?>
		</div>
				
					
		<?php }  else { ?>

        <div id="registro" data-height="cover-header" class="caption" style="display:block;">  

            <div class="content left-20 right-20">
                <div class="content content-box content-box-full-top">
                    <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">

					<p class="under-heading font-15 color-theme opacity-70 bottom-10">Ingresar detalles para el Registro de Residente</p>				
					
						<div class="tab-controls tab-animated tabs-large tabs-rounded" 
							 data-tab-items="2" 
							 data-tab-active="bg-green1-dark">
							<a href="#" data-tab-active data-tab="tab-1"><i class="far fa-user"></i> Individual</a>
							<a href="#" data-tab="tab-2"><i class="fas fa-users"></i> Evento</a>
						</div>
						<div class="clear bottom-15"></div>
						<div class="tab-content" id="tab-1">
						
						<form method="post" enctype="multipart/form-data" id="invitacion_individual">
						
							<input type="hidden" name="event_type" value="INDIVIDUAL"/>
						
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Nombre del Visitante</span>
								<em>(requerido)</em>
								<input type="text" name="nombres" id="nombres" class="form-control" required placeholder="Nombres" />
							</div>	

							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-envelope"></i>
								<span>Correo Electrónico</span>
								<em>(requerido)</em>
								<input type="email" name="correo" id="correo" class="form-control" required placeholder="Correo Electrónico" />
							</div> 						
							
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-phone"></i>
								<span>Teléfonos</span>
								<em>(requerido)</em>
								<input type="text" name="telefono" id="telefono" class="form-control" required placeholder="Teléfonos" />
							</div>
							
							<div class="input-style bottom-10">						
								<div class="checkboxes-demo">
									<div class="">
										<center>
											<span>¿Visitante frecuente?</span>	
											<input id="frecuente" name="frecuente" type="checkbox" value="1">
										</center>
									</div>
								</div>
							</div>
							
							<div id="visitante-frecuente" style="display:none;">				

							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fas fa-id-card"></i>
								<span>IFE</span>
								<em>(requerido)</em>
								<input type="text" name="ife" id="ife" class="form-control" placeholder="IFE" />
							</div>
				

								<div class="file-data top-20">
									<input type="file" id="set_image_visitor" class="upload-file button bg-green1-dark button-full shadow-huge round-small button-xs " accept="image/png, image/jpeg">
									<p class="upload-file-text">Subir una Imagen</p>
									<img src="../app/images/empty.png">
									<input type="hidden" id="image_visitor" name="image_visitor" value=""/>
								</div>
								<div class="divider divider-margins bottom-0"></div>
								<!--div class="link-list link-list-2 link-list-long-border upload-file-data disabled">
									<a href="#">
										<i class="fa fa-info-circle color-blue2-dark"></i>
										<span>Nombre del Archivo</span>
										<strong class="font-12 upload-file-name">Flexible, Fast, and Very Powerful</strong>
										<i class="fa fa-angle-right"></i>
									</a>
									<a href="#">
										<i class="fa fa-weight-hanging color-brown1-dark"></i>
										<span>Tamaño del Archivo</span>
										<strong class="font-12 upload-file-size">Colorful, easy to use and beautiful</strong>
										<i class="fa fa-angle-right"></i>
									</a>    
									<a href="#">
										<i class="fa fa-tag color-red2-dark"></i>
										<span>Tipo de Archivo</span>
										<strong class="font-12 upload-file-type">Colorful, easy to use and beautiful</strong>
										<i class="fa fa-angle-right"></i>
									</a>
									<a href="#">
										<i class="fa fa-clock color-green1-dark"></i>
										<span>Actualizado el:</span>
										<strong class="font-12 upload-file-modified">Designed to feel iOS like</strong>
										<i class="fa fa-angle-right"></i>
									</a>
								</div-->  						

							</div>
								<span class="input-style-1-active input-style-1-inactive">Fecha Inicio</span>

							<div class="input-style input-style-1 input-required">
								
								<input type="text" id="fecha_inicio_individual" data-toggle="datepicker" name="fecha_inicio_individual" value="" required>
							</div>
								<span class="input-style-1-active input-style-1-inactive">Fecha Fin</span>
							
							<div class="input-style input-style-1 input-required">
								<em><i class="fa fa-angle-down"></i></em>
								<input type="text" id="fecha_fin_individual" data-toggle="datepicker" name="fecha_fin_individual" value="" required>
							</div>			
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_individual" name="registro_individual" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Guardar y Generar QR</button>
							</center>	
						</form>
					
						</div>
						<div class="tab-content" id="tab-2">
						<form method="post" enctype="multipart/form-data" id="invitacion_evento">
						
							<input type="hidden" name="event_type" value="EVENTO"/>
						
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Nombre del Evento</span>
								<em>(requerido)</em>
								<input type="text" name="nombre_evento" id="nombre_evento" class="form-control" required placeholder="Nombre del Evento" />
							</div>	
							
							<div class="input-style has-icon input-style-1 input-required">
								<i class="input-icon fa fa-user"></i>
								<span>Número de Invitados</span>
								<em>(requerido)</em>
								<input type="number" name="numero_invitados" id="numero_invitados" class="form-control" required placeholder="Número de Invitados" />
							</div>								
								<span class="input-style-1-active input-style-1-inactive">Fecha Inicio</span>

							<div class="input-style input-style-1 input-required">
								
								<em><i class="fa fa-angle-down"></i></em>
								<input type="text" data-toggle="datepicker" id="fecha_inicio_evento" name="fecha_inicio_evento" value="" required>
							</div>
								<span class="input-style-1-active input-style-1-inactive">Fecha Fin</span>
							
							<div class="input-style input-style-1 input-required">
								<em><i class="fa fa-angle-down"></i></em>
								<input type="text" id="fecha_fin_evento" name="fecha_fin_evento" data-toggle="datepicker" value="">
							</div>			
							<div class="clear"></div>
							<center>
								<button type="submit" id="registro_evento"  name="registro_evento" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Guardar y Generar QR</button>
							</center>	
						</form>					
							
						</div>										
                </div>
            </div>
	
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="" style=""></div>
        </div> 


		<?php } ?>
		
		
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div> 

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-green1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_viewmeeting.php?id_evento=<?php echo $id_event; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-green1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>

	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>

    <div id="action-maps"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="374"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Ubicación del Evento</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Mapa detallado del Evento</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class='responsive-iframe max-iframe bottom-0'>
		
			<?php $address = "https://maps.google.com/maps?q=".$r_address."&t=&z=13&ie=UTF8&iwloc=&output=embed"; ?>
		
			<iframe src='<?= $address ?>' frameborder='0' allowfullscreen>
			
			</iframe>
		
		</div>
    </div> 

	<?php } ?>


<div id="event_share" class="menu-box menu-box-detached round-medium" data-menu-type="menu-box-bottom" data-menu-height="390" data-menu-effect="menu-over">
	<div class="content bottom-0">
        <img src="../app/images/logo-dark.png" class="center-text responsive-image-full top-20 bottom-20" />
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>

        <div class="landing-icons">
            <a href="#">
                <img src="../app/images/iconos/compartir/whatsapp.png" />
                <em>Whatsapp</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/facebook.png" />
                <em>Facebook</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/messenger.png" />
                <em>Messenger</em>
            </a>

            <div class="clear"></div>
        </div>

        <div class="landing-icons">
            <a href="#">
                <img src="../app/images/iconos/compartir/instagram.png" />
                <em>Instagram</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/whatsappbusiness.png" />
                <em>Whatsapp Business</em>
            </a>
            <a href="#">
                <img src="../app/images/iconos/compartir/gmail.png" />
                <em>Gmail</em>
            </a>

            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript" src="../app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript" src="../app/scripts/datepicker.min.js"></script>
<script type="text/javascript" src="../app/scripts/datepicker.es.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		$("[data-toggle=\"datepicker\"").datepicker({
			timepicker: true,
			timeFormat: "hh:mm"
		}).on("keydown", function (e) {
			e.preventDefault()
		});
	<?php if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == true && $id_event != false) { ?>

		activateMenu('action-add');

	<?php } else if((isset($_POST['registro_individual']) || isset($_POST['registro_evento'])) && $add == false && $id_event == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	
	
	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>
		
		generateQR(<?= $meeting_i ?>);

	<?php } ?>

	});

function activateMenu(menuID) {
    var body = $("body");
    var header = $(".header-fixed");
    var pageContent = $(".page-content");
    var footerMenu = $(".footer-menu");
    var menu = $(".menu-box");
    var menuHider = $(".menu-hider");
    var menuClose = $(".menu-close, .menu-hider, .close-menu");
    var menuDeployer = $("[data-menu]");
    var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
    var menuSelected = $("[data-menu-selected]").data("menu-selected");

    var menuID = $("#" + menuID);

    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-3d");
    menuHider.css({
        transform: "translate(0,0)",
    });
    var menuType = menuID.data("menu-type");
    var menuWidth = menuID.data("menu-width");
    var menuHeight = menuID.data("menu-height");
    var menuEffect = menuID.data("menu-effect");
    var menuWidthParallax = menuID.data("menu-width") / 3;
    var menuHeightParallax = menuID.data("menu-height") / 3;
    if (menuEffect === "menu-push") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeight * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-parallax") {
        if (menuType === "menu-box-top") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax + "px)",
            });
        }
        if (menuType === "menu-box-bottom") {
            menuPushElements.css({
                transform: "translateY(" + menuHeightParallax * -1 + "px)",
            });
        }
        if (menuType === "menu-box-left") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuPushElements.css({
                transform: "translateX(" + menuWidthParallax * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-reveal") {
        if (menuType === "menu-box-left") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth + "px)",
            });
        }
        if (menuType === "menu-box-right") {
            menuID.show(0);
            menuHider.addClass("menu-hider-reveal");
            menuPushElements.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
            menuHider.css({
                transform: "translateX(" + menuWidth * -1 + "px)",
            });
        }
        menuID.addClass("menu-active");
    }
    if (menuEffect === "menu-over") {
        menuID.addClass("menu-active");
    }
    if (menuType === "menu-box-modal") {
        menuID.addClass("menu-active");
    }
    menuHider.addClass("menu-hider-visible");
}

function disableMenu() {
    setTimeout(function () {
        $('[data-menu-effect="menu-reveal"]').hide(0);
    }, 150);
    menuPushElements.css({
        transform: "translate(0,0)",
    });
    header.removeClass("menu-hide-header");
    footerMenu.removeClass("menu-hide-footer");
    menu.removeClass("menu-active");
    menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
    menuHider.css({
        transform: "translate(0,0)",
    });
    menu.delay(250).animate(
        {
            scrollTop: 0,
        },
        350
    );
}

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
    $("#qr-auth").addClass("shadow-huge");
    //$("#qr-auth").css("width", "50%");
}

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>