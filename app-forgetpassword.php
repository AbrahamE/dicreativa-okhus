<?php
include("app_config.php");
include("library/encryption.php");
include("library/helper.php");
$converter = new Encryption;
$helper = new ams_helper;
$msg = false;
$send_email = false;
$sql = '';
///////////////////////////////////////////////////////////////////////////////
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
}

include(ROOT_PATH.'language/'.$lang_code_global.'/lang_index.php');

$xMsg = false;
if(isset($_POST['username']) && !empty($_POST['username'])){
	$user_name = make_safe($link, $_POST['username']); //Escaping Strings
	//
	$password = '';
	$name = '';
	//
	if($_POST['ddlLoginType'] == '1'){
		//here for admin
		$sql = mysqli_query($link, "SELECT * FROM tbl_add_admin WHERE email = '".$user_name."'");
		if($row = mysqli_fetch_assoc($sql)){
			$password = $converter->decode($row['password']);
			$name = $row['name'];
		}
	}
	else if($_POST['ddlLoginType'] == '2'){
		//here for owner
		$sql = mysqli_query($link, "SELECT * FROM tbl_add_owner WHERE o_email = '".$user_name."'");
		if($row = mysqli_fetch_assoc($sql)){
			$password = $converter->decode($row['o_password']);
			$name = $row['o_name'];
		}
	}
	else if($_POST['ddlLoginType'] == '3'){
		//here for employee
		$sql = mysqli_query($link, "SELECT * FROM tbl_add_employee WHERE e_email = '".$user_name."'");
		if($row = mysqli_fetch_assoc($sql)){
			$password = $converter->decode($row['e_password']);
			$name = $row['e_name'];
		}
	}
	else if($_POST['ddlLoginType'] == '4'){
		//here for renter
		$sql = mysqli_query($link, "SELECT * FROM tbl_add_rent WHERE r_email = '".$user_name."'");
		if($row = mysqli_fetch_assoc($sql)){
			$password = $converter->decode($row['r_password']);
			$name = $row['r_name'];
		}
	}
	else if($_POST['ddlLoginType'] == '5'){
		//here for renter
		$sql = mysqli_query($link, "SELECT * FROM tblsuper_admin WHERE email = '".$user_name."'");
		if($row = mysqli_fetch_assoc($sql)){
			$password = $converter->decode($row['password']);
			$name = $row['name'];
		}
	}
	if(!empty($password)){
		$msg = true;
		$query_set = mysqli_query($link, "SELECT * FROM tblsuper_admin");
		if($admin = mysqli_fetch_assoc($query_set)){
			$replyEmail = $admin['email'];
			$to = trim($_POST['username']);
			$subject = 'Detalle de Inicio de Sesión';
			$variables = array(
				'subject'	=> $subject,
				'message'	=> $msg,
				'site_url'	=> WEB_URL,
				'user_name'	=> $_POST['username'],
				'password'	=> $password,
				'name'		=> $name
			);
			$message = loadForgotEmailTemplate('app_forget.html', $variables);

			$query_set = mysqli_query($link, "SELECT * FROM tbl_settings");
			
			if($localization = mysqli_fetch_assoc($query_set)){			
				
				$send_email = $helper->sendEmailByPhpMailerPublic($localization, $to, $subject, $message, $admin);
					
				$xMsg = ($send_email == true) ? $_data['send_email_text'] : "El correo electrónico con los detalles del inicio de sesión no se pudo enviar.";
				
			}
		}
	}
	else{
		$msg = true;
	}
}
function make_safe($con, $variable){
	$variable = mysqli_real_escape_string($con, strip_tags(trim($variable)));
	return $variable; 
}
function loadForgotEmailTemplate($temp_name, $variables = array()) {
	$template = file_get_contents(ROOT_PATH."partial/email_templates/".$temp_name);
	foreach($variables as $key => $value){
		$template = str_replace('{{ '.$key.' }}', $value, $template);
	}
	return $template;
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $_data['application_title']; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(100%);
    transform: translateX(0px);
    overflow: hidden;
}

.header {
    /*background-color: #FFF!important;*/
}

</style>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app.php" class="back-button header-icon header-icon-1 color-white back-button font-20 left-0"><i class="fa fa-home"></i></a>
	</div>
                  
    <div class="page-bg"><div></div></div>
    <div class="page-content">     
        <div data-height="cover-header" class="caption">
            <div class="caption-center left-20 right-20" style="top: 58%;">
                <div class="content content-box content-box-full-top content-box-full-bottom">
                    <img src="app/images/logo-dark.png" class="responsive-image-full bottom-20">
					<p class="under-heading font-15 color-theme opacity-70 bottom-10"><?php echo $_data['forgot_your_password']; ?></p>		
					
					<form role="form" id="form" method="post">
					
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-envelope"></i>
							<span><?php echo $_data['your_email']; ?></span>
							<em>(requerido)</em>
							<input type="email" name="username" id="username" class="form-control" required placeholder="<?php echo $_data['your_email']; ?>" />
						</div> 

						<div class="input-style input-style-1 input-required" style="display:none;">
							<em><i class="fa fa-check color-green1-dark"></i></em>
							<select name="ddlLoginType" onChange="mewhat(this.value);" id="ddlLoginType" class="form-control" required>
								<option selected value="<?php echo $_GET['tipo']; ?>"><?php echo $_data['user_'.$_GET['tipo']]; ?></option>
							</select>
						</div>

						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-13" onclick="window.location.href = '<?php echo WEB_URL; ?>app-index.php?tipo=<?php echo $_GET['tipo']; ?>'">Inicio de Sesión</a>
						</div>					
						
						
						<div class="clear"></div>
						<center>
						<button type="submit" id="login" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25"><?php echo $_data['btn_submit'];?></button>
						</center>
						
						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" onclick="window.location.href = '<?php echo WEB_URL; ?>rent/app_addrent.php'">Registrarme</a>
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-pilitica-bottom">Política de Privacidad</a>
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-termino-bottom">Termino de Condiciones</a>

						</div>							
						
					</form>
                </div>
            </div>
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="caption-bg" style=""></div>
        </div>         
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  	

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-green1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                <?php echo $xMsg; ?>
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app-index.php?tipo=4'"class="button button-margins button-center-large button-m round-small shadow-huge bg-green1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                <?php echo ($xMsg == false) ? $_data['no_info_found'] : $xMsg; ?>
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app-forgetpassword.php?tipo=<?php echo $_GET['tipo']; ?>'" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>  		

<div class="menu-hider"></div> 
</div>

<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	<?php if(isset($_POST['username']) && $msg == true && $send_email == true) { ?>

		activateMenu('action-add');

	<?php } else if(isset($_POST['username']) && ($msg == false || $send_email == false || $xMsg == false)) { ?>
	
		activateMenu('action-error');

	<?php } ?>	

	});

    function activateMenu(menuID) {
		
		var body = $("body");
		var header = $(".header-fixed");
		var pageContent = $(".page-content");
		var footerMenu = $(".footer-menu");
		var menu = $(".menu-box");
		var menuHider = $(".menu-hider");
		var menuClose = $(".menu-close, .menu-hider, .close-menu");
		var menuDeployer = $("[data-menu]");
		var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
		var menuSelected = $("[data-menu-selected]").data("menu-selected");	
		
        var menuID = $("#"+menuID);

        menuPushElements.css({
            transform: "translate(0,0)",
        });
        header.removeClass("menu-hide-header");
        footerMenu.removeClass("menu-hide-footer");
        menu.removeClass("menu-active");
        menuHider.removeClass("menu-hider-visible menu-hider-3d");
        menuHider.css({
            transform: "translate(0,0)",
        });
        var menuType = menuID.data("menu-type");
        var menuWidth = menuID.data("menu-width");
        var menuHeight = menuID.data("menu-height");
        var menuEffect = menuID.data("menu-effect");
        var menuWidthParallax = menuID.data("menu-width") / 3;
        var menuHeightParallax = menuID.data("menu-height") / 3;
        if (menuEffect === "menu-push") {
            if (menuType === "menu-box-top") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeight + "px)",
                });
            }
            if (menuType === "menu-box-bottom") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeight * -1 + "px)",
                });
            }
            if (menuType === "menu-box-left") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-parallax") {
            if (menuType === "menu-box-top") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeightParallax + "px)",
                });
            }
            if (menuType === "menu-box-bottom") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeightParallax * -1 + "px)",
                });
            }
            if (menuType === "menu-box-left") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidthParallax + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidthParallax * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-reveal") {
            if (menuType === "menu-box-left") {
                menuID.show(0);
                menuHider.addClass("menu-hider-reveal");
                menuPushElements.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
                menuHider.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuID.show(0);
                menuHider.addClass("menu-hider-reveal");
                menuPushElements.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
                menuHider.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-over") {
            menuID.addClass("menu-active");
        }
        if (menuType === "menu-box-modal") {
            menuID.addClass("menu-active");
        }
        menuHider.addClass("menu-hider-visible");
    }
	
    function disableMenu() {
        setTimeout(function () {
            $('[data-menu-effect="menu-reveal"]').hide(0);
        }, 150);
        menuPushElements.css({
            transform: "translate(0,0)",
        });
        header.removeClass("menu-hide-header");
        footerMenu.removeClass("menu-hide-footer");
        menu.removeClass("menu-active");
        menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
        menuHider.css({
            transform: "translate(0,0)",
        });
        menu.delay(250).animate(
            {
                scrollTop: 0,
            },
            350
        );
    }	

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>