<?php 
ob_start();
session_start();
include(__DIR__ . "/app_config.php");

//
if(!isset($_SESSION['objLogin'])){
	
	if(isset($_SESSION['is_mobile']) && $_SESSION['is_mobile'] == true) {		
		header("Location: ".WEB_URL."app_logout.php");
		die();			
	}
	
	header("Location: ".WEB_URL."logout.php");
	die();
}
//
// allow only for employee
if(!empty($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 3){
	
	if(isset($_SESSION['is_mobile']) && $_SESSION['is_mobile'] == true) {		
		header("Location: ".WEB_URL."app_logout.php");
		die();			
	}
	
	header("Location: " . WEB_URL . "logout.php");
	die();
}
//
$image = WEB_URL . 'img/no_image.jpg';	
if(isset($_SESSION['objLogin']['image'])){
	if(file_exists(ROOT_PATH . '/img/upload/' . $_SESSION['objLogin']['image']) && $_SESSION['objLogin']['image'] != ''){
		$image = WEB_URL . 'img/upload/' . $_SESSION['objLogin']['image'];
	}
}
$desig = '';
$resultx = mysqli_query($link,"SELECT member_type FROM tbl_add_member_type where member_id =".(int)$_SESSION['objLogin']['e_designation']);
if($rowx = mysqli_fetch_array($resultx)){
	$desig = $rowx['member_type'];
}

include(ROOT_PATH.'partial/report_top_common_header.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_left_menu.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_common.php');
include(ROOT_PATH.'library/helper.php');
include(ROOT_PATH."library/encryption.php");
$ams_helper = new ams_helper();
$converter = new Encryption;
$page_name = $ams_helper->curPageUrlInfo('page');

?>