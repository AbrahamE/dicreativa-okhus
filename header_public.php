<?php 
ob_start();
session_start();
include(__DIR__ . "/app_config.php");
$page_name = '';
$lang_code_global = "English";
$global_currency = "$";
$currency_position = "left";
$currency_sep = ".";
$localization = array();
$cookie_name = "ams_lang_code";
$cookie_name_branch = "ams_branch_code";

$super_admin_image = 'img/no_image.jpg';
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$localization = $row_query_ams_core;
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
	if($row_query_ams_core['super_admin_image'] != ''){
		$super_admin_image = WEB_URL . 'img/upload/' . $row_query_ams_core['super_admin_image'];
	}
}
//set lang from cookie
if(isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
	$lang_code_global = $_COOKIE[$cookie_name];
}

//set branch from cookie
if(isset($_COOKIE[$cookie_name_branch]) && !empty($_COOKIE[$cookie_name_branch])) {
	$_SESSION['objLogin']['branch_id'] = $_COOKIE[$cookie_name_branch];
}

include(ROOT_PATH.'language/'.$lang_code_global.'/lang_left_menu.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_common.php');
include(ROOT_PATH.'library/helper.php');
include(ROOT_PATH."library/maxPower.php");
$ams_helper = new ams_helper();
include(ROOT_PATH."library/encryption.php");
$converter = new Encryption;
$page_name = $ams_helper->curPageUrlInfo('page');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $_data['application_title']; ?></title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link rel="icon" type="image/png" href="<?php echo WEB_URL; ?>assets/img/favicon.png" />
<?php include(ROOT_PATH.'/partial/header_script.php'); ?>

<style type="text/css">

.sidebar-mobil{
	
	font-size: 20px;
	text-shadow: 2px 2px 2px #000;
    line-height: 50px;
    text-align: center;
}

</style>

</head>
<body class="skin-green sidebar-mini">
<div class="wrapper">
<div class="content-wrapper">