<?php

include("../config.php");
include("../library/encryption.php");
session_start();

$success = false;
$message = "";

$add = false;
$id_event = false;
$type = false;

if (isset($_SESSION['objLogin'])) {

    if (isset($_POST["authorization"])) {

        $authorization = explode('-', $_POST["authorization"]);

        $sql_search = "SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where meeting_id = '" . $authorization[0] . "' and tbl_meeting.activated = 1 and tbl_meeting.deleted = 0 LIMIT 1";

        $result = mysqli_query($link, $sql_search);

        $event_count = mysqli_num_rows($result);

        if ($event_count != false && $event_count > 0) {

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            $meeting_id = $row["meeting_id"];
            $event_type = $row["event_type"];
            $visitor_name = $row["visitor_name"];
            $visitor_email = $row["visitor_email"];
            $visitor_phone = $row["visitor_phone"];
            $id_invitacion = $row["meeting_id"];
            $visitor_frequent = $row["visitor_frequent"];
            $event_name = $row["event_name"];
            $event_visitors = $row["event_visitors"];
            $r_address = $row["r_address"];
            $fecha_inicio_individual = date("d-m-Y h:m:s a", strtotime($row["start_date"]));
            $fecha_fin_individual = date("d-m-Y h:m:s a", strtotime($row["end_date"]));
            $visitor_enter = $row["visitor_enter"];
            $visitor_exit = $row["visitor_exit"];

            if ($visitor_enter == 1 && $visitor_exit != NULL) {

                $success = false;
                $message = "Autorización ya fue Verificada y Cerrada.";
				$type = "error";
            } else {

                if ($visitor_enter == 1 && $visitor_exit == NULL) {

                    $sql_update = "UPDATE tbl_meeting SET visitor_enter = 1, visitor_exit = now(), status = 'CERRADA'  WHERE meeting_id = '" . $meeting_id . "'";

                    mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);

                    $add = mysqli_query($link, $sql_update);

                    if ($add) {

                        $id_event = mysqli_affected_rows($link);
                    }

                    mysqli_commit($link);

                    if ($id_event > 0) {

                        $success = true;
                        $message = "Autorización Cerrada.";
						$type = "success";
                    } else {
                        $success = true;
                        $message = "Autorización ya fue Cerrada.";
						$type = "warning";
                    }
                } else if ($visitor_enter == 0 && $visitor_exit == NULL) {

                    $sql_update = "UPDATE tbl_meeting SET visitor_enter = 1, status = 'VERIFICADA' WHERE meeting_id = '" . $meeting_id . "'";

                    mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);

                    $add = mysqli_query($link, $sql_update);

                    if ($add) {

                        $id_event = mysqli_affected_rows($link);
                    }

                    mysqli_commit($link);


                    if ($id_event > 0) {

                        $success = true;
                        $message = "Autorización verificada.";
						$type = "success";
                    } else {
                        $success = true;
                        $message = "Autorización ya fue verificada.";
						$type = "warning";
                    }
                }
            }
        } else {

            $success = false;
            $message = "Error en el Detalle de la Autorización.  No se han encontrado ninguna información de la autorización buscada.";
			$type = "error";
		}
    } else {
        $success = false;
		$message = "Debe escanear una Código QR Valido";
		$type = "error";
    }
} else {
    $success = false;
	$message = "Debe iniciar sesión en el Aplicativo";
	$type = "error";
}

echo json_encode([
    'success' => $success,
    'message' => $message,
	'type' => $type
]);
?>