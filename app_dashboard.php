<?php

include('app_header.php');
if($_SESSION['login_type'] != '1' && $_SESSION['login_type'] != '5'){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}

include(ROOT_PATH.'language/'.$lang_code_global.'/lang_admin_dashboard.php');

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 11px!important;
}

</style>


</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_logout.php" class="back-button header-logo"></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php" class="header-icon header-icon-1 back-button"><i class="fas fa-arrow-left"></i></a>
		<!--a href="#" class="header-icon header-icon-3" data-toggle-theme style="right: 0px;"><i class="fas fa-moon"></i></a-->
	</div>	
                  
    <div class="page-bg"><div></div></div>
	
	<div class="page-content header-clear-large">
			
			<div class="landing-page">			
				<div class="landing-icons" style="display:block;">
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app_dashboard_data.php'">
				   <i class="bg-gradient-red2 shadow-huge fa fa-chart-line"></i>
				   <em><?php echo $_data['menu_dashboard']; ?></em>
				   </a>
				   
				   <a href="#" data-menu="action-piso">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-clone"></i>
				   <em><?php echo $_data['menu_floor']; ?></em>
				   </a>
				   
				   <a href="#" data-menu="action-unidad">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-cubes"></i>
				   <em><?php echo $_data['menu_unit_information']; ?></em>
				   </a>
				   
				   <a href="#" data-menu="action-propietario">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user-circle"></i>
				   <em><?php echo $_data['menu_owner_information']; ?></em>
				   </a>
				   
				   <a href="#" data-menu="action-inquilino">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-users"></i>
				   <em><?php echo $_data['menu_renter_information']; ?></em>
				   </a>		

				   <a href="#" data-menu="action-empleado">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user"></i>
				   <em><?php echo $_data['menu_employee_information']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-alquileres">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['menu_rent_collection']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-utilidad">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-cog"></i>
				   <em><?php echo $_data['menu_owner_utility']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-mantenimiento">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-cut"></i>
				   <em><?php echo $_data['menu_maintenance_cost']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-comite">
				   <i class="bg-gradient-blue2 shadow-huge fas fa-user-tie"></i>
				   <em><?php echo $_data['menu_management_committee']; ?></em>
				   </a>		

				   <a href="#" data-menu="action-fondo">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['menu_fund']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-facturas">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['menu_bill']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-quejas">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-comments"></i>
				   <em><?php echo $_data['menu_complain']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-visitante">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-car"></i>
				   <em><?php echo $_data['menu_visitor']; ?></em>
				   </a>		

				   <a href="#" data-menu="action-reunion">
				   <i class="bg-gradient-blue2 shadow-huge far fa-handshake"></i>
				   <em><?php echo $_data['menu_meeting']; ?></em>
				   </a>						   
				   			   
				   <a href="#" data-menu="action-anuncios">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-bullhorn"></i>
				   <em><?php echo $_data['notice_board']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>mailsms/mailsms.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-envelope"></i>
				   <em><?php echo $_data['email_sms']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-reportes">
				   <i class="bg-gradient-blue2 shadow-huge fas fa-chart-bar"></i>
				   <em><?php echo $_data['menu_report']; ?></em>
				   </a>	
				   
					<?php if((int)$_SESSION['login_type'] == 5){ ?>				   

				   <a href="#" data-menu="action-datos">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-database"></i>
				   <em><?php echo $_data['database_left_menu']; ?></em>
				   </a>
				   
				   <a href="#" data-menu="action-configuracion">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-wrench"></i>
				   <em><?php echo $_data['menu_settings']; ?></em>
				   </a>	

					<?php } ?>
				   				   
				   <div class="clear"></div>				
				</div>				
			</div>			
	</div>
	
    <div id="action-piso"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_floor']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_floor']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>floor/addfloor.php'"><?php echo $_data['menu_floor_add']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>floor/floorlist.php'"><?php echo $_data['menu_floor_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-unidad"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_unit_information']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_unit_information']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>unit/addunit.php'"><?php echo $_data['menu_add_unit']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>unit/unitlist.php'"><?php echo $_data['menu_unit_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	
	
    <div id="action-propietario"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_owner_information']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_owner_information']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>owner/addowner.php'"><?php echo $_data['menu_add_owner']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>owner/ownerlist.php'"><?php echo $_data['menu_owner_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-inquilino"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_renter_information']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_renter_information']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>rent/addrent.php'"><?php echo $_data['menu_add_renter']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>rent/rentlist.php'"><?php echo $_data['menu_renter_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>		
	
    <div id="action-empleado"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="220"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_employee_information']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_employee_information']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>employee/addemployee.php'"><?php echo $_data['menu_add_employee']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>employee/employeelist.php'"><?php echo $_data['menu_employee_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
		
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>employee/employee_salary_setup.php'"><?php echo $_data['menu_employee_setup']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>employee/leave_request_list.php'"><?php echo $_data['menu_employee_leave']; ?></a>
            </div>
            <div class="clear"></div>
        </div>		
    </div>	

    <div id="action-alquileres"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_rent_collection']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_rent_collection']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fair/fairlist.php'"><?php echo $_data['menu_add_rent']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fair/addfair.php'"><?php echo $_data['menu_rent_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>		
	
    <div id="action-utilidad"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_owner_utility']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_owner_utility']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>owner_utility/add_owner_utility.php'"><?php echo $_data['menu_add_owner_utility']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>owner_utility/owner_utility_list.php'"><?php echo $_data['menu_owner_utility_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	
    <div id="action-mantenimiento"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_maintenance_cost']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_maintenance_cost']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>maintenance/add_maintenance_cost.php'"><?php echo $_data['menu_add_maintenance_cost']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>maintenance/maintenance_cost_list.php'"><?php echo $_data['menu_maintenance_cost_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	
	
    <div id="action-comite"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_management_committee']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_management_committee']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>management/add_m_committee.php'"><?php echo $_data['menu_add_member']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>management/m_committee_list.php'"><?php echo $_data['menu_member_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
		
    <div id="action-fondo"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_fund']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_fund']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fund/fund_list.php'"><?php echo $_data['menu_add_fund']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fund/fund_list.php'"><?php echo $_data['menu_fund_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-facturas"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_bill']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_bill']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fund/fund_list.php'"><?php echo $_data['menu_add_bill']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>fund/fund_list.php'"><?php echo $_data['menu_bill_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-quejas"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_complain']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_complain']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>complain/complainlist.php'"><?php echo $_data['menu_add_complain']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>complain/complainlist.php'"><?php echo $_data['menu_complain_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	
    <div id="action-visitante"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_visitor']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_visitor']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>visitor/addvisitor.php'"><?php echo $_data['menu_add_visitor']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>visitor/visitorlist.php'"><?php echo $_data['menu_visitor_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-reunion"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_meeting']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_meeting']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>meeting/addmeeting.php'"><?php echo $_data['menu_add_meeting']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>meeting/meetinglist.php'"><?php echo $_data['menu_meeting_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-anuncios"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="250"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['notice_board']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['notice_board']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>notice/notice.php'"><?php echo $_data['notice_board_1']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>notice/employee_notice.php'"><?php echo $_data['notice_board_2']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
		
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>notice/owner_notice.php'"><?php echo $_data['notice_board_3']; ?></a>
            </div>
            <div class="clear"></div>
        </div>		
    </div>	

    <div id="action-reportes"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="320"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_report']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_report']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/fair_report.php'"><?php echo $_data['menu_fair_report']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/rented_report.php'"><?php echo $_data['menu_rented_report']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
		
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/visitors_report.php'"><?php echo $_data['menu_visitors_report']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/complain_report.php'"><?php echo $_data['menu_complain_report']; ?></a>
            </div>
            <div class="clear"></div>
        </div>	

        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/unit_report.php'"><?php echo $_data['menu_unit_status_report']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/fund_status.php'"><?php echo $_data['menu_fund_status']; ?></a>
            </div>
            <div class="clear"></div>
        </div>	

        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/bill_report.php'"><?php echo $_data['menu_bill_report']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>report/salary_report.php'"><?php echo $_data['salary_report_text']; ?></a>
            </div>
            <div class="clear"></div>
        </div>			
    </div>	

    <div id="action-datos"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['database_left_menu']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['database_left_menu']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>database/cleardata.php'"><?php echo $_data['database_clear_dummy_data']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	

    <div id="action-configuracion"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="450"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_settings']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_settings']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/admin.php'"><?php echo $_data['menu_admin_setup']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>branch/branchlist.php'"><?php echo $_data['branch_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
		
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>branch/addbranch.php'"><?php echo $_data['add_branch']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/bill_setup.php'"><?php echo $_data['menu_bill_setup']; ?></a>
            </div>
            <div class="clear"></div>
        </div>	

        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/utility_bill_setup.php'"><?php echo $_data['menu_utility_bill']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/member_type_setup.php'"><?php echo $_data['menu_management_member_type']; ?></a>
            </div>
            <div class="clear"></div>
        </div>	

        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/month_setup.php'"><?php echo $_data['menu_month_setup']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/year_setup.php'"><?php echo $_data['menu_year_setup']; ?></a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/currency_setup.php'"><?php echo $_data['menu_currency_setup']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>setting/language.php'"><?php echo $_data['menu_language_setup']; ?></a>
            </div>
            <div class="clear"></div>
        </div>		
    </div>		
		
	
    <div class="menu-hider"></div>
</div>
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
</body>

