<?php include('app_header_owner.php'); 

include(ROOT_PATH.'language/'.$lang_code_global.'/lang_left_menu.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_common.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."logout.php");
	die();
}
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 11px!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-logo"></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php" class="header-icon header-icon-1 back-button"><i class="fas fa-arrow-left"></i></a>
		<!--a href="#" class="header-icon header-icon-3" data-toggle-theme style="right: 0px;"><i class="fas fa-moon"></i></a-->
	</div>	
                  
    <div class="page-bg"><div></div></div>
	
	<div class="page-content header-clear-large">
			
			<div class="landing-page">			
				<div class="landing-icons" style="display:block;">
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app_o_dashboard_data.php'">
				   <i class="bg-gradient-red2 shadow-huge fa fa-chart-line"></i>
				   <em><?php echo $_data['text_1']; ?></em>
				   </a>
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/unitdetails.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-users"></i>
				   <em><?php echo $_data['text_2']; ?></em>
				   </a>				   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/tenantdetails.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user-circle"></i>
				   <em><?php echo $_data['text_3']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/employeedetails.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user"></i>
				   <em><?php echo $_data['text_4']; ?></em>
				   </a>						   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/fairdetails.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['text_5']; ?></em>
				   </a>				   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/owner_utility_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-cog"></i>
				   <em><?php echo $_data['text_6']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/maintenance_cost.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-wrench"></i>
				   <em><?php echo $_data['text_7']; ?></em>
				   </a>	


				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/fund_status.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['text_8']; ?></em>
				   </a>					   

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/complainlist.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-gavel"></i>
				   <em><?php echo $_data['text_88']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>o_dashboard/rented_report.php'">
				   <i class="bg-gradient-blue2 shadow-huge fas fa-chart-bar"></i>
				   <em><?php echo $_data['text_9']; ?></em>
				   </a>			   		   
				   				   
				   <div class="clear"></div>				
				</div>				
			</div>			
	</div>		
	
    <div class="menu-hider"></div>
</div>
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
</body>