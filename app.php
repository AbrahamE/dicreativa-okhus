<?php
ob_start();
session_start();
define('DIR_APPLICATION', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');
if(!file_exists("app_config.php")){
	header("Location: install/index.php");
	die();
}

unset($_SESSION['step']);
unset($_SESSION['domain']);
unset($_SESSION['is_mobile']);
include(DIR_APPLICATION."app_config.php");
include(DIR_APPLICATION."library/maxPower.php");
include(DIR_APPLICATION."library/encryption.php");
include(DIR_APPLICATION."library/helper.php");
$converter = new Encryption;
$helper = new ams_helper;
$helper->removeInstallFolder(ROOT_PATH.'install');

$_SESSION['is_mobile'] = true;

//redirect if session already exist
if(isset($_SESSION['objLogin']) && !empty($_SESSION['objLogin'])){
	
	if($_SESSION['login_type'] == '1' || $_SESSION['login_type'] == '5'){
		header("Location: app_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '2'){
		header("Location: app_o_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '3'){
		header("Location: app_e_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '4'){
		header("Location: app_t_dashboard.php");
		die();
	}
}
/*****************************************************************************/
/////////////variables/////////////////////////////////////////////////////////
$msg = false;
$sql = '';
$cookie_name = "ams_lang_code";
//////////////////////////////////////////////////////////////////////////////

if(isset($_POST['username']) && $_POST['username'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
	$user_name = trim(make_safe($link, $_POST['username'])); //Escaping Strings
	$password = $converter->encode(trim(make_safe($link, $_POST['password']))); //Escaping Strings
	//admin
	if($_POST['ddlLoginType'] == '1'){
		$sql = mysqli_query($link,"SELECT *,b.* FROM tbl_add_admin aa left join tblbranch b on b.branch_id = aa.branch_id WHERE aa.email = '".$user_name."' and aa.password = '".$password."'");
	}
	//owner
	if($_POST['ddlLoginType'] == '2'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_owner o left join tblbranch b on b.branch_id = o.branch_id WHERE o.o_email = '".$user_name."' and o.o_password = '".$password."'");
	}
	//employee
	if($_POST['ddlLoginType'] == '3'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_employee e left join tblbranch b on b.branch_id = e.branch_id WHERE e.e_email = '".$user_name."' and e.e_password = '".$password."'");
	}
	//renter
	if($_POST['ddlLoginType'] == '4'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_rent ad left join tblbranch b on b.branch_id = ad.branch_id WHERE ad.r_email = '".$user_name."' and ad.r_password = '".$password."'");
	}
	//super admin
	if($_POST['ddlLoginType'] == '5'){
		$sql = mysqli_query($link, "SELECT * FROM tblsuper_admin WHERE email = '".$user_name."' and password = '".$password."'");
	}
	if(!empty($sql)){
		if($row = mysqli_fetch_assoc($sql)){
			if($_POST['ddlLoginType'] == '5'){
				$branch_list = getBuildingDetails($_POST['ddlBranch'], $link);
				$arr = array(
					'user_id'				=> $row['user_id'],
					'name'					=> $row['name'],
					'email'					=> $row['email'],
					'contact'				=> $row['contact'],
					'password'				=> $_POST['password'],
					'added_date'			=> $row['added_date']
				);
				$arr = array_merge($arr, $branch_list);
				$_SESSION['objLogin'] = $arr;
			}
			else{
				$_SESSION['objLogin'] = $row;
			}
			
			mysqli_close($link);
			$link = NULL;
			
			$_SESSION['login_type'] = $_POST['ddlLoginType'];
			$_SESSION['is_mobile'] = true;
			if($_POST['ddlLoginType'] == '1' || $_POST['ddlLoginType'] == '5'){
				header("Location: app_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '2'){
				header("Location: app_o_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '3'){
				header("Location: app_e_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '4'){
				header("Location: app_t_dashboard.php");
				die();
			}	
		} else {
			$msg = true;
		}
	}
	else{
		$msg = true;
	}
}
//
function getBuildingDetails($bid, $link){
	$sql = mysqli_query($link, "SELECT * from tblbranch WHERE branch_id = ".(int)$bid);
	if($row = mysqli_fetch_assoc($sql)){
		return $row;
	}
	return array();
}
function make_safe($con, $variable){
	$variable = mysqli_real_escape_string($con, strip_tags(trim($variable)));
	return $variable; 
}
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
}
$lang_code = "Spanish";
if(isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
	$lang_code = $_COOKIE[$cookie_name];
	$lang_code_global = $_COOKIE[$cookie_name];
}
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_index.php');
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $_data['application_title']; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(80%);
    transform: translateX(0px);
    overflow: hidden;
	margin: auto!important;
}

.header {
    /*background-color: #FFF!important;*/
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app.php" class="back-button header-icon header-icon-1 color-white back-button font-20 left-0"><i class="fa fa-home"></i></a>
	</div>
                  
    <div class="page-bg"><div></div></div>
    <div class="page-content">     
                
        <div data-height="cover-header" class="caption bottom-0">
					<div class="caption-center left-20 right-20" style="top: 50%;">

						<div class="landing-page">
							<div class="landing-header">
								<img src="app/images/logo-dark.png" class="responsive-image-full bottom-20">
							</div>
							<div class="landing-header">
								<a href="app-index.php?tipo=4">
									<img src="app/images/iconos/inicio/residente.png">
									<em>Residentes</em>
								</a>  				
								<div class="clear"></div>
							</div>				
							<div class="landing-header"> 
								<a href="javascript:void(Tawk_API.toggle())">
									<img src="app/images/iconos/inicio/soporte.png" style="" />
									<em>Soporte</em>
								</a>
								
								<div class="clear"></div>
							</div>				

						</div>
				</div>
            </div>
        </div>        
    </div>      
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
<script type="text/javascript">

$(document).ready(function() {
    support_chat();
});

	function support_chat() {
		
		var Tawk_API = Tawk_API || {},
			Tawk_LoadStart = new Date();
		(function () {
			var s1 = document.createElement("script"),
				s0 = document.getElementsByTagName("script")[0];
			s1.async = true;
			s1.src = "https://embed.tawk.to/5f7a708cf0e7167d00161d6b/default";
			s1.charset = "UTF-8";
			s1.setAttribute("crossorigin", "*");
			s0.parentNode.insertBefore(s1, s0);
		})();		
		
	}

</script>
</body>