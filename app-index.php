<?php
ob_start();
session_start();
define('DIR_APPLICATION', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');
if(!file_exists("app_config.php")){
	header("Location: install/index.php");
	die();
}

unset($_SESSION['step']);
unset($_SESSION['domain']);
unset($_SESSION['is_mobile']);
include(DIR_APPLICATION."app_config.php");
include(DIR_APPLICATION."library/maxPower.php");
include(DIR_APPLICATION."library/encryption.php");
include(DIR_APPLICATION."library/helper.php");
$converter = new Encryption;
$helper = new ams_helper;
$helper->removeInstallFolder(ROOT_PATH.'install');

$_SESSION['is_mobile'] = true;

//redirect if session already exist
if(isset($_SESSION['objLogin']) && !empty($_SESSION['objLogin'])){
	
	if($_SESSION['login_type'] == '1' || $_SESSION['login_type'] == '5'){
		header("Location: app_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '2'){
		header("Location: app_o_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '3'){
		header("Location: app_e_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '4'){
		header("Location: app_t_dashboard.php");
		die();
	}
}
/*****************************************************************************/
/////////////variables/////////////////////////////////////////////////////////
$msg = false;
$sql = '';
$cookie_name = "ams_lang_code";
//////////////////////////////////////////////////////////////////////////////

if(isset($_POST['username']) && $_POST['username'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
	$user_name = trim(make_safe($link, $_POST['username'])); //Escaping Strings
	$password = $converter->encode(trim(make_safe($link, $_POST['password']))); //Escaping Strings
	//admin
	if($_POST['ddlLoginType'] == '1'){
		$sql = mysqli_query($link,"SELECT *,b.* FROM tbl_add_admin aa left join tblbranch b on b.branch_id = aa.branch_id WHERE aa.email = '".$user_name."' and aa.password = '".$password."'");
	}
	//owner
	if($_POST['ddlLoginType'] == '2'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_owner o left join tblbranch b on b.branch_id = o.branch_id WHERE o.o_email = '".$user_name."' and o.o_password = '".$password."'");
	}
	//employee
	if($_POST['ddlLoginType'] == '3'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_employee e left join tblbranch b on b.branch_id = e.branch_id WHERE e.e_email = '".$user_name."' and e.e_password = '".$password."'");
	}
	//renter
	if($_POST['ddlLoginType'] == '4'){
		$sql = mysqli_query($link, "SELECT *,b.* FROM tbl_add_rent ad left join tblbranch b on b.branch_id = ad.branch_id WHERE ad.r_email = '".$user_name."' and ad.r_password = '".$password."'");
	}
	//super admin
	if($_POST['ddlLoginType'] == '5'){
		$sql = mysqli_query($link, "SELECT * FROM tblsuper_admin WHERE email = '".$user_name."' and password = '".$password."'");
	}
	if(!empty($sql)){
		if($row = mysqli_fetch_assoc($sql)){
			if($_POST['ddlLoginType'] == '5'){
				$branch_list = getBuildingDetails($_POST['ddlBranch'], $link);
				$arr = array(
					'user_id'				=> $row['user_id'],
					'name'					=> $row['name'],
					'email'					=> $row['email'],
					'contact'				=> $row['contact'],
					'password'				=> $_POST['password'],
					'added_date'			=> $row['added_date']
				);
				$arr = array_merge($arr, $branch_list);
				$_SESSION['objLogin'] = $arr;
			}
			else{
				$_SESSION['objLogin'] = $row;
			}
			
			mysqli_close($link);
			$link = NULL;
			
			$_SESSION['login_type'] = $_POST['ddlLoginType'];
			$_SESSION['is_mobile'] = true;
			if($_POST['ddlLoginType'] == '1' || $_POST['ddlLoginType'] == '5'){
				header("Location: app_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '2'){
				header("Location: app_o_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '3'){
				header("Location: app_e_dashboard.php");
				die();
			}
			else if($_POST['ddlLoginType'] == '4'){
				header("Location: app_t_dashboard.php");
				die();
			}	
		} else {
			$msg = true;
		}
	}
	else{
		$msg = true;
	}
}
//
function getBuildingDetails($bid, $link){
	$sql = mysqli_query($link, "SELECT * from tblbranch WHERE branch_id = ".(int)$bid);
	if($row = mysqli_fetch_assoc($sql)){
		return $row;
	}
	return array();
}
function make_safe($con, $variable){
	$variable = mysqli_real_escape_string($con, strip_tags(trim($variable)));
	return $variable; 
}
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
}
$lang_code = "Spanish";
if(isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
	$lang_code = $_COOKIE[$cookie_name];
	$lang_code_global = $_COOKIE[$cookie_name];
}
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_index.php');
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $_data['application_title']; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(80%);
    transform: translateX(0px);
    overflow: hidden;
	margin: auto!important;
}

.header {
    /*background-color: #FFF!important;*/
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app.php" class="back-button header-icon header-icon-1 color-white back-button font-20 left-0"><i class="fa fa-home"></i></a>
	</div>
                  
    <div class="page-bg"><div></div></div>
    <div class="page-content">     
        <div data-height="cover-header" class="caption">
            <div class="caption-center left-20 right-20" style="top: 58%;">
                <div class="content content-box content-box-full-top content-box-full-bottom">
                    <img src="app/images/logo-dark.png" class="responsive-image-full bottom-20">
					<p class="under-heading font-15 color-theme opacity-70 bottom-10"><?php echo $_data['enter_login_details']; ?></p>
					
				  <?php if($msg) { ?>
				  
						<div class="alert alert-small round-small shadow-huge bg-red2-dark bottom-10">
							<i class="fa fa-times-circle"></i>
							<span><?php echo $_data['wrong_login_msg']; ?></span>
							<i class="fa fa-times"></i>
						</div>	  
				  
				  <?php } ?>					
					
					<form onSubmit="return validationForm();" role="form" id="form" method="post">
					
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-user"></i>
							<span><?php echo $_data['your_email']; ?></span>
							<em>(requerido)</em>
							<input type="email" name="username" id="username" class="form-control" required placeholder="<?php echo $_data['your_email']; ?>" />
						</div> 
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-lock"></i>
							<span><?php echo $_data['your_password']; ?></span>
							<em>(requerido)</em>
							<input type="password" name="password" id="password" class="form-control" required placeholder="<?php echo $_data['your_password']; ?>" />
						</div> 
						
						<div class="input-style input-style-1 input-required" style="display:none;">
							<em><i class="fa fa-check color-green1-dark"></i></em>
							<select name="ddlLoginType" onChange="mewhat(this.value);" id="ddlLoginType" class="form-control" required>
								<option selected value="<?php echo $_GET['tipo']; ?>"><?php echo $_data['user_'.$_GET['tipo']]; ?></option>
							</select>
						</div>
						
						 <div id="x_branch" class="input-style input-style-1" style="display:none;"> 
					
						  <select name="ddlBranch" id="ddlBranch" class="form-control">
						  <option value="">--<?php echo $_data['select_branch']; ?>--</option>
						  <?php 
								$result_branch = mysqli_query($link,"SELECT * FROM tblbranch where b_status = 1 order by branch_name ASC");
								while($row_branch = mysqli_fetch_array($result_branch)){?>
						  <option value="<?php echo $row_branch['branch_id'];?>"><?php echo $row_branch['branch_name'];?></option>
						  <?php } ?>
							</select>
						</div>
						
						<div class="input-style input-style-1" style="display:none;">
						  <select name="ddlLanguage" id="ddlLanguage" class="form-control">
							<option value="">--<?php echo $_data['select_language']; ?>--</option>
							<?php
							
							$dir    = ROOT_PATH . 'language/';
							$files1 = scandir($dir);
							foreach(array_reverse($files1) as $folder){
								if($folder != '' && $folder != '.' && $folder != '..'){
									if(trim($folder) == $lang_code){
										echo '<option selected value="'.$folder.'">'.$folder.'</option>';
									}
									else{
										echo '<option value="'.$folder.'">'.$folder.'</option>';
									}
								}
							}
							?>
						  </select>
						</div>

						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-13" onclick="window.location.href = '<?php echo WEB_URL; ?>app-forgetpassword.php?tipo=<?php echo $_GET['tipo']; ?>'"><?php echo $_data['forgot_password'];?></a>
						</div>					
						
						
						<div class="clear"></div>
						<center>
						<button type="submit" id="login" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25"><?php echo $_data['_login'];?></button>
						</center>
						
						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" onclick="window.location.href = '<?php echo WEB_URL; ?>rent/app_addrent.php'">Registrarme</a>
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-pilitica-bottom">Política de Privacidad</a>
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-termino-bottom">Termino de Condiciones</a>

						</div>							
						
					</form>
                </div>
            </div>
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="caption-bg" style=""></div>
        </div>         
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  	

<div class="menu-hider"></div>

    
</div>

<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
<script type="text/javascript">
function validationForm(){
	if($("#username").val() == ''){
		alert("<?php echo $_data['v1'];?>");
		$("#username").focus();
		return false;
	}
	else if($("#password").val() == ''){
		alert("<?php echo $_data['v3'];?>");
		$("#password").focus();
		return false;
	}
	else if($("#ddlLoginType").val() == ''){
		alert("<?php echo $_data['v4'];?>");
		return false;
	}
	else if(!validateEmail($("#username").val())){
		alert("<?php echo $_data['v2'];?>");
		$("#username").focus();
		return false;
	}
	else{
		return true;
	}
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function mewhat(val){
	if(val != ''){
		if(val == '5'){
			$("#x_branch").show();
		}
		else{
			$("#x_branch").hide();
		}
	}
	else{
		$("#x_branch").hide();
	}
}

$('#ddlLoginType').on('change', function() {
	console.log(this.value === '5');
   if(this.value === '5') {
		$('#ddlBranch').attr('required',true);
   } else {   
	   $('#ddlBranch').attr('required',false);
   }	
});
</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>