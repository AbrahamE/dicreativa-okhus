<?php 
ob_start();
session_start();
include(__DIR__ . "/app_config.php");
$page_name = '';
$lang_code_global = "English";
$global_currency = "$";
$currency_position = "left";
$currency_sep = ".";
$localization = array();
$cookie_name = "ams_lang_code";
$cookie_name_branch = "ams_branch_code";

if(!isset($_SESSION['objLogin'])){
		
	header("Location: ".WEB_URL."app_logout.php");
	die();
}
//
if(isset($_SESSION['login_type']) && ((int)$_SESSION['login_type'] != 5 && (int)$_SESSION['login_type'] != 1)){
		
	header("Location: ".WEB_URL."app_logout.php");
	die();
}
$super_admin_image = 'img/no_image.jpg';
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$localization = $row_query_ams_core;
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
	if($row_query_ams_core['super_admin_image'] != ''){
		$super_admin_image = WEB_URL . 'img/upload/' . $row_query_ams_core['super_admin_image'];
	}
}
//set lang from cookie
if(isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
	$lang_code_global = $_COOKIE[$cookie_name];
}

//set branch from cookie
if(isset($_COOKIE[$cookie_name_branch]) && !empty($_COOKIE[$cookie_name_branch])) {
	$_SESSION['objLogin']['branch_id'] = $_COOKIE[$cookie_name_branch];
}

include(ROOT_PATH.'language/'.$lang_code_global.'/lang_left_menu.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_common.php');
include(ROOT_PATH.'library/helper.php');
include(ROOT_PATH."library/maxPower.php");
//instance 
$ams_helper = new ams_helper();

////////////////////////////////////////////APARTMENT DETAILS/////////////////////////////////////////////////////////////////////////////////////
$building_rules = '';
$building_name = '';
$moderator_mobile = '';
$secrataty_mobile = '';
$security_guard_mobile = '';
$result_apartment = mysqli_query($link,"SELECT * FROM tblbranch where branch_id=".(int)$_SESSION['objLogin']['branch_id']);
if($row_apartment = mysqli_fetch_array($result_apartment)){
	$building_rules = $row_apartment['building_rule'];
	$building_name = $row_apartment['branch_name'];
	$moderator_mobile = $row_apartment['moderator_mobile'];
	$secrataty_mobile = $row_apartment['secrataty_mobile'];
	$security_guard_mobile = $row_apartment['security_guard_mobile'];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////admin image/////////////////////////
$image = WEB_URL . 'img/no_image.jpg';	
if(isset($_SESSION['objLogin']['image'])){
	if(file_exists(ROOT_PATH . '/img/upload/' . $_SESSION['objLogin']['image']) && $_SESSION['objLogin']['image'] != ''){
		$image = WEB_URL . 'img/upload/' . $_SESSION['objLogin']['image'];
	}
}
if(isset($_SESSION['login_type']) && ((int)$_SESSION['login_type'] == 5)){
	$image = $super_admin_image;
}
/////////////////////////////////////////////////////////////

include(ROOT_PATH."library/encryption.php");
$converter = new Encryption;
$page_name = $ams_helper->curPageUrlInfo('page');
?>