<?php
ob_start();
session_start();
define('DIR_APPLICATION', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');
if(!file_exists("../app_config.php")){
	header("Location: install/index.php");
	die();
}

unset($_SESSION['step']);
unset($_SESSION['domain']);
unset($_SESSION['is_mobile']);
include(DIR_APPLICATION."../app_config.php");
include(DIR_APPLICATION."../library/maxPower.php");
include(DIR_APPLICATION."../library/encryption.php");
include(DIR_APPLICATION."../library/helper.php");
$converter = new Encryption;
$helper = new ams_helper;
$helper->removeInstallFolder(ROOT_PATH.'install');

$_SESSION['is_mobile'] = true;

if(isset($_SESSION['objLogin']) && !empty($_SESSION['objLogin'])){
	
	if($_SESSION['login_type'] == '1' || $_SESSION['login_type'] == '5'){
		header("Location: app_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '2'){
		header("Location: app_o_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '3'){
		header("Location: app_e_dashboard.php");
		die();
	}
	else if($_SESSION['login_type'] == '4'){
		header("Location: app_t_dashboard.php");
		die();
	}
}

$add = false;
$email = false;
$sql = '';
$cookie_name = "ams_lang_code";

function make_safe($con, $variable){
	$variable = mysqli_real_escape_string($con, strip_tags(trim($variable)));
	return $variable; 
}
$query_ams_settings = mysqli_query($link,"SELECT * FROM tbl_settings");
if($row_query_ams_core = mysqli_fetch_array($query_ams_settings)){
	$lang_code_global = $row_query_ams_core['lang_code'];
	$global_currency = $row_query_ams_core['currency'];
	$currency_position = $row_query_ams_core['currency_position'];
	$currency_sep = $row_query_ams_core['currency_seperator'];
}
$lang_code = "Spanish";
if(isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
	$lang_code = $_COOKIE[$cookie_name];
	$lang_code_global = $_COOKIE[$cookie_name];
}
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_index.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_add_rented.php');

if(isset($_POST['nombres'])){

			if(isset($_POST['correo']) && !empty($_POST['correo'])){
				$result_renter = mysqli_query($link,"SELECT * FROM tbl_add_rent where r_email = '".trim($_POST['correo'])."'");
				if($row_renter = mysqli_fetch_array($result_renter)){
					$email = true;
				}
			}
			
		if(!$email)	{

			$r_password = $converter->encode($_POST['password']);
			$sql = "INSERT INTO tbl_add_rent(
					r_name,
					r_email,
					r_contact,
					r_address,
					r_password,
					r_status,
					branch_id) 
					values(
					'$_POST[nombres]',
					'$_POST[correo]',
					'$_POST[telefono]',
					'$_POST[direccion]',
					'$r_password',
					0,
					'$_POST[branch_id]')";
			
			$add = mysqli_query($link, $sql);
								
			mysqli_close($link);
		
		}
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $_data['application_title']; ?></title>
<link rel="stylesheet" type="text/css" href="../app/styles/style.css">
<link rel="stylesheet" type="text/css" href="../app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="../app/fonts/css/fontawesome-all.min.css">  
<link rel="apple-touch-icon" sizes="180x180" href="../app/app/icons/icon-192x192.png">
<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(100%);
    transform: translateX(0px);
    overflow: hidden;
}

.header {
    /*background-color: #FFF!important;*/
}
</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app.php" class="back-button header-icon header-icon-1 color-white back-button font-20 left-0"><i class="fa fa-home"></i></a>
	</div>
                  
    <div class="page-bg"><div></div></div>
    <div class="page-content header-clear-large">    

        <div data-height="cover-header" class="caption">  		
		
            <div class="caption left-20 right-20">
                <div class="content content-box content-box-full-top content-box-full-bottom">
                    <img src="../app/images/logo-dark.png" class="responsive-image-full bottom-20">
					<p class="under-heading font-15 color-theme opacity-70 bottom-10">Ingresar detalles para el Registro de Residente</p>				
					
					<form method="post" enctype="multipart/form-data" id="frm_renter_entry">
					
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-user"></i>
							<span>Nombres</span>
							<em>(requerido)</em>
							<input type="text" name="nombres" id="nombres" class="form-control" required placeholder="Nombres" />
						</div>

						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-user"></i>
							<span>Apellidos</span>
							<em>(requerido)</em>
							<input type="text" name="apellidos" id="apellidos" class="form-control" required placeholder="Apellidos" />
						</div>						
					
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-lock"></i>
							<span>Contraseña</span>
							<em>(requerido)</em>
							<input type="password" name="password" id="password" class="form-control" required placeholder="Contraseña" />
						</div>
						
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-phone"></i>
							<span>Teléfonos</span>
							<em>(requerido)</em>
							<input type="text" name="telefono" id="telefono" class="form-control" required placeholder="Teléfonos" />
						</div>
					
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-envelope"></i>
							<span>Correo Electrónico</span>
							<em>(requerido)</em>
							<input type="email" name="correo" id="correo" class="form-control" required placeholder="Correo Electrónico" />
						</div> 
						
						<div class="input-style has-icon input-style-1 input-required">
							<i class="input-icon fa fa-address-card"></i>
							<span>Dirección</span>
							<em>(requerido)</em>
							<input type="text" name="direccion" id="direccion" class="form-control" required placeholder="Dirección" />
						</div>
						
						<div class="input-style input-style-2 input-required">
							<span>Conjunto Residencial</span>
							<em><i class="fa fa-angle-down"></i></em>
							<select name="branch_id" id="branch_id" required>
								<option value="" disabled selected>Seleccione un Edificio</option>
						  <?php 
								$result_branch = mysqli_query($link,"SELECT * FROM tblbranch where b_status = 1 order by branch_name ASC");
								while($row_branch = mysqli_fetch_array($result_branch)){?>
						  <option value="<?php echo $row_branch['branch_id'];?>"><?php echo $row_branch['branch_name'];?></option>
						  <?php } ?>
							</select>
						</div>											

						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-13" onclick="window.location.href = '<?php echo WEB_URL; ?>app-index.php?tipo=4'">Inicio de Sesión</a>
						</div>					
						
						
						<div class="clear"></div>
						<center>
						<button type="submit" id="registro" class="button button-m round-huge shadow-huge button-full bg-gradient-blue2 shadow-huge top-30 bottom-25">Registrarme</button>
						</center>
						
						<div class="one-half top-20 last-column" style="float:right;width: 100%!important;">
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-pilitica-bottom">Política de Privacidad</a>
							<a href="#" class="color-gray-light text-right font-10" style="color: #727171; font-size: 15px!important;" data-menu="action-termino-bottom">Termino de Condiciones</a>
							<a href="#" id="add" data-menu="action-add"></a>
							<a href="#" id="error" data-menu="action-error"></a>
						</div>							
						
					</form>
					
                </div>
            </div>
            <div class="caption-overlay bg-gradient-blue opacity-80"></div>
            <div class="caption-bg" style=""></div>
        </div>         
    </div>  

    <div id="action-pilitica-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Política de Privacidad</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Política de Privacidad</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div>  
	
    <div id="action-termino-bottom"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="200"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0">Termino de Condiciones</h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11">Termino de Condiciones</p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        <div class="content">
            <p class="boxed-text-huge">
                Agregar texto...
            </p>
        </div>
        <a href="#" class="close-menu button button-m button-full bg-blue2-dark button-margins button-round-large shadow-huge">Cerrar</a>        
    </div> 

    <div id="action-add"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-green1-dark"><i class="fa fa-check-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Exito</h1>        
            <p class="font-14 boxed-text-large">
                Registro completado
            </p>
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app-index.php?tipo=4'"class="button button-margins button-center-large button-m round-small shadow-huge bg-green1-dark">Continuar</a>
    </div>      

    <div id="action-error"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="360"
         data-menu-effect="menu-over">
        
        <div class="center-text">
            <h1 class="top-30 bottom-20 color-red2-dark"><i class="fa fa-times-circle shadow-huge round-circle fa-4x"></i></h1>
            <h1 class="font-40 bottom-10">Error</h1>        
            <p class="font-14 boxed-text-large">
                Registro no se envío correctamente
            </p>
			<?php if(!$email == true) { ?>
            <p class="font-14 boxed-text-large">
                El correo electrónico del ingresado ya existe
            </p>
			<?php } ?>	
        </div>
        <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>rent/app_addrent.php'" class="button button-margins button-center-large button-m round-small shadow-huge bg-red2-dark">Refrescar</a>
    </div>  		

<div class="menu-hider"></div>
 
</div>

<script type="text/javascript" src="../app/scripts/jquery.js"></script>
<script type="text/javascript" src="../app/scripts/plugins.js"></script>
<script type="text/javascript" src="../app/scripts/custom.js" async></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
		
	<?php if(isset($_POST['nombres']) && $add == true) { ?>

		activateMenu('action-add');

	<?php } else if(isset($_POST['nombres']) && $add == false) { ?>
	
		activateMenu('action-error');

	<?php } ?>	

	});

    function activateMenu(menuID) {
		
		var body = $("body");
		var header = $(".header-fixed");
		var pageContent = $(".page-content");
		var footerMenu = $(".footer-menu");
		var menu = $(".menu-box");
		var menuHider = $(".menu-hider");
		var menuClose = $(".menu-close, .menu-hider, .close-menu");
		var menuDeployer = $("[data-menu]");
		var menuPushElements = $(".header-fixed, .footer-menu, .page-content, .page-bg");
		var menuSelected = $("[data-menu-selected]").data("menu-selected");	
		
        var menuID = $("#"+menuID);

        menuPushElements.css({
            transform: "translate(0,0)",
        });
        header.removeClass("menu-hide-header");
        footerMenu.removeClass("menu-hide-footer");
        menu.removeClass("menu-active");
        menuHider.removeClass("menu-hider-visible menu-hider-3d");
        menuHider.css({
            transform: "translate(0,0)",
        });
        var menuType = menuID.data("menu-type");
        var menuWidth = menuID.data("menu-width");
        var menuHeight = menuID.data("menu-height");
        var menuEffect = menuID.data("menu-effect");
        var menuWidthParallax = menuID.data("menu-width") / 3;
        var menuHeightParallax = menuID.data("menu-height") / 3;
        if (menuEffect === "menu-push") {
            if (menuType === "menu-box-top") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeight + "px)",
                });
            }
            if (menuType === "menu-box-bottom") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeight * -1 + "px)",
                });
            }
            if (menuType === "menu-box-left") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-parallax") {
            if (menuType === "menu-box-top") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeightParallax + "px)",
                });
            }
            if (menuType === "menu-box-bottom") {
                menuPushElements.css({
                    transform: "translateY(" + menuHeightParallax * -1 + "px)",
                });
            }
            if (menuType === "menu-box-left") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidthParallax + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuPushElements.css({
                    transform: "translateX(" + menuWidthParallax * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-reveal") {
            if (menuType === "menu-box-left") {
                menuID.show(0);
                menuHider.addClass("menu-hider-reveal");
                menuPushElements.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
                menuHider.css({
                    transform: "translateX(" + menuWidth + "px)",
                });
            }
            if (menuType === "menu-box-right") {
                menuID.show(0);
                menuHider.addClass("menu-hider-reveal");
                menuPushElements.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
                menuHider.css({
                    transform: "translateX(" + menuWidth * -1 + "px)",
                });
            }
            menuID.addClass("menu-active");
        }
        if (menuEffect === "menu-over") {
            menuID.addClass("menu-active");
        }
        if (menuType === "menu-box-modal") {
            menuID.addClass("menu-active");
        }
        menuHider.addClass("menu-hider-visible");
    }
	
    function disableMenu() {
        setTimeout(function () {
            $('[data-menu-effect="menu-reveal"]').hide(0);
        }, 150);
        menuPushElements.css({
            transform: "translate(0,0)",
        });
        header.removeClass("menu-hide-header");
        footerMenu.removeClass("menu-hide-footer");
        menu.removeClass("menu-active");
        menuHider.removeClass("menu-hider-visible menu-hider-reveal menu-active");
        menuHider.css({
            transform: "translate(0,0)",
        });
        menu.delay(250).animate(
            {
                scrollTop: 0,
            },
            350
        );
    }	

</script>
<input type="hidden" id="web_url" value="<?php echo WEB_URL; ?>" />
</body>
</html>