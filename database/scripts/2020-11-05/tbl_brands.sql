-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-11-2020 a las 03:30:37
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ams_20201105`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_brands`
--

DROP TABLE IF EXISTS `tbl_brands`;
CREATE TABLE IF NOT EXISTS `tbl_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `category` enum('COMIDA','PAQUETERIA','TAXIS') DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_brands`
--

INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(1, 'Amazon', 'PAQUETERIA', 'amazon.jpg', '2020-11-05 17:23:46', '2020-11-05 17:28:15');
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(2, 'Cadefi', 'TAXIS', 'Cadefi.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(3, 'Cornershop', 'COMIDA', 'cornershop.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(4, 'Dhl', 'PAQUETERIA', 'dhl.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(5, 'Didi', 'TAXIS', 'didi.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(6, 'Rapi', 'COMIDA', 'rapi.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(7, 'Sindelantal', 'COMIDA', 'sindelantal.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(8, 'Uber', 'TAXIS', 'uber.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(9, 'Ubereast', 'COMIDA', 'ubereast.jpg', '2020-11-05 17:29:43', NULL);
INSERT INTO `tbl_brands` (`id`, `name`, `category`, `image`, `create_date`, `update_date`) VALUES(10, 'Ups', 'PAQUETERIA', 'ups.jpg', '2020-11-05 17:29:43', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
