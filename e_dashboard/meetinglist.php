<?php 
include('../header_emp.php');
include('../utility/common.php');
if(!isset($_SESSION['objLogin'])){
	header("Location: " . WEB_URL . "logout.php");
	die();
}

$disable = true;
?>
<?php
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_meeting_list.php');
$delinfo = 'none';
$addinfo = 'none';
$msg = "";
if(isset($_GET['id']) && $_GET['id'] != '' && $_GET['id'] > 0){
	$sqlx= "DELETE FROM `tbl_meeting` WHERE meeting_id = ".$_GET['id'];
	mysqli_query($link,$sqlx); 
	$delinfo = 'block';
}
if(isset($_GET['m']) && $_GET['m'] == 'add'){
	$addinfo = 'block';
	$msg = $_data['text_15'];
}
if(isset($_GET['m']) && $_GET['m'] == 'up'){
	$addinfo = 'block';
	$msg = $_data['text_17'];
}
?>
<!-- Content Header (Page header) -->

<section class="content-header">
  <h1><?php echo $_data['text_1_1'];?></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo WEB_URL?>dashboard.php"><i class="fa fa-dashboard"></i><?php echo $_data['home_breadcam'];?></a></li>
	<li class="active"><?php echo $_data['text_2'];?></li>
    <li class="active"><?php echo $_data['text_1_1'];?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Full Width boxes (Stat box) -->
<div class="row">
  <div class="col-xs-12">
    <div id="me" class="alert alert-danger alert-dismissable" style="display:<?php echo $delinfo; ?>">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-close"></i></button>
      <h4><i class="icon fa fa-ban"></i> <?php echo $_data['delete_text'];?> !</h4>
      <?php echo $_data['text_18'];?> </div>
    <div id="you" class="alert alert-success alert-dismissable" style="display:<?php echo $addinfo; ?>">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-close"></i></button>
      <h4><i class="icon fa fa-check"></i> <?php echo $_data['success'];?> !</h4>
      <?php echo $msg; ?> </div>
    <div align="right" style="margin-bottom:1%;"> 
		<a class="btn btn-success" data-toggle="tooltip" href="#" onClick="$('#qr_scan').modal('show');" data-original-title="Verificar Invitación"><i class="fa fa-qrcode"></i></a> 	
		
		<?php if($disable == false) { ?>
		<a class="btn btn-success" data-toggle="tooltip" href="<?php echo WEB_URL; ?>e_dashboard/addmeeting.php" data-original-title="<?php echo $_data['text_3'];?>"><i class="fa fa-plus"></i></a> 
		<?php } ?>
		
		<a class="btn btn-success" data-toggle="tooltip" href="<?php echo WEB_URL; ?>dashboard.php" data-original-title="<?php echo $_data['home_breadcam'];?>"><i class="fa fa-dashboard"></i></a> 	
	</div>
    <div class="box box-success">
      <div class="box-header">
        <h3 class="box-title"><?php echo $_data['text_1_1'];?></h3>
      </div>
      <!-- /.box-header -->
	  
	<div id="qr_scan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header green_header">
					<button id="modal_closed" aria-label="Close" data-dismiss="modal" class="close" type="button">
						<span aria-hidden="true"><i class="fa fa-close"></i></span>
					</button>
					<h3 class="modal-title">Escaneé el Código QR de la autorización</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="qr-reader" style="width: 500px; margin: auto;"></div>
							<div id="qr-reader-results"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	
	<div id="table_invitation">
		
	
	</div>

    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<script type="text/javascript">
$(document).ready(function () {
    "use strict";

    $("#table_invitation").load("meetinglist_table.php");

});

function updateInvitation(invitation) {
    $.ajax({
        url: "<?php echo WEB_URL; ?>ajax/updateInvitation.php",
        type: "POST",
        data: {
            invitation: invitation,
        },
        dataType: "json",
        beforeSend: function () {		
			//$("#modal_closed").click();		
		},
        success: function (data, textStatus, jqXHR) {
			
			swal({
			  title: "Verificación de Código QR",
			  text: data.message,
			  type: data.type,
			  showCancelButton: false,
			  confirmButtonClass: "btn-primary",
			  confirmButtonText: "Aceptar",
			  closeOnConfirm: true
			},
			function(){
				$("#qr_sacan_stop").click();
				$("#table_invitation").load("meetinglist_table.php");
			});			
			
			console.log(data.success);
			
		},
        error: function (jqXHR, errorThrown) {
            console.log(formatErrorMessage(jqXHR, errorThrown));
        },
    });
}

function formatErrorMessage(jqXHR, errorThrown) {
    var statusCodes = {};

    statusCodes[(ACCEPTED = 202)] = "Accepted";
    statusCodes[(BAD_GATEWAY = 502)] = "Bad Gateway";
    statusCodes[(BAD_REQUEST = 400)] = "Bad Request";
    statusCodes[(CONFLICT = 409)] = "Conflict";
    statusCodes[(CONTINUE = 100)] = "Continue";
    statusCodes[(CREATED = 201)] = "Created";
    statusCodes[(EXPECTATION_FAILED = 417)] = "Expectation Failed";
    statusCodes[(FAILED_DEPENDENCY = 424)] = "Failed Dependency";
    statusCodes[(FORBIDDEN = 403)] = "Forbidden";
    statusCodes[(GATEWAY_TIMEOUT = 504)] = "Gateway Timeout";
    statusCodes[(GONE = 410)] = "Gone";
    statusCodes[(HTTP_VERSION_NOT_SUPPORTED = 505)] = "HTTP Version Not Supported";
    statusCodes[(IM_A_TEAPOT = 418)] = "I'm a teapot";
    statusCodes[(INSUFFICIENT_SPACE_ON_RESOURCE = 419)] = "Insufficient Space on Resource";
    statusCodes[(INSUFFICIENT_STORAGE = 507)] = "Insufficient Storage";
    statusCodes[(INTERNAL_SERVER_ERROR = 500)] = "Server Error";
    statusCodes[(LENGTH_REQUIRED = 411)] = "Length Required";
    statusCodes[(LOCKED = 423)] = "Locked";
    statusCodes[(METHOD_FAILURE = 420)] = "Method Failure";
    statusCodes[(METHOD_NOT_ALLOWED = 405)] = "Method Not Allowed";
    statusCodes[(MOVED_PERMANENTLY = 301)] = "Moved Permanently";
    statusCodes[(MOVED_TEMPORARILY = 302)] = "Moved Temporarily";
    statusCodes[(MULTI_STATUS = 207)] = "Multi-Status";
    statusCodes[(MULTIPLE_CHOICES = 300)] = "Multiple Choices";
    statusCodes[(NETWORK_AUTHENTICATION_REQUIRED = 511)] = "Network Authentication Required";
    statusCodes[(NO_CONTENT = 204)] = "No Content";
    statusCodes[(NON_AUTHORITATIVE_INFORMATION = 203)] = "Non Authoritative Information";
    statusCodes[(NOT_ACCEPTABLE = 406)] = "Not Acceptable";
    statusCodes[(NOT_FOUND = 404)] = "Not Found";
    statusCodes[(NOT_IMPLEMENTED = 501)] = "Not Implemented";
    statusCodes[(NOT_MODIFIED = 304)] = "Not Modified";
    statusCodes[(OK = 200)] = "OK";
    statusCodes[(PARTIAL_CONTENT = 206)] = "Partial Content";
    statusCodes[(PAYMENT_REQUIRED = 402)] = "Payment Required";
    statusCodes[(PERMANENT_REDIRECT = 308)] = "Permanent Redirect";
    statusCodes[(PRECONDITION_FAILED = 412)] = "Precondition Failed";
    statusCodes[(PRECONDITION_REQUIRED = 428)] = "Precondition Required";
    statusCodes[(PROCESSING = 102)] = "Processing";
    statusCodes[(PROXY_AUTHENTICATION_REQUIRED = 407)] = "Proxy Authentication Required";
    statusCodes[(REQUEST_HEADER_FIELDS_TOO_LARGE = 431)] = "Request Header Fields Too Large";
    statusCodes[(REQUEST_TIMEOUT = 408)] = "Request Timeout";
    statusCodes[(REQUEST_TOO_LONG = 413)] = "Request Entity Too Large";
    statusCodes[(REQUEST_URI_TOO_LONG = 414)] = "Request-URI Too Long";
    statusCodes[(REQUESTED_RANGE_NOT_SATISFIABLE = 416)] = "Requested Range Not Satisfiable";
    statusCodes[(RESET_CONTENT = 205)] = "Reset Content";
    statusCodes[(SEE_OTHER = 303)] = "See Other";
    statusCodes[(SERVICE_UNAVAILABLE = 503)] = "Service Unavailable";
    statusCodes[(SWITCHING_PROTOCOLS = 101)] = "Switching Protocols";
    statusCodes[(TEMPORARY_REDIRECT = 307)] = "Temporary Redirect";
    statusCodes[(TOO_MANY_REQUESTS = 429)] = "Too Many Requests";
    statusCodes[(UNAUTHORIZED = 401)] = "Unauthorized";
    statusCodes[(UNPROCESSABLE_ENTITY = 422)] = "Unprocessable Entity";
    statusCodes[(UNSUPPORTED_MEDIA_TYPE = 415)] = "Unsupported Media Type";
    statusCodes[(USE_PROXY = 305)] = "Use Proxy";

    return "HTTP Status => " + "{Code: " + jqXHR.status + ", Status: " + statusCodes[jqXHR.status] + "}" + " => Error: " + errorThrown;
}

function deleteMeeting(Id) {
    var iAnswer = confirm("<?php echo $_data['delete_confirm']; ?>");
    if (iAnswer) {
        window.location = "<?php echo WEB_URL; ?>e_dashboard/meetinglist.php?id=" + Id;
    }
}
$(document).ready(function () {
    setTimeout(function () {
        $("#me").hide(300);
        $("#you").hide(300);
    }, 3000);
});

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady(function () {
    var resultContainer = document.getElementById("qr-reader-results");
    var lastResult,
        countResults = 0;
    function onScanSuccess(qrCodeMessage) {
        console.log(qrCodeMessage);
		$("#qr_sacan_stop").click();	
		updateInvitation(qrCodeMessage);
    }

    var html5QrcodeScanner = new Html5QrcodeScanner("qr-reader", { fps: 10, qrbox: 250 });
    html5QrcodeScanner.render(onScanSuccess);

    $("#modal_closed").click(function () {
        $("#qr_sacan_stop").click();		
        $("#table_invitation").load("meetinglist_table.php");
    });
});

</script>
<?php include('../footer.php'); ?>