<?php 
session_start();

include("../config.php");

$disable = true;

include(ROOT_PATH.'partial/report_top_common_header.php');
include(ROOT_PATH.'language/'.$lang_code_global.'/lang_authorization_list.php');
include(ROOT_PATH.'library/helper.php');

$ams_helper = new ams_helper();

?>

<script type="text/javascript">

$(function () {
	$('.sakotable').dataTable({
	  "bPaginate": true,
	  "bLengthChange": true,
	  "bFilter": true,
	  "bSort": true,
	  "bInfo": false,
	  "bAutoWidth": false,
	  "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": gobal_url + "dist/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
                //"print",
				//"csv",
				//"xls",
				//"pdf"
            ]
        },
		"order": [[ 0, "desc" ]],
		"language": {
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst": "Primero",
					"sLast": "Último",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				},
				"buttons": {
					"copy": "Copiar",
					"colvis": "Visibilidad"
				}
			}
		
	});
});

</script>

<!-- Content Header (Page header) -->
      <div class="box-body">
        <table class="table sakotable table-bordered table-striped dt-responsive">
          <thead>
            <tr>
			  <th>#</th>
              <th><?php echo $_data['text_5'];?></th>
              <th><?php echo $_data['text_6'];?></th>
              <th>Nombre</th>
              <th>Categoría</th>
              <th>Notas</th>
              <th><?php echo $_data['text_25'];?></th>
              <th><?php echo $_data['text_26'];?></th>
			  <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
				$result = mysqli_query($link,"Select * from tbl_meeting where branch_id = " . (int)$_SESSION['objLogin']['branch_id'] . " and event_type IN ('AUTORIZACION') order by meeting_id desc");
				while($row = mysqli_fetch_array($result)){?>
            <tr>
			  <td><?php echo $row['meeting_id']; ?></td>
              <td><?php echo ($row['issue_date'] != NULL && $row['issue_date'] != "") ? $ams_helper->mySqlToDatePicker($row['issue_date']) : date("d-m-Y h:m:s a",strtotime($row["start_date"])); ?></td>
			  <td><?php echo utf8_encode($row['event_type']); ?></td>
			  <td><?php echo utf8_encode($row['authorization_name']); ?></td>
			  <td><?php echo utf8_encode($row['authorization_category']); ?></td>
			  <td><?php echo utf8_encode($row['authorization_note']); ?></td>
			  <td><?php echo utf8_encode(
						"<b>Fecha Inicio:</b> </br>".date("d-m-Y h:m a",strtotime($row["start_date"]))."</br>".
						"<b>Fecha Fin:</b> </br>".date("d-m-Y h:m a",strtotime($row["end_date"]))
						);
					?>
			  
			  </td>
			  <td><b><?php echo utf8_encode($row['status']); ?></b></td>
              <td>
				  <a class="btn btn-success ams_btn_special" data-toggle="tooltip" href="javascript:;" onClick="$('#nurse_view_<?php echo $row['meeting_id']; ?>').modal('show');" data-original-title="<?php echo $_data['view_text'];?>"><i class="fa fa-eye"></i></a> 
				  
				  <?php if($disable == false) { ?>
				  <a class="btn btn-warning ams_btn_special" data-toggle="tooltip" href="<?php echo WEB_URL;?>e_dashboard/addmeeting.php?id=<?php echo $row['meeting_id']; ?>" data-original-title="<?php echo $_data['edit_text'];?>"><i class="fa fa-pencil"></i></a> 
				  <?php } ?>
				  
				  <a class="btn btn-danger ams_btn_special" data-toggle="tooltip" onclick="deleteMeeting(<?php echo $row['meeting_id']; ?>);" href="javascript:;" data-original-title="<?php echo $_data['delete_text'];?>"><i class="fa fa-trash-o"></i></a>
				  
			  <div id="nurse_view_<?php echo $row['meeting_id']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header green_header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                        <h3 class="modal-title">Detalle de la Autorización</h3>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-xs-12">
                          	<?php echo $row['authorization_note']; ?>
						  </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                </div>  
			  </td>
            </tr>
            <?php } mysqli_close($link);$link = NULL; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->