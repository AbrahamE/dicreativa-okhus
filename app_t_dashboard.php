<?php include('app_header_ten.php');

if(isset($_SESSION['login_type']) && (int)$_SESSION['login_type'] != 4){
	header("Location: " . WEB_URL . "app_logout.php");
	die();
}
 ?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(100%);
    transform: translateX(0px);
    overflow: hidden;
}

.header {
    /*background-color: #FFF!important;*/
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="<?php echo WEB_URL; ?>app_logout.php" class="back-button header-title color-white"></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php" class="back-button header-icon header-icon-1 color-white back-button font-20 left-0"><i class="fa fa-home"></i></a>
	</div>	
	        
    <div class="page-bg"><div></div></div>
	
    <div class="page-content">     
                
        <div data-height="cover-header" class="caption bottom-0">
					<div class="caption-center center-text">

						<div class="landing-page">
							<div class="landing-header">
								<img src="app/images/logo-dark.png" class="responsive-image-full bottom-20">
							</div>	

							<div class="landing-icons">  
							   <a href="#" data-menu="action-reunion">
								   <img src="app/images/iconos/inquilino/invitacion.png">
								   <em>Invitación</em>
							   </a>
							   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php'">
								   <img src="app/images/iconos/inquilino/bitacora.png">
								   <em>Bitacora</em>
							   </a>
							   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addauthorization.php'" style="float: right;">
								   <img src="app/images/iconos/inquilino/autorizacion.png">
								   <em>Autorización</em>
							   </a>							   
								
								<div class="clear"></div>
							</div>	
							<div class="landing-icons">  

							   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_tenant_details.php'">
								   <img src="app/images/iconos/inquilino/estado_de_cuenta.png">
								   <em>Estado de Cuenta</em>
							   </a>
							   
							   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_notice.php?aviso=general'">
								   <img src="app/images/iconos/inquilino/aviso.png">
								   <em>Aviso</em>
							   </a>
							   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_complainlist.php'" style="float: right;">
								   <img src="app/images/iconos/inquilino/incidencia.png">
								   <em>Incidencia</em>
							   </a>							   
								
								<div class="clear"></div>
							</div>	
							<div class="landing-icons">  

								
								<div class="clear"></div>
							</div>	
							<!--div class="landing-icons">  
							    <a href="#" data-menu="action-comite">
								   <img src="app/images/iconos/inquilino/directorio.png">
								   <em>Directorio</em>
							   </a>
							   <a href="#" data-menu="action-facturas">
								   <img src="app/images/iconos/inquilino/finanza.png">
								   <em>Finanza</em>
							   </a>
							   
								<a href="#" onclick="window.location.href = '<?php //echo WEB_URL; ?>app_t_dashboard.php'" style="float: right;">
									<img src="app/images/iconos/inquilino/oferta.png">
									<em>Oferta de Servicio</em>
								</a>   					
								<div class="clear"></div>							   
								
							</div-->										

						</div>
				</div>
            </div>
        </div> 	
	
	
	<div class="page-content header-clear-large" style="display:none;">
			
			<div class="landing-page">			
				<div class="landing-icons" style="display:block;">
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app_t_dashboard_data.php'">
				   <i class="bg-gradient-red2 shadow-huge fa fa-chart-line"></i>
				   <em><?php echo $_data['menu_dashboard']; ?></em>
				   </a>
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/tenant_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-file-alt"></i>
				   <em><?php echo $_data['rented_statement']; ?></em>
				   </a>

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/unit_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-list"></i>
				   <em><?php echo $_data['unit_details']; ?></em>
				   </a>

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/complainlist.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-comments"></i>
				   <em><?php echo $_data['renter_complain_details']; ?></em>
				   </a>

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/r_report.php'">
				   <i class="bg-gradient-blue2 shadow-huge fas fa-chart-bar"></i>
				   <em><?php echo $_data['rented_report']; ?></em>
				   </a>				   
				   				   
				   <div class="clear"></div>				
				</div>				
			</div>			
	</div>
	
    <div id="action-reunion"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_meeting']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_meeting']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_addmeeting.php'"><?php echo $_data['menu_add_meeting']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/app_listmeeting.php'"><?php echo $_data['menu_meeting_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	
    <div id="action-facturas"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_bill']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_bill']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/add_bill.php'"><?php echo $_data['menu_add_bill']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/bill_list.php'"><?php echo $_data['menu_bill_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	
    <div id="action-comite"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="180"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_management_committee']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_management_committee']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/add_m_committee.php'"><?php echo $_data['menu_add_member']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>t_dashboard/m_committee_list.php'"><?php echo $_data['menu_member_list']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>	
	
    <div class="menu-hider"></div>
</div>
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
</body>