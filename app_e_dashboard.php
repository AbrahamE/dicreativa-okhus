<?php
include('app_header_emp.php');

if(!isset($_SESSION['objLogin'])){
	header("Location: ".WEB_URL."app_logout.php");
	die();
}
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title><?php echo $building_name; ?></title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">

<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">  
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 11px!important;
}

</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
	
	<div class="header header-fixed header-logo-app">
        <a href="#" class="back-button header-logo"></a>
		<a href="<?php echo WEB_URL; ?>app_logout.php" class="header-icon header-icon-1 back-button"><i class="fas fa-arrow-left"></i></a>
		<!--a href="#" class="header-icon header-icon-3" data-toggle-theme style="right: 0px;"><i class="fas fa-moon"></i></a-->
	</div>	
                  
    <div class="page-bg"><div></div></div>
	
	<div class="page-content header-clear-large">
			
			<div class="landing-page">			
				<div class="landing-icons" style="display:block;">
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>app_e_dashboard_data.php'">
				   <i class="bg-gradient-red2 shadow-huge fa fa-chart-line"></i>
				   <em><?php echo $_data['menu_dashboard']; ?></em>
				   </a>
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/rented_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-users"></i>
				   <em><?php echo $_data['rented_details']; ?></em>
				   </a>				   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/member_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user-circle"></i>
				   <em><?php echo $_data['member_details']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/ownerlist.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-user"></i>
				   <em><?php echo $_data['owner_details']; ?></em>
				   </a>						   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/leave_request_list.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-bell"></i>
				   <em><?php echo $_data['leave_request']; ?></em>
				   </a>				   
				   
				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/visitorlist.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-car"></i>
				   <em><?php echo $_data['e_visitor_list']; ?></em>
				   </a>	

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/complain.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-comments"></i>
				   <em><?php echo $_data['text_complain_job']; ?></em>
				   </a>		

				   <a href="#" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/salary_details.php'">
				   <i class="bg-gradient-blue2 shadow-huge fa fa-money-bill"></i>
				   <em><?php echo $_data['salary_statement']; ?></em>
				   </a>	

				   <a href="#" data-menu="action-reportes">
				   <i class="bg-gradient-blue2 shadow-huge fas fa-chart-bar"></i>
				   <em><?php echo $_data['salary_report']; ?></em>
				   </a>				   
				   				   
				   <div class="clear"></div>				
				</div>				
			</div>			
	</div>

    <div id="action-reportes"
         class="menu-box menu-box-detached round-medium"
         data-menu-type="menu-box-bottom"
         data-menu-height="150"
         data-menu-effect="menu-over">
        
        <div class="content bottom-0">
            <h3 class="font-900 top-20 bottom-0"><?php echo $_data['menu_report']; ?></h3>
            <p class="under-heading color-blue2-dark bottom-10 font-11"><?php echo $_data['menu_report']; ?></p>
            <a href="#" class="close-menu close-menu-icon"><i class="fa fa-times-circle color-red2-dark"></i></a>
        </div>
        
        <div class="content bottom-0">
            <div class="one-half">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/e_report.php'"><?php echo $_data['salary_report_text']; ?></a>
            </div>
            <div class="one-half last-column">
                <a href="#" class="close-menu button button-full button-s round-small shadow-huge bg-blue2-dark" onclick="window.location.href = '<?php echo WEB_URL; ?>e_dashboard/v_report.php'"><?php echo $_data['visitor_report_text']; ?></a>
            </div>
            <div class="clear"></div>
        </div>
		
    </div>		
	
    <div class="menu-hider"></div>
</div>
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
</body>