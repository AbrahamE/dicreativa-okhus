<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe de salario del vigilante";
$_data['text_2'] = "Fecha de emisión";
$_data['text_3'] = "Nombre";
$_data['text_4'] = "Designación";
$_data['text_5'] = "Mes de salario";
$_data['text_6'] = "Año salarial";
$_data['text_7'] = "Salario";
$_data['text_15'] = "Total";
$_data['text_16'] = "Imprimir información";
?>