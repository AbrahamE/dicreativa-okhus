<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe de visitantes";
$_data['text_2'] = "Informe";
$_data['text_3'] = "Formulario de informe de visitantes";
$_data['text_4'] = "Seleccionar fecha";
$_data['text_5'] = "Seleccionar mes";
$_data['text_6'] = "Seleccionar año";
$_data['required'] = "Seleccione al menos uno o más campos";
?>