<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Tablón de anuncios del inquilino";
$_data['text_12'] = "Tablón de anuncios del vigilante";
$_data['text_13'] = "Tablón de anuncios del propietario";
$_data['text_2'] = "Formulario de ingreso de aviso";
$_data['text_2a'] = "Título del aviso";
$_data['text_2b'] = "Fecha de aviso";
$_data['text_3'] = "Título del aviso";
$_data['text_33'] = "Estado del aviso";
$_data['text_3a'] = "Estado del aviso";
$_data['text_3b'] = "Publicar ahora";
$_data['text_3bb'] = "Aviso";
$_data['text_3c'] = "Desactivar";
$_data['text_4'] = "Lista de avisos";
$_data['text_5'] = "Restablezca primero antes de insertar y puede publicar un aviso a la vez que mostrar el tablero del inquilino ";
$_data['text_55'] = "Restablezca primero antes de insertar y puede publicar un aviso a la vez que mostrar el tablero de los vigilantes ";
$_data['text_555'] = "Restablezca primero antes de insertar y puede publicar un aviso a la vez que mostrar el tablero del propietario ";
$_data['text_6'] = "Detalles del aviso";
$_data['text_7'] = "Se agregó la información del aviso correctamente";
$_data['text_8'] = "La información de aviso actualizada se realizó correctamente";
$_data['text_9'] = "Información de aviso eliminada correctamente";
//validar
$_data['v_1'] = "¡Se requiere título del aviso!";
$_data['v_2'] = "Aviso Descripción requerida !!!";
$_data['v_3'] = "¡Se requiere aviso de fecha de creación !!!";
//confirmar
$_data['confirm'] = "¿Está seguro de que desea eliminar este Aviso?";
?>