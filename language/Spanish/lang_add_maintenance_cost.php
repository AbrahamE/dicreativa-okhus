<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_title_text'] = "Agregar nuevo costo de mantenimiento";
$_data['add_msg'] = "Costo de mantenimiento agregado exitoso";
$_data['update_title_text'] = "Actualizar costo de mantenimiento";
$_data['update_msg'] = "Costo de mantenimiento actualizado exitosamente";
$_data['maintenance_cost'] = "Costo de mantenimiento";
$_data['add_m_cost'] = "Agregar costo de mantenimiento";
$_data['m_cost_entry_form'] = "Formulario de ingreso de costos de mantenimiento";
$_data['date'] = "Fecha";
$_data['month'] = "Mes";
$_data['select_month'] = "Seleccionar mes";
$_data['year'] = "Año";
$_data['select_year'] = "Seleccionar año";
$_data['text_1'] = "Título de mantenimiento";
$_data['text_2'] = "Cantidad";
$_data['text_3'] = "Detalles";
//validación
$_data['v1'] = "Seleccionar fecha de emisión";
$_data['v2'] = "Seleccionar mes de costo";
$_data['v3'] = "Seleccionar año de costo";
$_data['v4'] = "Título de mantenimiento requerido";
$_data['v5'] = "Cantidad de mantenimiento requerida";
$_data['v6'] = "Detalles de mantenimiento requeridos";
?>