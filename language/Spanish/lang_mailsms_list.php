<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['list_title'] = "Lista de correo electrónico / SMS";
$_data['add_new_floor_information_breadcam'] = "Correo electrónico / SMS";
$_data['delete_floor_information'] = "Información de notificación eliminada correctamente";
$_data['list_1'] = "Fecha";
$_data['list_2'] = "Asunto";
$_data['list_3'] = "Mensaje";
$_data['list_4'] = "Tipo de notificación";
$_data['list_5'] = "Lista de remitentes";
$_data['add_sms'] = "Enviar correo electrónico / SMS";
$_data['text_1'] = "* Nota: Seleccione todos los usuarios o usuarios específicos para enviar sms o correo electrónico.";
$_data['text_2'] = "Asunto del mensaje";
$_data['text_3'] = "Mensaje";
$_data['text_4'] = "Todos";
$_data['text_5'] = "Inquilino específico";
$_data['text_6'] = "Propietario específico";
$_data['text_7'] = "Vigilante específico";
$_data['text_8'] = "Escriba el nombre del inquilino aquí ...";
$_data['text_9'] = "Escriba el nombre del propietario aquí ...";
$_data['text_10'] = "Escriba el nombre del vigilante aquí ...";
$_data['text_11'] = "Tipo de mensaje";
$_data['text_12'] = "SMS";
$_data['text_13'] = "Correo electrónico";
$_data['text_14'] = "ENVIAR NOTIFICACIÓN";
$_data['text_15'] = "CERRAR";
$_data['text_16'] = "Ya agregado";
$_data['text_17'] = "SMS y correo electrónico";
$_data['text_18'] = "Inquilino";
$_data['text_19'] = "Propietario";
$_data['text_20'] = "Vigilante";
$_data['text_21'] = "Antes de enviar correos electrónicos o sms, debe configurar su correo electrónico y configuración de sms desde ";
$_data['text_22'] = "De lo contrario, no puede enviar correos electrónicos o sms.";
$_data['text_23'] = "aquí";
$_data['add_msg'] = "Se agregó la información de notificación correctamente";
$_data['update_msg'] = "Actualizar la información de notificación con éxito";
$_data['confirm'] = "¿Está seguro de que desea eliminar esta Notificación?";
?>