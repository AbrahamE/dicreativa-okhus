<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['button_text_save'] = "Guardar información";
$_data['button_text_update'] = "Actualizar información";
$_data['btn_reset'] = "Restablecer";
$_data['label_warning'] = "Restablezca primero antes de insertar";
$_data['home'] = "Inicio";
$_data['settings'] = "Configuración";
$_data['page_title'] = "Configuración de administrador";
$_data['page_form'] = "Formulario de configuración de administrador";
$_data['label_name'] = "Nombre";
$_data['label_email'] = "Correo electrónico (debe ser único)";
$_data['label_password'] = "Contraseña";
$_data['label_branch'] = "Seleccionar rama";
$_data['label_image'] = "Foto";
$_data['button_text_update'] = "Actualizar información";
$_data['save_success'] = "Se agregó la información de administración correctamente";
$_data['save_update'] = "Información de administración actualizada correctamente";
$_data['delete_success'] = "Información de administración eliminada correctamente";
//lista
$_data['admin_list'] = "Lista de administradores";
$_data['admin_details'] = "Detalles del administrador";
$_data['column_0'] = "Foto";
$_data['column_1'] = "Nombre";
$_data['column_2'] = "Correo electrónico";
$_data['column_33'] = "Contacto";
$_data['profile_update_contact'] = "No de contacto (con código de país)";
$_data['column_3'] = "Rama";
$_data['column_4'] = "Acción";
//validación
$_data['email_exist'] = "¡El correo electrónico ya existe!";
$_data['email_exist_warning'] = "Advertencia";
$_data['required_1'] = "¡Se requiere correo electrónico de administrador y debe ser único!";
$_data['required_2'] = "¡¡¡Se requiere nombre de administrador !!!";
$_data['required_3'] = "¡Se requiere contraseña de administrador!";
$_data['required_4'] = "Seleccione la rama de administración !!!";
$_data['confirm_delete'] = "¿Está seguro de que desea eliminar este administrador?";
?>