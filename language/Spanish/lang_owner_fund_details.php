<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista de fondos";
$_data['text_2'] = "Nombre del propietario";
$_data['text_3'] = "Mes";
$_data['text_4'] = "Año";
$_data['text_5'] = "Fecha";
$_data['text_6'] = "Cantidad";
$_data['text_7'] = "Propósito";
$_data['text_8'] = "Detalles del fondo";
$_data['text_9'] = "Total";
$_data['text_10'] = "Fecha de emisión";
?>