<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['employee_list'] = "Lista de vigilantes";
$_data['employee_details'] = "Detalles del vigilante";
$_data['add_new_employee_information_breadcam'] = "Información del vigilante";
$_data['add_new_employee_breadcam'] = "Agregar vigilante";
$_data['imgage'] = "imagen";
$_data['add_new_form_field_text_1'] = "Nombre";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID (ID nacional del vigilante)";
$_data['add_new_form_field_text_8'] = "Designación";
$_data['add_new_form_field_text_9'] = "Fecha de incorporación";
$_data['add_new_form_field_text_10'] = "Fecha de finalización";
$_data['add_new_form_field_text_11'] = "Estado";
$_data['add_new_form_field_text_12'] = "Activo";
$_data['add_new_form_field_text_13'] = "Inactivo";
$_data['add_new_form_field_text_14'] = "Priview";
$_data['add_new_form_field_text_15'] = "Seleccionar";
$_data['add_new_form_field_text_16'] = "Fecha de finalización";
$_data['added_employee_successfully'] = "Se agregó la información del vigilante correctamente";
$_data['update_employee_successfully'] = "La información actualizada del vigilante se realizó correctamente";
$_data['delete_employee_information'] = "Información del vigilante eliminada exitosamente";
$_data['employee_confirm_delete_msg'] = "¿Está seguro de que desea eliminar a este vigilante?";
$_data['employee_salary'] = "Salario por mes";
?>