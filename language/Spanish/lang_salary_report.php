<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe de salario del vigilante";
$_data['text_2'] = "Informe";
$_data['text_3'] = "Formulario de informe de salario del vigilante";
$_data['text_4'] = "Seleccionar vigilante";
$_data['text_6'] = "Seleccionar mes";
$_data['text_66'] = "Seleccionar año";
$_data['text_7'] = "Enviar";
$_data['validation'] = "Seleccione al menos uno o más campos";
?>