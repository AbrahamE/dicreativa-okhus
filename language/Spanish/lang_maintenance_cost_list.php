<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['m_cost'] = "Mantenimiento";
$_data['m_cost_details'] = "Detalles del costo de mantenimiento";
$_data['add_title_text'] = "Lista de costos de mantenimiento";
$_data['add_msg'] = "Costo de mantenimiento agregado exitoso";
$_data['update_title_text'] = "Actualizar costo de mantenimiento";
$_data['update_msg'] = "Costo de mantenimiento actualizado exitosamente";
$_data['delete_msg'] = "Costo de mantenimiento eliminado con éxito";
$_data['maintenance_cost'] = "Costo de mantenimiento";
$_data['add_m_cost'] = "Agregar costo de mantenimiento";
$_data['m_cost_entry_form'] = "Formulario de ingreso de costos de mantenimiento";
$_data['date'] = "Fecha";
$_data['month'] = "Mes";
$_data['select_month'] = "Seleccionar mes";
$_data['year'] = "Año";
$_data['select_year'] = "Seleccionar año";
$_data['text_1'] = "Título de mantenimiento";
$_data['text_2'] = "Cantidad";
$_data['text_3'] = "Detalles";
$_data['confirm'] = "¿Está seguro de que desea eliminar este costo de mantenimiento?";
?>