<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración de moneda";
$_data['text_2'] = "Formulario de ingreso de moneda";
$_data['text_3'] = "Símbolo";
$_data['text_33'] = "Nombre";
$_data['text_4'] = "Lista de divisas";
$_data['text_5'] = "Se agregó la información de moneda correctamente";
$_data['text_6'] = "La información de la moneda se actualizó correctamente";
$_data['text_7'] = "Información de moneda eliminada con éxito";
$_data['text_8'] = "Detalles de la moneda";
//validación
$_data['required_1'] = "¡Se requiere el símbolo de moneda!";
$_data['required_2'] = "¡Se requiere nombre de moneda!";
$_data['confirm_delete'] = "¿Está seguro de que desea eliminar esta moneda?";
?>