<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['menu_dashboard'] = "Tablero";
$_data['menu_floor'] = "Información del piso";
$_data['menu_floor_list'] = "Lista de piso";
$_data['menu_floor_add'] = "Agregar piso";
$_data['menu_unit_information'] = "Información de la unidad";
$_data['menu_unit_list'] = "Lista de unidades";
$_data['menu_add_unit'] = "Agregar unidad";
$_data['menu_owner_information'] = "Información del propietario";
$_data['menu_owner_list'] = "Lista de propietarios";
$_data['menu_add_owner'] = "Agregar propietario";
$_data['menu_renter_information'] = "Inquilino";
$_data['menu_renter_list'] = "Lista de inquilinos";
$_data['menu_add_renter'] = "Agregar inquilino";
$_data['menu_employee_information'] = "Vigilante";
$_data['menu_employee_list'] = "Lista de vigilantes";
$_data['menu_add_employee'] = "Agregar vigilante";
$_data['menu_employee_leave'] = "Solicitud de licencia del vigilante";
$_data['menu_rent_collection'] = "Renta";
$_data['menu_rent_list'] = "Lista de alquileres";
$_data['menu_add_rent'] = "Agregar alquiler";
$_data['menu_owner_utility'] = "Utilidad del propietario";
$_data['menu_owner_utility_list'] = "Lista de utilidades del propietario";
$_data['menu_add_owner_utility'] = "Agregar utilidad de propietario";
$_data['menu_maintenance_cost'] = "Mantenimiento";
$_data['menu_maintenance_cost_list'] = "Lista de costos de mantenimiento";
$_data['menu_add_maintenance_cost'] = "Agregar costo de mantenimiento";
$_data['menu_management_committee'] = "Comité de gestión";
$_data['menu_member_list'] = "Lista de miembros";
$_data['menu_add_member'] = "Agregar miembro";
$_data['menu_fund'] = "Fondo";
$_data['menu_fund_list'] = "Lista de fondos";
$_data['menu_add_fund'] = "Agregar fondo";
$_data['menu_bill'] = "Depósito de facturas";
$_data['menu_bill_list'] = "Lista de facturas";
$_data['menu_add_bill'] = "Agregar factura";
$_data['menu_building_info'] = "Configuración de detalles de construcción";
$_data['menu_complain'] = "Incidencia";
$_data['menu_complain_list'] = "Lista de incidencias";
$_data['menu_add_complain'] = "Agregar incidencia";
$_data['menu_visitor'] = "Visitante";
$_data['menu_visitor_list'] = "Lista de visitantes";
$_data['menu_add_visitor'] = "Agregar visitante";
$_data['menu_meeting'] = "Invitación";
$_data['menu_meeting_list'] = "Listar Invitaciones";
$_data['menu_add_meeting'] = "Registrar Invitación";
$_data['menu_report'] = "Informe";
$_data['menu_fair_report'] = "Informe de alquiler";
$_data['menu_rented_report'] = "Informe del inquilino";
$_data['menu_visitors_report'] = "Informe de visitantes";
$_data['menu_complain_report'] = "Informe de incidencia";
$_data['menu_unit_status_report'] = "Informe del estado de la unidad";
$_data['menu_fund_status'] = "Estado del fondo";
$_data['menu_bill_report'] = "Informe de factura";
$_data['menu_settings'] = "Configuración";
$_data['menu_bill_setup'] = "Configuración del tipo de factura";
$_data['menu_utility_bill'] = "Configuración de factura de servicios públicos";
$_data['menu_employee_setup'] = "Salario del vigilante";
$_data['menu_management_member_type'] = "Tipo de miembro de administración";
$_data['menu_month_setup'] = "Configuración del mes";
$_data['menu_year_setup'] = "Configuración del año";
$_data['menu_currency_setup'] = "Configuración de moneda";
$_data['menu_language_panel'] = "Configuración de idioma";
$_data['menu_language_setup'] = "Configuración del sistema";
$_data['menu_branch'] = "Rama";
$_data['menu_add_branch'] = "Agregar edificio";
$_data['menu_branch_list'] = "Lista de construcción";
$_data['menu_admin_setup'] = "Configuración de administrador";
$_data['branch'] = "Edificio";
$_data['notice_board'] = "Tablón de anuncios";
$_data['email_sms'] = "Alertas";
$_data['notice_board_1'] = "Tablón de anuncios del inquilino";
$_data['notice_board_2'] = "Tablón de anuncios del vigilante";
$_data['notice_board_3'] = "Tablón de anuncios del propietario";
$_data['add_branch'] = "Agregar edificio";
$_data['branch_list'] = "Lista de construcción";
/*Propietario Menú izquierdo Inicio*/
$_data['text_1'] = "Panel del propietario";
$_data['text_2'] = "Detalles de la unidad";
$_data['text_3'] = "Detalles del inquilino";
$_data['text_4'] = "Información de detalles del vigilante";
$_data['text_5'] = "Declaración de renta";
$_data['text_6'] = "Declaración de utilidad del propietario";
$_data['text_7'] = "Detalles del costo de mantenimiento";
$_data['text_8'] = "Información de detalles del fondo";
$_data['text_88'] = "Incidencia";
$_data['text_complain_job'] = "Asignar trabajo de incidencia";
$_data['text_9'] = "Informe alquilado";
/*Propietario Menú izquierdo Fin*/
/*vigilante Menú izquierdo Inicio*/
$_data['leave_request'] = "Solicitud de licencia";
$_data['salary_statement'] = "Declaración de salario";
$_data['e_visitor_list'] = "Visitante";
$_data['salary_report'] = "Informe";
$_data['salary_report_text'] = "Informe de salario";
$_data['visitor_report_text'] = "Informe de visitante";
$_data['rented_details'] = "Detalles del inquilino";
$_data['member_details'] = "Comité de gestión";
$_data['owner_details'] = "Detalles del propietario";
$_data['dashboard_emp'] = "Vigilante del tablero";
$_data['control'] = "Panel de control";
/*Fin del menú izquierdo del vigilante*/
/*Renter Menú izquierdo Inicio*/
$_data['rented_statement'] = "Declaración de renta";
$_data['rented_report'] = "Informe alquilado";
$_data['unit_details'] = "Detalles del inquilino";
$_data['renter_complain_details'] = "El inquilino se incidencia";
$_data['dashboard_total_branch'] = "Rama total";
/*Renter Izquierda Menú Fin*/
?>