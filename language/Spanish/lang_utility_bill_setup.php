<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración de factura de servicios públicos";
$_data['text_2'] = "Formulario de ingreso de facturas de servicios públicos";
$_data['text_3'] = "Factura de gas por piso";
$_data['text_4'] = "Factura de seguridad por piso";
$_data['text_5'] = "Se agregó la información de la factura de servicios públicos con éxito";
$_data['text_6'] = "La información actualizada de la factura de servicios públicos se realizó correctamente";
$_data['text_7'] = "Información de factura de servicios públicos eliminada con éxito";
$_data['text_8'] = "Lista de facturas de servicios públicos";
$_data['text_9'] = "Detalles de la factura de servicios públicos";
?>