<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista de facturas";
$_data['text_1_1'] = "Detalles de la factura";
$_data['text_2'] = "Información de la factura";
$_data['text_3'] = "Agregar factura";
$_data['text_4'] = "Formulario de ingreso de facturas";
$_data['text_5'] = "Tipo de factura";
$_data['text_6'] = "Seleccionar tipo";
$_data['text_7'] = "Fecha de emisión";
$_data['text_8'] = "Mes de la factura";
$_data['text_9'] = "Seleccionar mes";
$_data['text_10'] = "Año de factura";
$_data['text_11'] = "Seleccionar año";
$_data['text_12'] = "Cantidad total";
$_data['text_13'] = "Nombre del banco de depósito";
$_data['text_14'] = "Detalles";
$_data['text_15'] = "Se agregó la información de la factura correctamente";
$_data['text_16'] = "La información actualizada de la factura se realizó correctamente";
$_data['text_17'] = "Información de factura eliminada con éxito";
$_data['text_18'] = "¿Está seguro de que desea eliminar esta información de factura?";
?>