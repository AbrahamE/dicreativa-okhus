<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe de cobro de tarifas";
$_data['text_22'] = "# Factura";
$_data['text_2'] = "Fecha";
$_data['text_3'] = "Nombre";
$_data['text_4'] = "Tipo";
$_data['text_5'] = "Piso";
$_data['text_6'] = "Unidad";
$_data['text_7'] = "Mes";
$_data['text_77'] = "Año";
$_data['text_8'] = "Alquiler";
$_data['text_9'] = "Factura de gas";
$_data['text_10'] = "Factura eléctrica";
$_data['text_11'] = "Factura de agua";
$_data['text_12'] = "Factura de seguridad";
$_data['text_13'] = "Factura de servicios públicos";
$_data['text_14'] = "Otra factura";
$_data['text_15'] = "Total";
$_data['text_16'] = "Imprimir información";
?>