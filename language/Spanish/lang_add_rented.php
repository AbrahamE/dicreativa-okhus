<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_renter'] = "Agregar nuevo inquilino";
$_data['update_rent'] = "Actualizar alquiler";
$_data['add_new_renter_entry_form'] = "Formulario de inscripción del inquilino";
$_data['add_new_form_field_text_1'] = "Nombre del inquilino";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto (con código de país)";
$_data['add_new_form_field_text_5'] = "Dirección";
$_data['add_new_form_field_text_6'] = "NID (ID nacional del inquilino)";
$_data['add_new_form_field_text_7'] = "Número de piso";
$_data['add_new_form_field_text_8'] = "Número de unidad disponible";
$_data['add_new_form_field_text_9'] = "Alquiler anticipado";
$_data['add_new_form_field_text_10'] = "Alquiler por mes";
$_data['add_new_form_field_text_11'] = "Fecha de emisión";
$_data['add_new_form_field_text_12'] = "Mes de alquiler";
$_data['add_new_form_field_text_13'] = "Año de alquiler";
$_data['add_new_form_field_text_14'] = "Estado del inquilino";
$_data['add_new_form_field_text_15'] = "Foto del inquilino";
$_data['add_new_form_field_text_16'] = "Activo";
$_data['add_new_form_field_text_17'] = "In-Active";
$_data['select_floor'] = "Seleccionar piso";
$_data['select_unit'] = "Seleccionar unidad";
$_data['select_month'] = "Seleccionar mes";
$_data['select_year'] = "Seleccionar año";
$_data['add_new_renter_information_breadcam'] = "Inquilino";
$_data['add_new_renter_breadcam'] = "Agregar inquilino";
$_data['added_renter_successfully'] = "Se agregó la información del inquilino con éxito";
$_data['update_renter_successfully'] = "La información del inquilino se actualizó correctamente";
$_data['delete_renter_information'] = "La información del inquilino eliminada se realizó correctamente";
$_data['email_exist'] = "El correo electrónico del inquilino ya existe";
//validación
$_data['required_1'] = "¡Se requiere el nombre del inquilino!";
$_data['required_2'] = "¡Se requiere correo electrónico del inquilino!";
$_data['required_3'] = "¡Se requiere contraseña de inquilino!";
$_data['required_4'] = "¡Se requiere el número de contacto del inquilino!";
$_data['required_5'] = "¡Se requiere la dirección del inquilino!";
$_data['required_6'] = "¡Se requiere NID del inquilino!";
$_data['required_7'] = "¡Se requiere piso del inquilino!";
$_data['required_8'] = "¡Unidad de inquilino requerida!";
$_data['required_9'] = "¡Se requiere alquiler anticipado del inquilino!";
$_data['required_10'] = "¡Se requiere alquiler de inquilino por mes!";
$_data['required_11'] = "¡Se requiere la fecha de alquiler del inquilino!";
$_data['required_12'] = "¡Se requiere mes de inquilino!";
$_data['required_13'] = "¡Año del inquilino requerido!";
?>