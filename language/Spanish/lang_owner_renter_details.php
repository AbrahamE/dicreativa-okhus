<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Detalles del inquilino";
$_data['text_2'] = "Nombre del inquilino";
$_data['text_3'] = "Correo electrónico";
$_data['text_4'] = "Contacto";
$_data['text_5'] = "Dirección";
$_data['text_6'] = "Floor No";
$_data['text_7'] = "Número de unidad";
$_data['text_8'] = "Alquiler anticipado";
$_data['text_9'] = "Fecha de alquiler";
$_data['text_10'] = "Alquiler por mes";
?>