<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración del tipo de factura";
$_data['text_2'] = "Formulario de entrada de tipo de factura";
$_data['text_3'] = "Tipo de factura";
$_data['text_4'] = "Lista de tipos de factura";
$_data['text_5'] = "Restablezca primero antes de insertar";
$_data['text_6'] = "Detalles del tipo de factura";
$_data['text_7'] = "Se agregó la información del tipo de factura correctamente";
$_data['text_8'] = "La información actualizada del tipo de factura se realizó correctamente";
$_data['text_9'] = "Información de tipo de factura eliminada correctamente";
//validar
$_data['v_1'] = "¡Tipo de factura requerido!";
//confirmar
$_data['confirm'] = "¿Está seguro de que desea eliminar este tipo de factura?";
?>