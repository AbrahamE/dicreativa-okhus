<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Panel de modificación del idioma";
$_data['text_2'] = "Formulario de ingreso del año";
$_data['text_3'] = "Nombre del año";
$_data['text_4'] = "Lista de años";
$_data['text_5'] = "Se agregó la información del año correctamente";
$_data['text_6'] = "La información actualizada del año se realizó correctamente";
$_data['text_7'] = "Información de año eliminada exitosamente";
$_data['text_8'] = "Detalles del año";
//validación
$_data['required_1'] = "¡Texto del año requerido!";
$_data['confirm_delete'] = "¿Está seguro de que desea eliminar este año?";
?>