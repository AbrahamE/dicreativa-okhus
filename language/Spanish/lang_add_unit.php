<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_unit'] = "Agregar nueva unidad";
$_data['update_unit'] = "Actualizar unidad";
$_data['add_new_unit_entry_form'] = "Formulario de ingreso a la unidad";
$_data['add_new_form_field_text_1'] = "Número de piso";
$_data['add_new_form_field_text_2'] = "Número de unidad";
$_data['add_new_unit_information_breadcam'] = "Información de la unidad";
$_data['add_new_unit_breadcam'] = "Agregar unidad";
$_data['select_floor'] = "Seleccionar piso";
$_data['add_unit_successfully'] = "Agregar unidad con éxito";
$_data['update_unit_successfully'] = "Actualizar la unidad con éxito";
//validación
$_data['floor_required'] = "Seleccionar piso.";
$_data['unit_required'] = "Unidad requerida";
?>