<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['dashboard_title'] = "Tablero";
$_data['dashboard_more_info'] = "Más información";
$_data['dashboard_total_floor'] = "Piso total";
$_data['dashboard_total_unit'] = "Unidad total";
$_data['dashboard_total_owner'] = "Propietario";
$_data['dashboard_total_rented'] = "Inquilino";
$_data['dashboard_total_employee'] = "Vigilante";
$_data['dashboard_total_committee'] = "Comité gestión";
$_data['dashboard_total_fare'] = "Renta";
$_data['dashboard_total_maintenance'] = "Mantenimiento";
$_data['dashboard_total_fund'] = "Fondo";
$_data['dashboard_total_owner_utility'] = "Utilidad de propietario";
$_data['dashboard_report'] = "Salario del vigilante";
$_data['dashboard_settings'] = "Configuración";
$_data['dashboard_total_complain'] = "Incidencia";
/*Inicio del panel de control del propietario*/
$_data['text_1'] = "Mi unidad";
$_data['text_2'] = "Mi inquilino";
$_data['text_3'] = "Vigilante";
$_data['text_4'] = "Mi renta total";
$_data['text_5'] = "Utilidad de propietario";
$_data['text_6'] = "Informe alquilado";
$_data['text_7'] = "Mantenimiento";
$_data['text_8'] = "Fondo";
$_data['owner_dashboard'] = "Tablero del propietario";
$_data['control_panel'] = "Panel de control";
$_data['graph_text_1'] = "Gráfico de factura de depósito mensual por año";
$_data['graph_text_2'] = "Gráfico de alquiler mensual del inquilino por año";
$_data['o_text_1'] = "Lista de incidencias";
$_data['o_text_1_1_1'] = "Detalles de la incidencia";
$_data['o_text_2'] = "Registar incidencia";
$_data['o_text_3'] = "Agregar incidencia";
$_data['o_text_4'] = "Formulario de inscripción de incidencia";
$_data['o_text_5'] = "Título";
$_data['o_text_6'] = "Descripción";
$_data['o_text_7'] = "Fecha";
$_data['o_text_11'] = "Mes";
$_data['o_text_12'] = "Año";
/*Fin del tablero del propietario*/
/*Inicio del panel de control del vigilante*/
$_data['dashboard_emp'] = "Panel del vigilante";
$_data['control'] = "Panel de control";
$_data['salary_statement'] = "Declaración de salario";
$_data['salary_report'] = "Licencia total";
$_data['e_total_owner'] = "Propietario";
$_data['rented_details'] = "Inquilino";
$_data['member_details'] = "Comité de gestión";
$_data['dash'] = "Tablero";
$_data['leave_graph_header_text'] = "Gráfico de visitante mensual";
$_data['leave_graph_no_record'] = "No se encontraron licencias aceptadas";
$_data['salary_header_text'] = "Gráfico de salario mensual";
$_data['e_last_5_visitors'] = "Últimos 5 visitantes";
$_data['ee_text_5'] = "Fecha de emisión";
$_data['ee_text_6'] = "Nombre";
$_data['ee_text_7'] = "Móvil";
$_data['ee_text_8'] = "Dirección";
$_data['ee_text_9'] = "Floor No";
$_data['ee_text_10'] = "Seleccionar piso";
$_data['ee_text_11'] = "Unidad No";
$_data['ee_text_12'] = "Seleccionar unidad";
$_data['ee_text_13'] = "A tiempo";
$_data['ee_text_14'] = "Tiempo de espera";
/*Final del panel de vigilantes*/
/*Renter Dashboard Start*/
$_data['rented_statement'] = "Declaración alquilada";
$_data['rented_report'] = "Informe alquilado";
$_data['infomation'] = "Mesa de ayuda";
$_data['unit_details'] = "Detalles de la unidad";
$_data['rented_dashboard'] = "Panel del inquilino";
$_data['c_panel'] = "Panel de control";
$_data['renter_complain'] = "Incidencia total";
$_data['graph_text_11'] = "Gráfico de alquiler mensual por año";
$_data['graph_text_111'] = "Lista de las 5 últimas incidencias";
/*Renter Dashboard End*/
// texto de tabla de incidencia
$_data['c_table_header'] = "Últimos 5 incidencias";
$_data['c_table_column_1'] = "Título";
$_data['c_table_column_2'] = "Fecha de emisión";
$_data['c_table_column_3'] = "Acción";
$_data['c_table_column_4'] = "Mes";
$_data['c_table_column_5'] = "Año";
$_data['c_table_column_6'] = "Descripción";
$_data['c_modal_top_header'] = "Detalles de la incidencia";
$_data['c_notice_board'] = "Tablón de anuncios";
$_data['c_apartment_rules'] = "Reglas y Regulación del Apartamento";
$_data['help_desk_1'] = "Número de contacto del moderador:";
$_data['help_desk_2'] = "Secrataty Contact No:";
$_data['help_desk_3'] = "Número de contacto del guardia de seguridad:";
// texto de la tabla de visitantes
$_data['v_table_header'] = "Últimos 5 visitantes";
$_data['v_table_column_1'] = "Fecha de emisión";
$_data['v_table_column_2'] = "Nombre del visitante";
$_data['v_table_column_3'] = "Móvil";
$_data['v_table_column_4'] = "Dirección";
$_data['v_table_column_5'] = "Número de piso";
$_data['v_table_column_6'] = "Número de unidad";
$_data['v_table_column_7'] = "En el tiempo";
$_data['v_table_column_8'] = "Tiempo fuera";
$_data['v_modal_top_header'] = "Detalles del visitante";
?>