<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1_1'] = "Lista de Invitaciones";
$_data['text_2'] = "Invitación";
$_data['text_3'] = "Agregar Invitación";
$_data['text_4'] = "Formulario de inscripción a la Invitación";
$_data['text_5'] = "Fecha de emisión de la Invitación";
$_data['text_6'] = "Título de la Invitación";
$_data['text_7'] = "Descripción de la Invitación";
$_data['text_15'] = "Se agregó la información de la Invitación correctamente";
$_data['text_16'] = "Actualizar Invitación";
$_data['view_text'] = "Detalles de la Invitación";
$_data['text_17'] = "La información de la Invitación se actualizó correctamente";
$_data['text_18'] = "La información de la Invitación se eliminó correctamente";
$_data['delete_confirm'] = "¿Está seguro de que desea eliminar esta información de la Invitación?";

$_data['text_21'] = "Tipo de Evento";
$_data['text_22'] = "Nombre del Visitante";
$_data['text_23'] = "Telefonos del Visitante";
$_data['text_24'] = "Invitados";
$_data['text_25'] = "Fecha de Invitación";
$_data['text_26'] = "Estatus";
$_data['text_27'] = "Acciones Disponibles";


?>