<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_employee'] = "Agregar nuevo vigilante";
$_data['update_employee'] = "Actualizar vigilante";
$_data['add_new_employee_information_breadcam'] = "Vigilante";
$_data['add_new_employee_breadcam'] = "Agregar vigilante";
$_data['add_new_employee_entry_form'] = "Formulario de inscripción del vigilante";
$_data['add_new_form_field_text_1'] = "Nombre";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto (con código de país)";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID (ID nacional del vigilante)";
$_data['add_new_form_field_text_8'] = "Designación";
$_data['add_new_form_field_text_9'] = "Fecha de incorporación";
$_data['add_new_form_field_text_10'] = "Fecha de finalización";
$_data['add_new_form_field_text_100'] = "Salario por mes";
$_data['add_new_form_field_text_11'] = "Estado";
$_data['add_new_form_field_text_12'] = "Activo";
$_data['add_new_form_field_text_13'] = "Salir";
$_data['add_new_form_field_text_14'] = "Foto del vigilante";
$_data['add_new_form_field_text_15'] = "Seleccionar";
$_data['added_employee_successfully'] = "Se agregó la información del vigilante correctamente";
$_data['update_employee_successfully'] = "La información actualizada del vigilante se realizó correctamente";
$_data['delete_employee_information'] = "Información del vigilante eliminada exitosamente";
$_data['email_exist'] = "La dirección de correo electrónico del vigilante ya existe";
//validación
$_data['v1'] = "¡Se requiere nombre del vigilante!";
$_data['v2'] = "Correo electrónico requerido !!!";
$_data['v3'] = "¡Se requiere contraseña!";
$_data['v4'] = "¡Se requiere número de contacto!";
$_data['v5'] = "¡Se requiere la dirección actual !!!";
$_data['v6'] = "¡Se requiere dirección permanente !!!";
$_data['v7'] = "NID requerido !!!";
$_data['v8'] = "¡Se requiere designación!";
$_data['v9'] = "¡Se requiere la fecha de incorporación !!!";
?>