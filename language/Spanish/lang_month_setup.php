<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración del mes";
$_data['text_2'] = "Formulario de ingreso mensual";
$_data['text_3'] = "Nombre del mes";
$_data['text_4'] = "Lista de meses";
$_data['text_5'] = "Se agregó la información del mes con éxito";
$_data['text_6'] = "La información del mes se actualizó correctamente";
$_data['text_7'] = "Información del mes eliminada correctamente";
$_data['text_8'] = "Detalles del mes";
//validación
$_data['required_1'] = "¡Texto del mes requerido!";
$_data['confirm_delete'] = "¿Está seguro de que desea eliminar este mes?";
?>