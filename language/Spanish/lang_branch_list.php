<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Agregar edificio";
$_data['text_1_1'] = "Actualizar edificio";
$_data['text_2'] = "Edificio";
$_data['text_3'] = "Formulario de entrada del edificio";
$_data['text_4'] = "Nombre";
$_data['text_5'] = "Correo electrónico";
$_data['text_6'] = "No de contacto";
$_data['text_7'] = "Dirección";
$_data['text_8'] = "Estado";
$_data['text_9'] = "Activar";
$_data['text_10'] = "Desactivar";
$_data['text_11'] = "Se agregó la información del edificio correctamente";
$_data['text_12'] = "La información de construcción actualizada se realizó correctamente";
$_data['text_13'] = "Información de construcción eliminada con éxito";
$_data['text_14'] = "Lista de construcción";
$_data['text_15'] = "Nombre del edificio";
$_data['text_16'] = "Detalles del edificio";
$_data['text_17'] = "Tablero";
$_data['text_19'] = "Inicio";
$_data['text_20'] = "Configuración";
$_data['text_144'] = "Security Guard Mobile No";
$_data['text_155'] = "Secretario móvil no";
$_data['text_166'] = "Moderador Móvil No";
$_data['text_177'] = "Año de construcción del edificio";
$_data['text_188'] = "Imagen del edificio";
$_data['text_199'] = "Información del constructor";
$_data['text_200'] = "Nombre de la empresa";
$_data['text_211'] = "Número de teléfono de la compañía";
$_data['text_222'] = "Dirección de la empresa";
$_data['text_233'] = "Estado";
?>