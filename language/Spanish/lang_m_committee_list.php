<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_m_committee'] = "Lista de miembros de administración";
$_data['m_committee_details'] = "Detalles del miembro del comité de administración";
$_data['m_committee'] = "Comité de gestión";
$_data['update_m_committee'] = "Actualizar Comité de Administración";
$_data['add_new_m_committee_information_breadcam'] = "Información del comité de administración";
$_data['add_new_m_committee_breadcam'] = "Agregar comité de administración";
$_data['add_new_m_committee_entry_form'] = "Formulario de inscripción al Comité de Administración";
$_data['add_new_form_field_text_1'] = "Nombre de miembro";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID (ID nacional)";
$_data['add_new_form_field_text_8'] = "Tipo de miembro";
$_data['add_new_form_field_text_9'] = "Fecha de incorporación";
$_data['add_new_form_field_text_10'] = "Fecha de finalización";
$_data['add_new_form_field_text_11'] = "Estado";
$_data['add_new_form_field_text_12'] = "Activo";
$_data['add_new_form_field_text_13'] = "Inactivo";
$_data['add_new_form_field_text_14'] = "Priview";
$_data['add_new_form_field_text_15'] = "Seleccionar";
$_data['added_m_committee_successfully'] = "Se agregó información del comité de administración Exitosamente";
$_data['update_m_committee_successfully'] = "Información actualizada del Comité de Administración Exitosamente";
$_data['delete_m_committee_information'] = "Información eliminada del comité de administración Exitosamente.";
$_data['delete_confirm'] = "¿Está seguro de que desea eliminar este miembro de administración? ";
?>