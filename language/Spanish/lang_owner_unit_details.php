<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista de unidades";
$_data['text_2'] = "Floor No";
$_data['text_3'] = "Número de unidad";
$_data['text_4'] = "Detalles de la unidad";
$_data['text_5'] = "Nombre del propietario";
$_data['text_6'] = "Correo electrónico";
$_data['text_7'] = "No de contacto";
$_data['text_8'] = "Dirección";
$_data['text_9'] = "Tablero del propietario";
?>