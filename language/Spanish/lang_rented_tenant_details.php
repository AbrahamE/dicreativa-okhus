<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Declaración de renta";
$_data['text_2'] = "Mes de la factura";
$_data['text_3'] = "Cantidad total";
$_data['text_4'] = "Fecha de emisión";
$_data['text_5'] = "Nombre alquilado";
$_data['text_6'] = "Correo electrónico";
$_data['text_7'] = "Contacto";
$_data['text_8'] = "Dirección";
$_data['text_9'] = "Floor No";
$_data['text_10'] = "Número de unidad";
$_data['text_11'] = "Alquiler";
$_data['text_12'] = "Factura de agua";
$_data['text_13'] = "Factura eléctrica";
$_data['text_14'] = "Factura de gas";
$_data['text_15'] = "Factura de seguridad";
$_data['text_16'] = "Factura de servicios públicos";
$_data['text_17'] = "Otra factura";
$_data['text_18'] = "Renta Tatal";
$_data['text_19'] = "Panel de control de Tanant";
$_data['text_20'] = "Estado del pago";
$_data['text_21'] = "PAGADO";
$_data['text_22'] = "PENDIENTE";
$_data['text_23'] = "# Factura";
$_data['text_24'] = "Fecha de pago";
?>