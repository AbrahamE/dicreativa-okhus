<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Agregar / actualizar información del edificio";
$_data['text_2'] = "Edificio";
$_data['text_3'] = "Información del edificio";
$_data['text_4'] = "Información del edificio";
$_data['text_5'] = "Nombre";
$_data['text_6'] = "Dirección";
$_data['text_7'] = "Security Guard Mobile";
$_data['text_8'] = "Secretario móvil";
$_data['text_9'] = "Moderador móvil";
$_data['text_10'] = "Año de construcción del edificio";
$_data['text_11'] = "Información del constructor";
$_data['text_12'] = "Teléfono";
$_data['text_13'] = "Imagen del edificio";
$_data['text_14'] = "Seleccionar año";
$_data['text_15'] = "Reglas de construcción";
$_data['text_16'] = "Reglas del apartamento";
$_data['success_text'] = "Éxito";
$_data['update_text'] = "Información de construcción actualizada con éxito";
$_data['r1'] = "¡¡Se requiere el nombre del edificio !!";
$_data['r2'] = "¡Se requiere dirección del edificio !!!";
$_data['r3'] = "Building Security Guard Mobile ¡No se requiere!";
$_data['r4'] = "¡No se requiere la creación de dispositivos móviles móviles de Secratary!";
$_data['r5'] = "¡No se requiere Mobile Moderator Mobile!";
$_data['r6'] = "Año de construcción del edificio requerido !!!";
$_data['r7'] = "¡Se requiere el nombre del constructor!";
$_data['r8'] = "¡Se requiere la dirección del constructor!";
$_data['r9'] = "¡No se requiere el dispositivo móvil del creador!";
?>