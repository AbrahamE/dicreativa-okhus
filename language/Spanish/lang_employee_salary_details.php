<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Detalles del salario";
$_data['text_2'] = "Fecha de emisión";
$_data['text_3'] = "Nombre del mes";
$_data['text_4'] = "Año";
$_data['text_5'] = "Cantidad";
$_data['text_6'] = "Panel del vigilante";
$_data['text_7'] = "Detalles del vigilante";
$_data['text_8'] = "Nombre del vigilante";
$_data['text_9'] = "Correo electrónico";
$_data['text_10'] = "Contacto";
$_data['text_11'] = "Dirección actual";
$_data['text_12'] = "Dirección permanente";
?>