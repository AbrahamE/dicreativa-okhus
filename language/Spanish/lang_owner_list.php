<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['owner_list'] = "Lista de propietarios";
$_data['add_new_form_field_text_1'] = "Nombre del propietario";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID (ID nacional)";
$_data['add_new_form_field_text_8'] = "Unidad del propietario";
$_data['add_new_form_field_text_9'] = "Imagen";
$_data['add_new_owner_information_breadcam'] = "Información del propietario";
$_data['add_new_owner_breadcam'] = "Agregar propietario";
$_data['added_owner_successfully'] = "Se agregó la información del propietario correctamente";
$_data['update_owner_successfully'] = "La información del propietario se actualizó correctamente";
$_data['delete_owner_information'] = "Información del propietario eliminada correctamente";
$_data['owner_details'] = "Detalles del propietario";
$_data['confirm'] = "¿Está seguro de que desea eliminar este propietario?";
?>