<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_m_committee'] = "Agregar nuevo comité de administración";
$_data['m_committee'] = "Comité de gestión";
$_data['update_m_committee'] = "Actualizar Comité de Administración";
$_data['add_new_m_committee_information_breadcam'] = "Información del comité de administración";
$_data['add_new_m_committee_breadcam'] = "Agregar comité de administración";
$_data['add_new_m_committee_entry_form'] = "Formulario de inscripción al Comité de Administración";
$_data['add_new_form_field_text_1'] = "Nombre de miembro";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Teléfono";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID";
$_data['add_new_form_field_text_8'] = "Designación";
$_data['add_new_form_field_text_9'] = "Fecha de incorporación";
$_data['add_new_form_field_text_10'] = "Fecha de finalización";
$_data['add_new_form_field_text_11'] = "Estado";
$_data['add_new_form_field_text_12'] = "Activo";
$_data['add_new_form_field_text_13'] = "Caducado";
$_data['add_new_form_field_text_14'] = "Vista previa";
$_data['add_new_form_field_text_15'] = "Seleccionar";
$_data['added_m_committee_successfully'] = "Comité de administración agregado con éxito";
$_data['update_m_committee_successfully'] = "Comité de administración actualizado con éxito";
$_data['delete_m_committee_information'] = "Comité de administración eliminado con éxito";
//
$_data['email_exist'] = "El correo electrónico Memeber ya existe";
//validación
$_data['required_1'] = "¡Se requiere nombre de miembro!";
$_data['required_2'] = "¡Se requiere correo electrónico del miembro!";
$_data['required_3'] = "¡Se requiere contraseña de miembro!";
$_data['required_4'] = "¡Se requiere número de contacto de miembro!";
$_data['required_5'] = "¡Se requiere la dirección actual del miembro !!!";
$_data['required_6'] = "¡NID de miembro requerido!";
$_data['required_7'] = "Seleccione el tipo de miembro !!!";
$_data['required_8'] = "¡Se requiere la fecha de ingreso del miembro!";
?>