<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['unit_list_title'] = "Lista de unidades";
$_data['add_new_unit_information_breadcam'] = "Información de la unidad";
$_data['delete_unit_information'] = "Información de la unidad eliminada con éxito";
$_data['floor_no'] = "Floor No";
$_data['unit_no'] = "Unidad No";
$_data['unit_details'] = "Detalles de la unidad";
$_data['add_unit'] = "Agregar unidad";
$_data['add_unit_successfully'] = "Se agregó la información de la unidad con éxito";
$_data['update_unit_successfully'] = "Información de la unidad actualizada con éxito";
$_data['details_info'] = "Información de detalles";
$_data['confirm'] = "¿Está seguro de que desea eliminar esta Unidad?";
?>