<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['floor_list_title'] = "Lista de piso";
$_data['add_new_floor_information_breadcam'] = "Información del piso";
$_data['delete_floor_information'] = "La información del piso eliminada se realizó correctamente";
$_data['floor_no'] = "Floor No";
$_data['floor_details'] = "Detalles del piso";
$_data['add_floor'] = "Agregar piso";
$_data['delete_msg'] = "¿Está seguro de que desea eliminar este piso?";
$_data['add_msg'] = "Se agregó la información del piso correctamente";
$_data['update_msg'] = "Actualizar la información del piso con éxito";
$_data['confirm'] = "¿Está seguro de que desea eliminar este piso?";
?>