<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['save_button_text'] = "Guardar información";
$_data['update_button_text'] = "Actualizar información";
$_data['home_breadcam'] = "Inicio";
$_data['delete_text'] = "Eliminar";
$_data['action_text'] = "Acción";
$_data['details_information'] = "Información de detalles";
$_data['view_text'] = "Ver detalles";
$_data['edit_text'] = "Editar";
$_data['back_text'] = "Atrás";
$_data['success'] = "Success";
$_data['upload_image'] = "Subir imagen";
$_data['image'] = "Imagen";
$_data['submit'] = "Enviar";
$_data['reset'] = "Restablecer";
$_data['setting'] = "Configuración";
$_data['reset_text'] = "Restablezca primero antes de insertar";
$_data['owner_dashboard'] = "Tablero del propietario";
$_data['dashboard'] = "Tablero";
$_data['print'] = "Imprimir información";
$_data['valid_email'] = "Proporcione una dirección de correo electrónico válida";
//tipo de usuario
$_data['user_admin'] = "Admin";
$_data['user_owner'] = "Propietario";
$_data['user_employee'] = "Vigilante";
$_data['user_tenant'] = "Inquilino";
$_data['user_super_admin'] = "Super Admin";
$_data['lang_online'] = "En línea";
$_data['lang_profile'] = "Perfil";
$_data['lang_signout'] = "Cerrar sesión";
//Base de datos
$_data['database_left_menu'] = "Base de datos";
$_data['database_clear_dummy_data'] = "Borrar datos";
$_data['database_clear_dummy_msg'] = "Puede eliminar todos los datos ficticios desde aquí, por lo que eliminaremos
toda la información, excepto los datos de configuración, por lo que debe cambiar eso o eliminarlo usted mismo ";
$_data['database_clear_dummy_data_start'] = "Haga clic para comenzar";
$_data['database_clear_dummy_data_confirm'] = "¿Está seguro de que desea eliminar los datos ficticios?";
$_data['database_clear_msg'] = "Borrar la información con éxito";
//perfil
$_data['user_lang_switch'] = "Cambiar idioma";
$_data['user_profile'] = "Perfil";
$_data['user_logout'] = "Cerrar sesión";
$_data['user_lang_switcher'] = "INTERRUPTOR DE IDIOMA";
$_data['user_branch_switcher'] = "Cambiar edificio";
$_data['available_rooms'] = "Lista de habitaciones disponibles";
$_data['available_rooms_floor'] = "Nombre del piso";
$_data['available_rooms_unit'] = "Nombre de la unidad";
$_data['app_version'] = "Versión";
$_data['app_copyright'] = "Copyright";
//actualización del perfil
$_data['profile_update_title'] = "Actualice su perfil";
$_data['profile_update_name'] = "Nombre";
$_data['profile_update_email'] = "Correo electrónico";
$_data['profile_update_contact'] = "No de contacto (con código de país)";
$_data['profile_update_password'] = "Contraseña";
$_data['profile_update_button'] = "Actualizar";
$_data['profile_update_information'] = "Después de la actualización, la aplicación cerrará sesión automáticamente";
?>