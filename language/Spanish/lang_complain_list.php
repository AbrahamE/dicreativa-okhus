<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista de incidencias";
$_data['text_1_1_1'] = "Detalles de la incidencia";
$_data['text_2'] = "Incidencias";
$_data['text_3'] = "Agregar incidencia";
$_data['text_4'] = "Formulario de inscripción de incidencia";
$_data['text_5'] = "Título";
$_data['text_6'] = "Descripción";
$_data['text_7'] = "Fecha";
$_data['text_8'] = "Se agregó la incidencia con éxito";
$_data['text_9'] = "Actualizada incidencia exitosamente";
$_data['text_10'] = "Eliminado incidencia exitosamente";
$_data['text_11'] = "Mes";
$_data['text_12'] = "Año";
$_data['confirm'] = "¿Está seguro de que desea eliminar esta incidencia?";
// inquilino se incidencia lang
$_data['t_text_1'] = "El inquilino se incidencia";
$_data['t_text_2'] = "Estado";
$_data['t_status_pending'] = "Pendiente";
$_data['t_status_in_progress'] = "En progreso";
$_data['t_status_on_hold'] = "En espera";
$_data['t_status_completed'] = "Completado";
$_data['execute_job'] = "Incidencia asignado";
$_data['select_employee'] = "Seleccionar vigilante";
$_data['execute_job_btn'] = "Asignar trabajo";
$_data['execute_job_msg'] = "Trabajo asignado con éxito";
$_data['add_emp_solution'] = "Agregar solución";
$_data['add_solution'] = "Agregue su solución";
$_data['solution_label'] = "Solución";
$_data['solution_label_required'] = "Se requiere descripción de la solución";
$_data['solution_success_msg'] = "Guardar la información de la solución con éxito";
$_data['complain_person_details'] = "Reclamar detalles de la persona";
$_data['complain_person_by'] = "Incidencia por";
$_data['complain_person_name'] = "Nombre";
$_data['complain_person_email'] = "Correo electrónico";
$_data['complain_person_phone'] = "Teléfono";
?>