<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['renter_list'] = "Lista de inquilinos";
$_data['update_rent'] = "Actualizar inquilino";
$_data['add_new_form_field_text_0'] = "Imagen";
$_data['add_new_form_field_text_1'] = "Nombre del inquilino";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Dirección";
$_data['add_new_form_field_text_6'] = "NID (ID nacional)";
$_data['add_new_form_field_text_7'] = "Número de piso";
$_data['add_new_form_field_text_8'] = "Unidad alquilada no";
$_data['add_new_form_field_text_9'] = "Pago por adelantado";
$_data['add_new_form_field_text_10'] = "Pago por mes";
$_data['add_new_form_field_text_11'] = "Fecha de emisión";
$_data['add_new_form_field_text_12'] = "Mes de alquiler";
$_data['add_new_form_field_text_13'] = "Año de alquiler";
$_data['add_new_form_field_text_14'] = "Estado";
$_data['add_new_form_field_text_15'] = "Vista previa";
$_data['add_new_form_field_text_16'] = "Activo";
$_data['add_new_form_field_text_17'] = "In-Active";
$_data['add_new_renter_information_breadcam'] = "Información del inquilino";
$_data['add_new_rent_breadcam'] = "Agregar inquilino";
$_data['rented_details'] = "Detalles alquilados";
$_data['added_renter_successfully'] = "Se agregó la información del cliente correctamente";
$_data['update_renter_successfully'] = "La información actualizada del arrendatario se realizó correctamente";
$_data['delete_renter_information'] = "La información del inquilino eliminada se realizó correctamente";
$_data['confirm'] = "¿Está seguro de que desea eliminar este inquilino? información? ";
?>