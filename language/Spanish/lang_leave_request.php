<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Dejar la lista de solicitudes";
$_data['text_1_1_1'] = "Dejar detalles";
$_data['text_2'] = "Salir";
$_data['text_2_2'] = "Información del vigilante";
$_data['text_3'] = "Agregar solicitud de licencia";
$_data['text_4'] = "Formulario de entrada de solicitud de licencia";
$_data['required_date'] = "Fecha solicitada";
$_data['emp_name'] = "Nombre";
$_data['emp_designation'] = "Designación";
$_data['leave_from'] = "Salir de";
$_data['leave_to'] = "Dejar para";
$_data['leave_days'] = "Días de licencia";
$_data['leave_status'] = "Dejar estado";
$_data['leave_description'] = "Dejar detalles";
$_data['text_8'] = "La solicitud de licencia creada esperó con éxito la respuesta del administrador";
$_data['text_9'] = "Solicitud de licencia actualizada con éxito";
$_data['text_10'] = "Solicitud de licencia eliminada exitosamente";
$_data['text_100'] = "¿Está seguro de que desea eliminar esta solicitud de licencia?";
$_data['text_11'] = "Mes";
$_data['text_12'] = "Año";
$_data['t_text_1'] = "Solicitud de licencia del vigilante";
?>