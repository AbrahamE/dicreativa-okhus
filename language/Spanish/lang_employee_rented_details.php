<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Detalles del inquilino";
$_data['text_2'] = "Nombre del inquilino";
$_data['text_3'] = "Contacto";
$_data['text_4'] = "Número de unidad";
$_data['text_5'] = "Alquiler anticipado";
$_data['text_6'] = "Alquiler por mes";
$_data['text_7'] = "Estado";
$_data['text_8'] = "Correo electrónico";
$_data['text_9'] = "Contraseña";
$_data['text_10'] = "Dirección";
$_data['text_11'] = "NID (ID nacional)";
$_data['text_12'] = "Fecha de inicio del alquiler";
$_data['dashboard'] = "Tablero";
$_data['active'] = "Activo";
$_data['expired'] = "Caducado";
?>