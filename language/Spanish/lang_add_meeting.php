<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Agregar información de la Invitación";
$_data['text_2'] = "Invitación";
$_data['text_3'] = "Agregar Invitación";
$_data['text_4'] = "Formulario de inscripción a la Invitación";
$_data['text_5'] = "Fecha de emisión de la Invitación";
$_data['text_6'] = "Título de la Invitación";
$_data['text_66'] = "Descripción de la Invitación";
$_data['text_15'] = "Se agregó la información de la Invitación correctamente";
$_data['text_16'] = "Actualizar Invitación";
$_data['text_17'] = "La información de la Invitación se actualizó correctamente";
//validación
$_data['v1'] = "Fecha de emisión de la Invitación requerida !!!";
$_data['v2'] = "Se requiere título de la Invitación !!!";
$_data['v3'] = "¡Se requiere descripción de la Invitación!";
?>