<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Agregar nueva factura";
$_data['text_1_1'] = "Actualizar factura";
$_data['text_2'] = "Información de la factura";
$_data['text_3'] = "Agregar factura";
$_data['text_4'] = "Formulario de ingreso de facturas";
$_data['text_5'] = "Tipo de factura";
$_data['text_6'] = "Seleccionar tipo";
$_data['text_7'] = "Fecha de depósito de factura";
$_data['text_8'] = "Mes de la factura";
$_data['text_9'] = "Seleccionar mes";
$_data['text_10'] = "Año de factura";
$_data['text_11'] = "Seleccionar año";
$_data['text_12'] = "Cantidad total";
$_data['text_13'] = "Nombre del banco de depósito";
$_data['text_14'] = "Detalles";
$_data['text_15'] = "Factura agregada exitosamente";
$_data['text_16'] = "Factura actualizada exitosamente";
$_data['text_17'] = "";
//validación
$_data['required_1'] = "¡Tipo de factura requerido!";
$_data['required_2'] = "¡Se requiere la fecha de depósito de la factura!";
$_data['required_3'] = "¡Se requiere el mes de la factura!";
$_data['required_4'] = "¡Se requiere el año de facturación!";
$_data['required_5'] = "Factura total requerida !!!";
$_data['required_6'] = "¡Se requiere nombre del banco!";
$_data['required_7'] = "Detalles de la factura requeridos !!!";
?>