<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración del sistema";
$_data['text_222'] = "Moneda";
$_data['text_2'] = "Idioma";
$_data['text_22'] = "Correo electrónico";
$_data['text_x33'] = "SMS";
$_data['text_3'] = "Seleccionar idioma";
$_data['text_4'] = "Configuración actualizada correctamente";
$_data['text_5'] = "Seleccionar moneda";
$_data['text_6'] = "Doller";
$_data['text_7'] = "Taka";
$_data['text_8'] = "--Seleccionar--";
$_data['text_9'] = "Punto (.)";
$_data['text_10'] = "Coma (,)";
$_data['text_11'] = "Izquierda";
$_data['text_12'] = "Derecha";
$_data['text_14'] = "Posición de moneda";
$_data['text_144'] = "Moneda decimal";
$_data['text_15'] = "Separador de moneda";
$_data['text_16'] = "Imagen Super Admin";
$_data['text_17'] = "Subir archivo zip de idioma";
$_data['text_18'] = "Subir archivo zip";
$_data['text_19'] = "Protocolo de correo";
$_data['text_20'] = "Nombre de host Smtp";
$_data['text_21'] = "Nombre de usuario SMTP";
$_data['text_22x'] = "Contraseña SMTP";
$_data['text_23'] = "Publicación SMTP";
$_data['text_24'] = "SMTP Secure";
$_data['text_25'] = "ClickATell Username";
$_data['text_26'] = "Contraseña de ClickATell";
$_data['text_27'] = "ClickATell API Key";
$_data['text_lang_1'] = "Cómo crear y cargar un nuevo archivo de idioma:";
$_data['text_lang_2'] = "muestra el archivo zip del archivo de idioma";
$_data['text_lang_22'] = "Descargar";
$_data['text_lang_3'] = "Descomprima el archivo zip y luego cambie todo el texto del archivo manualmente";
$_data['text_lang_4'] = "Agregar imagen de flag.png de idioma de tamaño 24x24px.";
$_data['text_lang_5'] = "Cambie el nombre de la carpeta por el nombre de su idioma, luego haga que sea zip y cárguelo a través de nuestro
cargador de idiomas ";
?>