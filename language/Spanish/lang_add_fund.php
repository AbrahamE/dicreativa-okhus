<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_fund'] = "Agregar nuevo fondo";
$_data['fund'] = "Fondo";
$_data['update_fund'] = "Actualizar fondo";
$_data['add_new_fund_breadcam'] = "Agregar fondo";
$_data['add_new_fund_entry_form'] = "Formulario de ingreso de fondos";
$_data['add_new_form_field_text_1'] = "Nombre del propietario";
$_data['add_new_form_field_text_2'] = "Seleccionar propietario";
$_data['add_new_form_field_text_3'] = "Seleccionar mes";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Seleccionar año";
$_data['add_new_form_field_text_6'] = "Fecha de emisión";
$_data['add_new_form_field_text_7'] = "Cantidad total";
$_data['add_new_form_field_text_8'] = "Propósito";
$_data['added_fund_successfully'] = "Se agregó la información del fondo con éxito";
$_data['update_fund_successfully'] = "La información actualizada del fondo se realizó correctamente";
$_data['delete_fund_information'] = "Información del fondo eliminada con éxito";
$_data['save_and_print_invoice'] = "Guardar e imprimir factura";
//validación
$_data['required_1'] = "Seleccionar nombre del propietario";
$_data['required_2'] = "Seleccionar mes";
$_data['required_3'] = "Seleccionar año";
$_data['required_4'] = "Fecha de emisión requerida !!!";
$_data['required_5'] = "El monto del fondo no puede estar vacío";
$_data['required_6'] = "Propósito del fondo requerido !!!";
?>