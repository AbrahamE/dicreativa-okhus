<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['rent_list'] = "Lista de alquileres";
$_data['fare_details'] = "Detalles del alquiler";
$_data['add_new_rent_information_breadcam'] = "Renta";
$_data['add_new_rent_breadcam'] = "Agregar alquiler";
$_data['add_new_form_field_text_1'] = "Número de piso";
$_data['add_new_form_field_text_2'] = "Seleccionar piso";
$_data['add_new_form_field_text_3'] = "Número de unidad";
$_data['add_new_form_field_text_4'] = "Seleccionar unidad";
$_data['add_new_form_field_text_5'] = "Seleccionar mes";
$_data['add_new_form_field_text_6'] = "Nombre del arrendatario";
$_data['add_new_form_field_text_7'] = "Alquiler";
$_data['add_new_form_field_text_77'] = "Estado de la factura";
$_data['add_new_form_field_text_8'] = "Factura de agua";
$_data['add_new_form_field_text_9'] = "Factura eléctrica";
$_data['add_new_form_field_text_10'] = "Factura de gas";
$_data['add_new_form_field_text_11'] = "Factura de seguridad";
$_data['add_new_form_field_text_12'] = "Factura de servicios públicos";
$_data['add_new_form_field_text_13'] = "Otra factura";
$_data['add_new_form_field_text_14'] = "Renta";
$_data['add_new_form_field_text_15'] = "Fecha de emisión";
$_data['add_new_form_field_text_16'] = "Tipo";
$_data['add_new_form_field_text_17'] = "Mes de alquiler";
$_data['add_new_form_field_text_18'] = "Año de alquiler";
$_data['added_rent_successfully'] = "Se agregó la información de alquiler con éxito";
$_data['update_rent_successfully'] = "La información de alquiler actualizada se realizó correctamente";
$_data['delete_rent_information'] = "Información de alquiler eliminada con éxito";
$_data['confirm'] = "¿Está seguro de que desea eliminar esta información de alquiler del inquilino?";
$_data['invoice_no'] = "# Factura";
$_data['status_field_text_18'] = "DUE";
$_data['status_field_text_19'] = "PAGADO";
$_data['add_new_form_field_text_19'] = "Fecha de pago";
?>