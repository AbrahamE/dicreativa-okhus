<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista del Comité de Administración";
$_data['text_2'] = "Nombre de miembro";
$_data['text_3'] = "Designación";
$_data['text_4'] = "Correo electrónico";
$_data['text_5'] = "Contacto";
$_data['text_6'] = "Dirección actual";
$_data['text_7'] = "Detalles del miembro";
$_data['text_8'] = "Dirección permanente";
$_data['text_9'] = "NID";
?>