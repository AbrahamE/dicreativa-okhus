<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_owner'] = "Agregar nuevo propietario";
$_data['update_owner'] = "Actualizar propietario";
$_data['add_new_owner_entry_form'] = "Formulario de inscripción del propietario";
$_data['add_new_form_field_text_1'] = "Nombre del propietario";
$_data['add_new_form_field_text_2'] = "Correo electrónico";
$_data['add_new_form_field_text_3'] = "Contraseña";
$_data['add_new_form_field_text_4'] = "Número de contacto (con código de país)";
$_data['add_new_form_field_text_5'] = "Dirección actual";
$_data['add_new_form_field_text_6'] = "Dirección permanente";
$_data['add_new_form_field_text_7'] = "NID (tarjeta de identificación nacional)";
$_data['add_new_form_field_text_8'] = "Unidad del propietario";
$_data['add_new_form_field_text_9'] = "Foto del propietario";
$_data['add_new_owner_information_breadcam'] = "Información del propietario";
$_data['add_new_owner_breadcam'] = "Agregar propietario";
$_data['added_owner_successfully'] = "Se agregó la información del propietario correctamente";
$_data['update_owner_successfully'] = "La información del propietario se actualizó correctamente";
$_data['delete_owner_information'] = "Información del propietario eliminada correctamente";
$_data['email_exist'] = "El correo electrónico del propietario ya existe";
//validación
$_data['owner_name_required'] = "Se requiere nombre del propietario";
$_data['owner_email_required'] = "Se requiere la dirección de correo electrónico del propietario";
$_data['owner_password'] = "Se requiere contraseña de propietario";
$_data['owner_contact'] = "No se requiere contacto con el propietario";
$_data['owner_present_address'] = "Se requiere la dirección actual del propietario";
$_data['owner_permanent_address'] = "Se requiere dirección permanente del propietario";
$_data['owner_nid_no'] = "Se requiere NID del propietario";
?>