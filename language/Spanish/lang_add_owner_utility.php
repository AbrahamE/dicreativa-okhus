<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_owner_utility_title'] = "Agregar nueva utilidad de propietario";
$_data['update_owner_utility'] = "Actualizar la utilidad del propietario";
$_data['owner_utility'] = "Utilidad del propietario";
$_data['add_owner_utility'] = "Agregar utilidad de propietario";
$_data['add_owner_utility_entry_form'] = "Formulario de entrada de utilidad del propietario";
$_data['add_new_form_field_text_1'] = "Número de piso";
$_data['add_new_form_field_text_2'] = "Seleccionar piso";
$_data['add_new_form_field_text_3'] = "Número de unidad";
$_data['add_new_form_field_text_4'] = "Seleccionar unidad";
$_data['add_new_form_field_text_5'] = "Seleccionar mes de utilidad";
$_data['add_new_form_field_text_55'] = "Seleccionar año de utilidad";
$_data['add_new_form_field_text_6'] = "Nombre del propietario";
$_data['add_new_form_field_text_8'] = "Factura de agua";
$_data['add_new_form_field_text_9'] = "Factura eléctrica";
$_data['add_new_form_field_text_10'] = "Factura de gas";
$_data['add_new_form_field_text_11'] = "Factura de seguridad";
$_data['add_new_form_field_text_12'] = "Factura de servicios públicos";
$_data['add_new_form_field_text_13'] = "Otra factura";
$_data['add_new_form_field_text_14'] = "Renta";
$_data['add_new_form_field_text_15'] = "Fecha de emisión";
$_data['added_owner_utility_successfully'] = "Se agregó la información de utilidad del propietario correctamente";
$_data['update_owner_utility_successfully'] = "La información actualizada de la utilidad del propietario se realizó correctamente";
$_data['delete_owner_utility_information'] = "La información de la utilidad del propietario se eliminó correctamente";
//validación
$_data['v1'] = "¡Seleccione el piso del propietario!";
$_data['v2'] = "¡Seleccione la unidad del propietario!";
$_data['v3'] = "¡Seleccione el mes de la utilidad!";
$_data['v33'] = "¡Seleccione el año de utilidad !!!";
$_data['v4'] = "¡Se requiere la factura de agua del propietario!";
$_data['v5'] = "¡Se requiere la factura de electricidad del propietario!";
$_data['v6'] = "¡Se requiere la factura de gas del propietario!";
$_data['v7'] = "¡Se requiere la factura de seguridad del propietario!";
$_data['v8'] = "¡Se requiere la factura de servicios públicos del propietario!";
$_data['v9'] = "Fecha de emisión de la utilidad requerida";
?>