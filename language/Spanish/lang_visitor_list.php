<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1_1'] = "Lista de visitantes";
$_data['text_2'] = "Visitante";
$_data['text_3'] = "Agregar visitante";
$_data['text_4'] = "Formulario de entrada de visitante";
$_data['text_5'] = "Fecha de entrada";
$_data['text_6'] = "Nombre";
$_data['text_7'] = "Móvil";
$_data['text_8'] = "Dirección";
$_data['text_9'] = "Floor No";
$_data['text_10'] = "Seleccionar piso";
$_data['text_11'] = "Número de unidad";
$_data['text_12'] = "Seleccionar unidad";
$_data['text_13'] = "A tiempo";
$_data['text_14'] = "Tiempo de espera";
$_data['text_15'] = "Se agregó el visitante correctamente";
$_data['text_16'] = "Actualizar visitante";
$_data['text_17'] = "Visitante actualizado exitosamente";
$_data['text_18'] = "Visitante eliminado exitosamente";
$_data['text_19'] = "¿Está seguro de que desea eliminar este visitante?";
?>