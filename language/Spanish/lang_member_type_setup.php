<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración de tipo de miembro";
$_data['text_2'] = "Formulario de ingreso de tipo de miembro";
$_data['text_3'] = "Tipo de miembro";
$_data['text_4'] = "Lista de tipos de miembros";
$_data['text_5'] = "Se agregó la información del tipo de miembro de administración con éxito";
$_data['text_6'] = "La información actualizada del tipo de miembro de administración se realizó correctamente";
$_data['text_7'] = "Información de tipo de miembro de administración eliminada correctamente";
$_data['text_8'] = "Detalles del tipo de miembro";
//validación
$_data['required_1'] = "¡Se requiere texto de tipo de miembro!";
$_data['confirm_delete'] = "¿Está seguro de que desea eliminar este tipo de miembro?";
?>