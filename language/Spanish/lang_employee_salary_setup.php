<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Configuración de salario del vigilante";
$_data['text_2'] = "Formulario de ingreso de salario del vigilante";
$_data['text_3'] = "Nombre del vigilante";
$_data['text_4'] = "Seleccionar nombre";
$_data['text_5'] = "Designación";
$_data['text_6'] = "Mes de salario";
$_data['text_66'] = "Año salarial";
$_data['text_7'] = "Salario por mes";
$_data['text_8'] = "Fecha de emisión";
$_data['text_9'] = "Se agregó la información de salario del vigilante correctamente";
$_data['text_10'] = "La información actualizada del salario del vigilante se realizó correctamente";
$_data['text_11'] = "La información salarial del vigilante se eliminó correctamente";
$_data['text_12'] = "Detalles del salario del vigilante";
$_data['text_13'] = "Nombre";
$_data['text_14'] = "Correo electrónico";
$_data['text_15'] = "Teléfono";
$_data['delete_alert'] = "¿Está seguro de que desea eliminar la información salarial de los vigilantes?";
//validación
$_data['v1'] = "¡Seleccionar vigilante !!!";
$_data['v2'] = "Seleccione el mes de salario !!!";
$_data['v22'] = "Seleccione el año de salario !!!";
$_data['v3'] = "¡Salario requerido!";
$_data['v4'] = "¡Se requiere la fecha de emisión del salario!";
?>