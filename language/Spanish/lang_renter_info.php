<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe del inquilino";
$_data['text_5'] = "Nombre del inquilino";
$_data['text_6'] = "Correo electrónico";
$_data['text_7'] = "Contacto";
$_data['text_8'] = "Dirección";
$_data['text_9'] = "Floor No";
$_data['text_10'] = "Número de unidad";
$_data['text_11'] = "Pago por adelantado";
$_data['text_12'] = "Alquiler por mes";
$_data['text_13'] = "Fecha";
$_data['text_14'] = "NID";
$_data['text_15'] = "Estado";
$_data['text_16'] = "Imprimir información";
$_data['text_17'] = "Activo";
$_data['text_18'] = "In-Active";
?>