<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Estado del fondo";
$_data['text_2'] = "Imagen";
$_data['text_3'] = "Fecha";
$_data['text_4'] = "Nombre del propietario";
$_data['text_5'] = "Mes";
$_data['text_6'] = "Año";
$_data['text_7'] = "Cantidad";
$_data['text_8'] = "Propósito";
$_data['text_9'] = "Mantenimiento del estado del costo";
$_data['text_10'] = "Título del costo";
$_data['text_11'] = "Detalles";
$_data['text_12'] = "Saldo restante";
$_data['text_13'] = "Imprimir información";
?>