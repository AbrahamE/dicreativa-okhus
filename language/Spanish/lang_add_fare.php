<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_rent'] = "Agregar nueva renta";
$_data['update_rent'] = "Actualizar alquiler";
$_data['add_new_rent_information_breadcam'] = "Renta";
$_data['add_new_rent_breadcam'] = "Agregar alquiler";
$_data['add_new_rent_entry_form'] = "Formulario de inscripción de alquiler";
$_data['add_new_form_field_text_1'] = "Número de piso";
$_data['add_new_form_field_text_2'] = "Seleccionar piso";
$_data['add_new_form_field_text_3'] = "Número de unidad";
$_data['add_new_form_field_text_4'] = "Seleccionar unidad";
$_data['add_new_form_field_text_5'] = "Seleccionar mes de alquiler";
$_data['add_new_form_field_text_55'] = "Seleccionar año de alquiler";
$_data['add_new_form_field_text_6'] = "Nombre del arrendatario";
$_data['add_new_form_field_text_7'] = "Alquiler";
$_data['add_new_form_field_text_8'] = "Factura de agua";
$_data['add_new_form_field_text_9'] = "Factura eléctrica";
$_data['add_new_form_field_text_10'] = "Factura de gas";
$_data['add_new_form_field_text_11'] = "Factura de seguridad";
$_data['add_new_form_field_text_12'] = "Factura de servicios públicos";
$_data['add_new_form_field_text_13'] = "Otra factura";
$_data['add_new_form_field_text_14'] = "Renta";
$_data['add_new_form_field_text_15'] = "Fecha de emisión";
$_data['add_new_form_field_text_16'] = "Fecha de pago de la factura";
$_data['add_new_form_field_text_17'] = "Estado de la factura";
$_data['add_new_form_field_text_18'] = "Debido";
$_data['add_new_form_field_text_19'] = "Pagado";
$_data['added_rent_successfully'] = "Se agregó la información de alquiler con éxito";
$_data['update_rent_successfully'] = "La información de alquiler actualizada se realizó correctamente";
$_data['delete_rent_information'] = "Información de alquiler eliminada con éxito";
//validación
$_data['v1'] = "Seleccione el piso del inquilino !!!";
$_data['v2'] = "Seleccione la unidad de arrendatario !!!";
$_data['v3'] = "¡Seleccione el mes de alquiler!";
$_data['v33'] = "¡Seleccione el año de alquiler!";
$_data['v4'] = "¡Se requiere factura de agua para inquilinos!";
$_data['v5'] = "¡Se requiere factura de electricidad para arrendatario!";
$_data['v6'] = "¡Se requiere factura de gas para inquilinos!";
$_data['v7'] = "¡Se requiere factura de seguridad para inquilinos!";
$_data['v8'] = "¡Requerir factura de servicios públicos requerida!";
$_data['v9'] = "Fecha de emisión del alquiler requerida";
//factura
$_data['invoice_title'] = "Factura de alquiler";
$_data['invoice_number'] = "Factura";
$_data['invoice_issue_date'] = "Fecha de emisión";
$_data['invoice_paid_date'] = "Fecha de pago";
$_data['invoice_bill_date'] = "Mes de la factura";
$_data['invoice_phone'] = "Teléfono";
$_data['invoice_email'] = "Correo electrónico";
$_data['invoice_floor'] = "Piso";
$_data['invoice_unit'] = "Unidad";
$_data['invoice_payment_method'] = "Método de pago";
$_data['invoice_payment_method_name'] = "EFECTIVO";
$_data['invoice_bill_details'] = "Detalles de la factura";
$_data['invoice_bill_amount'] = "Cantidad";
$_data['invoice_home_rent'] = "Alquiler de casa";
$_data['invoice_water_bill'] = "Factura de agua";
$_data['invoice_electric_bill'] = "Factura eléctrica";
$_data['invoice_gas_bill'] = "Factura de gas";
$_data['invoice_guard_bill'] = "Guard Bill";
$_data['invoice_utility_bill'] = "Factura de servicios públicos";
$_data['invoice_other_bill'] = "Otra factura";
$_data['invoice_total_bill'] = "Total";
$_data['invoice_signature'] = "Firma";
$_data['invoice_print_text'] = "Imprimir factura";
$_data['save_and_invoice'] = "Guardar e imprimir factura";
$_data['invoice_title_fund'] = "Factura de fondos";
$_data['invoice_fund_details'] = "Pago mensual de recaudación de fondos";
?>