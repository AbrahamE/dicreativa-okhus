<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_floor_top_title'] = "Agregar piso nuevo";
$_data['update_floor_top_title'] = "Actualizar piso";
$_data['add_new_floor_entry_text'] = "Formulario de entrada al piso";
$_data['add_new_form_field_text_1'] = "Número de piso";
$_data['add_new_floor_information_breadcam'] = "Información del piso";
$_data['add_new_add_floor_breadcam'] = "Agregar piso";
$_data['back_text'] = "Atrás";
//validación
$_data['floor_required'] = "Se requiere piso";
?>