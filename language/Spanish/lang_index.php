<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['application_title'] = "SCAV | Sistema de control de acceso a visitantes";
$_data['application_heading_1'] = "SCAV";
$_data['application_heading_2'] = "Sistema de control de acceso a visitantes";
$_data['wrong_login_msg'] = "Correo electrónico o contraseña incorrectos";
$_data['enter_login_details'] = "Ingresar detalles de inicio de sesión";
$_data['select_type'] = "Seleccionar tipo de usuario";
$_data['user_1'] = "Admin";
$_data['user_2'] = "Propietario";
$_data['user_3'] = "Vigilante";
$_data['user_4'] = "Inquilino";
$_data['user_5'] = "Super Admin";
$_data['select_branch'] = "Seleccionar rama";
$_data['forgot_password'] = "¿Olvidó su contraseña?";
$_data['forgot_your_password'] = "¿Olvidó su contraseña?";
$_data['_login'] = "Iniciar sesión";
$_data['your_email'] = "Su correo electrónico";
$_data['your_password'] = "Su contraseña";
$_data['forget_your_password'] = "Olvidó su contraseña";
$_data['btn_submit'] = "Enviar";
$_data['btn_back_login'] = "Volver al inicio de sesión";
$_data['no_info_found'] = "No se encontró información en nuestra base de datos";
$_data['send_email_text'] = "Verifique su dirección de correo electrónico para ver los detalles de inicio de sesión";
//validar
$_data['v1'] = "Correo electrónico requerido !!!";
$_data['v2'] = "Correo electrónico válido requerido !!!";
$_data['v3'] = "¡Se requiere contraseña!";
$_data['v4'] = "¡Seleccione el tipo de usuario!";
$_data['select_language'] = "Seleccionar idioma";
?>