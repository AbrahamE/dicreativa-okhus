<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['add_new_fund'] = "Lista de fondos";
$_data['fund'] = "Fondo";
$_data['fund_details'] = "Detalles del fondo";
$_data['update_fund'] = "Actualizar fondo";
$_data['add_new_fund_breadcam'] = "Agregar fondo";
$_data['add_new_fund_entry_form'] = "Formulario de ingreso de fondos";
$_data['add_new_form_field_text_1'] = "Nombre del propietario";
$_data['add_new_form_field_text_2'] = "Seleccionar propietario";
$_data['add_new_form_field_text_3'] = "Mes";
$_data['add_new_form_field_text_4'] = "Contacto";
$_data['add_new_form_field_text_5'] = "Año";
$_data['add_new_form_field_text_6'] = "Fecha";
$_data['add_new_form_field_text_7'] = "Cantidad";
$_data['add_new_form_field_text_8'] = "Propósito";
$_data['add_new_form_field_text_9'] = "# factura";
$_data['added_fund_successfully'] = "Se agregó la información del fondo con éxito";
$_data['update_fund_successfully'] = "La información actualizada del fondo se realizó correctamente";
$_data['delete_fund_information'] = "Información del fondo eliminada con éxito";
$_data['delete_confirm'] = "¿Está seguro de que desea eliminar este fondo de eliminación?";
?>