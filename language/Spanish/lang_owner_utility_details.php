<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Detalles de la utilidad del propietario";
$_data['text_2'] = "Nombre del propietario";
$_data['text_3'] = "Floor No";
$_data['text_4'] = "Número de unidad";
$_data['text_5'] = "Nombre del mes";
$_data['text_6'] = "Alquiler";
$_data['text_7'] = "Factura total";
$_data['text_8'] = "Fecha de emisión";
$_data['text_9'] = "Factura de agua";
$_data['text_10'] = "Factura eléctrica";
$_data['text_11'] = "Factura de gas";
$_data['text_12'] = "Factura de seguridad";
$_data['text_13'] = "Factura de servicios públicos";
$_data['text_14'] = "Otra factura";
$_data['text_15'] = "Renta";
?>