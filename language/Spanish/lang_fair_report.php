<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Informe de colección de alquiler";
$_data['text_2'] = "Informe";
$_data['text_3'] = "Formulario de informe de colección de alquiler";
$_data['text_4'] = "Seleccionar piso";
$_data['text_5'] = "Seleccionar unidad";
$_data['text_6'] = "Seleccionar mes";
$_data['text_66'] = "Seleccionar año";
$_data['text_7'] = "Enviar";
$_data['text_8'] = "Estado del pago";
$_data['text_88'] = "Seleccionar";
$_data['text_99'] = "Pagado";
$_data['text_100'] = "Debido";
$_data['validation'] = "Seleccione al menos uno o más campos";
?>