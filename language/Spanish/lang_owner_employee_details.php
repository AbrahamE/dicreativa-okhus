<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Lista de vigilantes";
$_data['text_2'] = "Nombre";
$_data['text_3'] = "Correo electrónico";
$_data['text_4'] = "Contacto";
$_data['text_5'] = "Dirección actual";
$_data['text_6'] = "Designación";
$_data['text_7'] = "Fecha de incorporación";
$_data['text_8'] = "Detalles del vigilante";
$_data['text_9'] = "Dirección permanente";
?>