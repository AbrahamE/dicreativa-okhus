<?php
// Conversión de idioma del menú del sitio web
// Paquete en inglés
// derechos de autor: iposint.com
$_data['text_1'] = "Detalles del inquilino";
$_data['text_5'] = "Nombre";
$_data['text_6'] = "Correo electrónico";
$_data['text_7'] = "Contacto";
$_data['text_8'] = "Dirección";
$_data['text_9'] = "Floor No";
$_data['text_10'] = "Unidad alquilada no";
$_data['text_11'] = "Pago por adelantado";
$_data['text_12'] = "Alquiler por mes";
$_data['text_13'] = "Fecha de alquiler";
$_data['text_14'] = "NID";
$_data['text_19'] = "Tablero";
?>