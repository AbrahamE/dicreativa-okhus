<?php
define('DIR_APPLICATION', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');
include(DIR_APPLICATION."app_config.php");
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Código QR de Autorización</title>
<link rel="stylesheet" type="text/css" href="app/styles/style.css">
<link rel="stylesheet" type="text/css" href="app/styles/framework.css">
<link rel="stylesheet" type="text/css" href="app/fonts/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="app/scripts/qr-pdf/print.min.css"> 																				 
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="app/_manifest.json" data-pwa-version="set_by_pwa.js">
<link rel="apple-touch-icon" sizes="180x180" href="app/app/icons/icon-192x192.png">

<style type="text/css">

.landing-icons a em {
    font-size: 15px!important;
	color: #727171!important;
}

[data-gradient="gradient-1"] .page-bg, [data-gradient="gradient-1"] .footer-menu .active-nav i, [data-gradient="gradient-1"] .menu-box-gradient, .gradient-1 {
    background: linear-gradient(0deg,#ffffff,#ffffff);
}

.color-white {
    color: #006597!important;
}

.landing-header a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-icons a img {
    width: 80px!important;
    margin: 0 auto!important;
}

.landing-header a em {
    font-size: 15px!important;
    color: #727171!important;
}

.landing-header a em {
    display: block;
    color: #FFFFFF;
    text-align: center;
    font-size: 13px;
    font-style: normal;
    margin-top: 5px;
    text-shadow: 0px 1px 0px #000000;
}

.responsive-image-full {
    display: block;
    height: auto;
    width: calc(50% + 0px);
    transform: translateX(0%);
    overflow: hidden;
	margin: auto;
}

.responsive-image {
	margin: auto;
}


</style>

</head>
    
<body class="theme-light" data-gradient="gradient-1">
        
<div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner"></div></div>
    </div>
    
	<div class="header header-transparent header-fixed header-logo-app">
        <a href="#" onclick="window.history.back();" class="back-button header-title color-white"></a>
		<a href="#" onclick="window.history.back();" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-sign-out-alt"></i></a>
	</div>
                  
    <div class="page-bg"><div></div></div>
    <div class="page-content">     
                
        <div data-height="cover-header" class="caption bottom-0">
					<div class="caption-center center-text">

						<div class="landing-page">
						
				<?php 

				if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){
							
				$result = mysqli_query($link,"SELECT * FROM tbl_meeting INNER JOIN tbl_add_rent ON tbl_add_rent.rid = tbl_meeting.id_rent where meeting_id = '" . $_GET['id_evento'] . "' and tbl_meeting.activated = 1 and tbl_meeting.deleted = 0 LIMIT 1");
				$event_count = mysqli_num_rows($result);	
				
				if($event_count != false && $event_count > 0) {

					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

					$meeting_id = $row["meeting_id"];
									
					mysqli_close($link);
				}

				?>						
						
				<div class="landing-header">
					<img src="app/images/logo-dark.png" class="responsive-image-full bottom-20">
				</div>

				<div class="landing-header">
					<h4 class="text-center">Código QR para Autorización</h4>
					<h4 class="text-center">Detalle de la Autorización Número: <?= $meeting_id ?></h4>
					<p class="text-center">
						Código QR valido para autorizaciones de OKHUS.
					</p>

					<img id="qr-auth" src="app/images/page-loader.gif" class="center-text responsive-image-full bottom-20" />
					
							<div class="landing-header">
								<a id="qr-download" name="qr-download" href="#" class="back-button header-icon header-icon-1 color-white back-button font-30 left-10"><i class="fas fa-download"></i><em>Descargar</em></a> 				
								
								<div class="clear"></div>
							</div>					

					<div class="clear"></div>
				</div>

				<?php } else { ?>
				
				<img src="app/images/logo-dark.png" class="center-text responsive-image-full bottom-20" />
						
				<div class="content content-box round-medium shadow-huge bottom-0">
					<h4 class="text-center">Error en Detalle de la Autorización</h4>
					<p class="text-center">
						No se han encontrado ninguna información de la autorización buscada.
					</p>
					<center>
						<a href="#" onclick="window.history.back();" class="button shadow-huge button-3d button-m button-round-small border-blue2-dark bg-blue2-light"><i class="fa fa-home"></i> Regresar</a>            
					</center>
				</div>	
				<?php } ?>								

						</div>
				</div>
            </div>
        </div>        
    </div>      
<script type="text/javascript" src="app/scripts/jquery.js"></script>
<script type="text/javascript" src="app/scripts/plugins.js"></script>
<script type="text/javascript" src="app/scripts/custom.js" async></script>
<script type="text/javascript" src="app/scripts/qr-code/qrcode.js"></script>
<script type="text/javascript" src="app/scripts/qr-pdf/print.min.js"></script>
<script type="text/javascript" src="app/scripts/download2.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		'use strict'
	
	<?php if(isset($_GET['id_evento']) && $_GET['id_evento'] != '' && $_GET['id_evento'] > 0){ ?>
			
		generateQR(<?= $meeting_id ?>);

	<?php } ?>
	
		$('#qr-download').click(function(e) {
			
			e.preventDefault();
			
			download($('#qr-auth').attr('src'),"okhus_autorizacion_<?= $meeting_id ?>","image/jpeg");
			
			return false;
		});	

	});

function generateQR(data) {
    var typeNumber = 4;
    var errorCorrectionLevel = "L";
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData(data + "-" + Math.floor(Date.now() / 1000).toString());
    qr.make();
    document.getElementById("qr-auth").src = qr.createDataURL(10);
    $("#qr-auth").addClass("shadow-huge");
    $("#qr-auth").css("width", "50%");
}

</script>
</body>